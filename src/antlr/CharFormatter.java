package antlr;

/* ANTLR Translator Generator
 * Project led by Terence Parr at http://www.jGuru.com
 * Software rights: http://www.antlr.org/RIGHTS.html
 *
 * $Id: CharFormatter.java,v 1.5 2008/04/15 17:19:39 elinares690715 Exp $
 */

/**
 * Interface used by BitSet to format elements of the set when converting to
 * string
 */
public interface CharFormatter {
	public String escapeChar(int c, boolean forCharLiteral);

	public String escapeString(String s);

	public String literalChar(int c);

	public String literalString(String s);
}
