package antlr.collections;

/* ANTLR Translator Generator
 * Project led by Terence Parr at http://www.jGuru.com
 * Software rights: http://www.antlr.org/RIGHTS.html
 *
 * $Id: ASTEnumeration.java,v 1.5 2008/04/15 17:20:05 elinares690715 Exp $
 */
public interface ASTEnumeration {
	public boolean hasMoreNodes();

	public AST nextNode();
}
