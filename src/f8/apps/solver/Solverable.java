/*
 * Created on 07-oct-2003
 *
 
 
 */
package f8.apps.solver;

import f8.objects.types.Ereal;

/**
 * @author elinares
 */
public interface Solverable {

	public abstract boolean equals(Object obj);

	public abstract boolean tolerance(Solverable obj, Ereal tol);

	public abstract Solverable zero();

	public abstract int sign();

	public abstract Ereal compare(Solverable obj);

	public abstract Solverable mean(Solverable o1, Solverable o2);

	public abstract Solverable amplia(Ereal pct);

}
