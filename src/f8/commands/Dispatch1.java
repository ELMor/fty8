package f8.commands;

import f8.Core;
import f8.commands.alg.CommandSequence;
import f8.commands.math.mkl;
import f8.exceptions.BadArgumentTypeException;
import f8.exceptions.F8Exception;
import f8.exceptions.TooFewArgumentsException;
import f8.objects.Stackable;
import f8.objects.types.Complex;
import f8.objects.types.Double;
import f8.objects.types.F8String;
import f8.objects.types.F8Vector;
import f8.objects.types.InfixExp;
import f8.objects.types.Int;
import f8.objects.types.Lista;
import f8.objects.types.Literal;
import f8.objects.types.Matrix;
import f8.objects.types.Unit;

public abstract class Dispatch1 extends Command {
	public final void exec(Object extern) throws F8Exception {
		
		Stackable res = null;

		if (Core.check(1)) {
			Stackable a = Core.peek();
			if (a instanceof Double) {
				res = prfDouble((Double) a);
			} else if (a instanceof Unit) {
				res = prfUnit((Unit) a);
			} else if (a instanceof InfixExp || a instanceof Literal) {
				res = prfInfix((InfixExp) a);
			} else if (a instanceof Complex) {
				res = prfComplex((Complex) a);
			} else if (a instanceof F8Vector) {
				res = prfVector((F8Vector) a);
			} else if (a instanceof Matrix) {
				res = prfMatrix((Matrix) a);
			} else if (a instanceof Lista) {
				res = prfLista((Lista) a);
			} else if (a instanceof F8String) {
				res = prfString((F8String) a);
			} else if (a instanceof Int) {
				res = prfInt((Int) a);
			} else {
				throw new BadArgumentTypeException(this);
			}
			if (res != null) {
				Core.pop();
				if ( res instanceof CommandSequence
				     && !(res instanceof Lista)
					) {
					CommandSequence csres = (CommandSequence) res;
					for (int i = 0; i < csres.size(); i++)
						Core.push((Stackable) csres.get(i));
				} else {
					Core.push(res);
				}

			} else {
				throw new BadArgumentTypeException(this);
			}
		} else {
			throw new TooFewArgumentsException(this);
		}
	}

	public Stackable prfDouble(Double x) throws F8Exception {
		return null;
	}

	public Stackable prfUnit(Unit x) throws F8Exception {
		return null;
	}

	public Stackable prfComplex(Complex x) throws F8Exception {
		return null;
	}

	public Stackable prfVector(F8Vector x) throws F8Exception {
		return null;
	}

	public Stackable prfMatrix(Matrix x) throws F8Exception {
		return null;
	}

	public Stackable prfLista(Lista x) throws F8Exception {
		return null;
	}

	public Stackable prfString(F8String x) throws F8Exception {
		return null;
	}

	public Stackable prfInt(Int x) throws F8Exception {
		return null;
	}

	public Stackable prfInfix(InfixExp a) throws F8Exception {
		return new InfixExp(mkl.fca(toString(), a.getAST()));
	}

	public String undo() {
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.Command#isAlgebraic()
	 */
	public boolean isAlgebraic() {
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.Command#isInfix()
	 */
	public boolean isInfix() {
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.Command#isOperator()
	 */
	public boolean isOperator() {
		return false;
	}

}
