package f8.commands.alg;

import f8.objects.types.Ereal;

public interface ComplexValue {
	public Ereal[] complexValue();
}
