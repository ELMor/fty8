/*
 * Created on 29-ago-2003
 *
 
 
 */
package f8.commands.alg;

import f8.objects.Stackable;

/**
 * @author elinares
 * 
 * 
 * 
 */
public interface Copiable {
	public Stackable copia();
}
