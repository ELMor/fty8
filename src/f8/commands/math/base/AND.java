package f8.commands.math.base;


import java.io.IOException;

import f8.commands.Storable;
import f8.objects.types.Ereal;
import f8.platform.io.DIS;
import f8.platform.io.DOS;


public final class AND extends Logical2 {
	public AND() {
		// Aqui estaba systemInstall
	}

	public int getID() {
		return 1;
	}

	public Storable getInstance() {
		return new AND();
	}

	public void loadState(DIS ds) throws IOException {
	}

	public void saveState(DOS ds)  throws IOException {
	}

	public String toString() {
		return ("AND");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.Dispatch2#prfDoubleDouble(f8.kernel.types.Double,
	 *      f8.kernel.types.Double)
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.op.boo.Logical#logic(double, double)
	 */
	public Ereal logic(Ereal l, Ereal r) {
		return l.and(r);
	}

}
