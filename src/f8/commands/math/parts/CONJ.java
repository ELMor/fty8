package f8.commands.math.parts;


import java.io.IOException;

import f8.commands.Dispatch1;
import f8.commands.Storable;
import f8.commands.alg.IntValue;
import f8.exceptions.F8Exception;
import f8.objects.Stackable;
import f8.objects.types.Complex;
import f8.objects.types.Double;
import f8.objects.types.Int;
import f8.platform.io.DIS;
import f8.platform.io.DOS;

public final class CONJ extends Dispatch1 {
	public CONJ() {
		// Aqui estaba systemInstall
	}

	public int getID() {
		return 3002;
	}

	public Storable getInstance() {
		return new CONJ();
	}

	public void loadState(DIS ds) throws IOException {
	}

	public void saveState(DOS ds)  throws IOException {
	}

	public String toString() {
		return ("CONJ");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.Dispatch1#prfInt(f8.kernel.types.Int)
	 */
	public Stackable prfInt(Int a) throws F8Exception {
		int A = ((IntValue) a).intValue();

		if (A < 0) {
			A = -A;
		}
		return new Int(A);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.Dispatch1#prfDouble(f8.kernel.types.Double)
	 */
	public Stackable prfDouble(Double a) throws F8Exception {
		return new Double(a.doubleValue());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.Dispatch1#prfComplex(f8.kernel.types.Complex)
	 */
	public Stackable prfComplex(Complex a) throws F8Exception {
		return new Complex(a.re, a.im.neg());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.Command#isAlgebraic()
	 */
	public boolean isAlgebraic() {
		return true;
	}

}
