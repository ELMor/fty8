package f8.commands.math.parts;


import java.io.IOException;

import f8.commands.Dispatch1;
import f8.commands.Storable;
import f8.exceptions.F8Exception;
import f8.objects.Stackable;
import f8.objects.types.Complex;
import f8.objects.types.Double;
import f8.platform.io.DIS;
import f8.platform.io.DOS;

public final class RE extends Dispatch1 {
	public RE() {
		// Aqui estaba systemInstall
	}

	public int getID() {
		return 70;
	}

	public Storable getInstance() {
		return new RE();
	}

	public void loadState(DIS ds) throws IOException {
	}

	public void saveState(DOS ds)  throws IOException {
	}

	public String toString() {
		return ("RE");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.Dispatch1#prfComplex(f8.kernel.types.Complex)
	 */
	public Stackable prfComplex(Complex a) throws F8Exception {
		return (new Double(a.complexValue()[0]));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.Dispatch1#prfDouble(f8.kernel.types.Double)
	 */
	public Stackable prfDouble(Double x) throws F8Exception {
		return x;
	}

}
