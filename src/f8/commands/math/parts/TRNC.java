package f8.commands.math.parts;


import java.io.IOException;

import f8.commands.Dispatch2;
import f8.commands.Storable;
import f8.exceptions.F8Exception;
import f8.objects.Stackable;
import f8.objects.types.Double;
import f8.objects.types.Ereal;
import f8.platform.io.DIS;
import f8.platform.io.DOS;

public final class TRNC extends Dispatch2 {
	public TRNC() {
		// Aqui estaba systemInstall
	}

	public int getID() {
		return 3016;
	}

	public String toString() {
		return ("TRNC");
	}

	public Storable getInstance() {
		return new TRNC();
	}

	public void loadState(DIS ds) throws IOException {
	}

	public void saveState(DOS ds)  throws IOException {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.Command#isAlgebraic()
	 */
	public boolean isAlgebraic() {
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.Dispatch2#prfDoubleDouble(f8.types.Double, f8.types.Double)
	 */
	public Stackable prfDoubleDouble(Double a, Double b) throws F8Exception {
		Ereal pos = new Ereal(b.doubleValue().toInt());
		Ereal mant = new Ereal(a.doubleValue().mul(pos.exp10()).toInt());
		return new Double(mant.div(pos.exp10()));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.Command#isInfix()
	 */
	public boolean isInfix() {
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.Command#isOperator()
	 */
	public boolean isOperator() {
		return false;
	}

}
