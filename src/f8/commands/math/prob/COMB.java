package f8.commands.math.prob;


import java.io.IOException;

import f8.commands.Dispatch2;
import f8.commands.Storable;
import f8.exceptions.BadArgumentValueException;
import f8.exceptions.F8Exception;
import f8.objects.Stackable;
import f8.objects.types.Double;
import f8.objects.types.Ereal;
import f8.platform.io.DIS;
import f8.platform.io.DOS;

public final class COMB extends Dispatch2 {
	public COMB() {
		// Aqui estaba systemInstall
	}

	public int getID() {
		return 3019;
	}

	public String toString() {
		return ("COMB");
	}

	public Storable getInstance() {
		return new COMB();
	}

	public void loadState(DIS ds) throws IOException {
	}

	public void saveState(DOS ds)  throws IOException {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.Command#isAlgebraic()
	 */
	public boolean isAlgebraic() {
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.Command#isInfix()
	 */
	public boolean isInfix() {
		// TODO Auto-generated method stub
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.Command#isOperator()
	 */
	public boolean isOperator() {
		// TODO Auto-generated method stub
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.Dispatch2#prfDoubleDouble(f8.types.Double, f8.types.Double)
	 */
	public Stackable prfDoubleDouble(Double a, Double b) throws F8Exception {
		int n = a.doubleValue().toInt(), m = b.doubleValue().toInt();
		if (n < m)
			throw new BadArgumentValueException(this);
		if (n == m)
			return new Double(1);
		Ereal ret = Ereal.uno;
		for (int i = n; i > m; i--)
			ret = ret.mul(i);
		for (int j = n - m; j > 1; j--)
			ret = ret.div(j);
		return new Double(ret);
	}

}
