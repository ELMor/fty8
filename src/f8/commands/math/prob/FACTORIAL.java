package f8.commands.math.prob;


import java.io.IOException;

import f8.commands.Dispatch1;
import f8.commands.Storable;
import f8.exceptions.F8Exception;
import f8.objects.Stackable;
import f8.objects.types.Double;
import f8.objects.types.Ereal;
import f8.platform.io.DIS;
import f8.platform.io.DOS;

public final class FACTORIAL extends Dispatch1 {
	public FACTORIAL() {
		// Aqui estaba systemInstall
	}

	public int getID() {
		return 3021;
	}

	public String toString() {
		return ("!");
	}

	public Storable getInstance() {
		return new FACTORIAL();
	}

	public void loadState(DIS ds) throws IOException {
	}

	public void saveState(DOS ds)  throws IOException {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.Dispatch1#prfDouble(f8.kernel.types.Double)
	 */
	public Stackable prfDouble(Double a) throws F8Exception {
		int n = a.doubleValue().toInt();
		if (n == 0 || n == 1)
			return new Double(1);
		Ereal ret = Ereal.uno;
		for (int j = 2; j < n; j++)
			ret = ret.mul(j);
		return new Double(ret);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.Command#isAlgebraic()
	 */
	public boolean isAlgebraic() {
		return true;
	}

}
