package f8.commands.math.prob;


import java.io.IOException;

import f8.commands.Dispatch2;
import f8.commands.Storable;
import f8.exceptions.BadArgumentValueException;
import f8.exceptions.F8Exception;
import f8.objects.Stackable;
import f8.objects.types.Double;
import f8.objects.types.Ereal;
import f8.platform.io.DIS;
import f8.platform.io.DOS;

public final class PERM extends Dispatch2 {
	public PERM() {
		// Aqui estaba systemInstall
	}

	public int getID() {
		return 3020;
	}

	public String toString() {
		return ("PERM");
	}

	public Storable getInstance() {
		return new PERM();
	}

	public void loadState(DIS ds) throws IOException {
	}

	public void saveState(DOS ds)  throws IOException {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.Command#isAlgebraic()
	 */
	public boolean isAlgebraic() {
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.Command#isInfix()
	 */
	public boolean isInfix() {
		// TODO Auto-generated method stub
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.Command#isOperator()
	 */
	public boolean isOperator() {
		// TODO Auto-generated method stub
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.Dispatch2#prfDoubleDouble(f8.types.Double, f8.types.Double)
	 */
	public Stackable prfDoubleDouble(Double a, Double b) throws F8Exception {
		int n = a.doubleValue().toInt(), r = b.doubleValue().toInt();
		if (n < r)
			throw new BadArgumentValueException(this);
		Ereal ret = Ereal.uno;
		for (int i = n; i > 1; i--)
			ret = ret.mul(i);
		return new Double(ret.div(n - r));
	}

}
