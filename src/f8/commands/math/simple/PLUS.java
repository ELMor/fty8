package f8.commands.math.simple;

import java.io.IOException;

import antlr.collections.AST;
import f8.Core;
import f8.commands.Dispatch2;
import f8.commands.Storable;
import f8.commands.math.mkl;
import f8.commands.math.deriv.DER;
import f8.commands.math.deriv.Derivable;
import f8.exceptions.F8Exception;
import f8.exceptions.InvalidDimensionException;
import f8.objects.Stackable;
import f8.objects.types.Complex;
import f8.objects.types.Double;
import f8.objects.types.Ereal;
import f8.objects.types.F8String;
import f8.objects.types.F8Vector;
import f8.objects.types.InfixExp;
import f8.objects.types.Int;
import f8.objects.types.Lista;
import f8.objects.types.Matrix;
import f8.objects.types.TempUnit;
import f8.objects.types.Unit;
import f8.platform.io.DIS;
import f8.platform.io.DOS;

public final class PLUS extends Dispatch2 implements Derivable {
	public PLUS() {
		// Aqui estaba systemInstall
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.op.symb.Derivable#derive(antlr.collections.AST,
	 *      java.lang.String)
	 */
	public AST deriveWithArgs(AST args, String var) throws F8Exception {
		AST a1;
		AST a2;
		AST a3;
		a1 = args;
		a2 = a1.getNextSibling();
		a3 = a2.getNextSibling();
		if (a3 == null) {
			return mkl.sum(DER.deriveFunction(a1, var), DER.deriveFunction(a2,
					var));
		} else {
			return mkl
					.sum(DER.deriveFunction(a1, var), deriveWithArgs(a2, var));
		}
	}

	public int getID() {
		return 88;
	}

	public Storable getInstance() {
		return new PLUS();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.Command#isAlgebraic()
	 */
	public boolean isAlgebraic() {
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.Command#isInfix()
	 */
	public boolean isInfix() {
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.Command#isOperator()
	 */
	public boolean isOperator() {
		return true;
	}

	public void loadState(DIS ds) throws IOException {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.Dispatch2#prfComplexComplex(f8.kernel.types.Complex,
	 *      f8.kernel.types.Complex)
	 */
	public Stackable prfComplexComplex(Complex a, Complex b) throws F8Exception {
		return a.sum(b);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.Dispatch2#prfComplexDouble(f8.types.Complex, f8.types.Double)
	 */
	public Stackable prfComplexDouble(Complex a, Double b) throws F8Exception {
		return prfDoubleComplex(b, a);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.Dispatch2#prfComplexInfixExp(f8.types.Complex, f8.types.InfixExp)
	 */
	public Stackable prfComplexInfixExp(Complex a, InfixExp b)
			throws F8Exception {
		return new InfixExp(mkl.sum(a.getAST(), b.getAST()));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.Dispatch2#prfDoubleComplex(f8.types.Double, f8.types.Complex)
	 */
	public Stackable prfDoubleComplex(Double a, Complex b) throws F8Exception {
		return new Complex(b.re.plus(a.doubleValue()), b.im).mutate();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.Dispatch2#prfDoubleDouble(f8.kernel.types.Double,
	 *      f8.kernel.types.Double)
	 */
	public Stackable prfDoubleDouble(Double a, Double b) throws F8Exception {
		return new Double(a.doubleValue().plus(b.doubleValue()));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.Dispatch2#prfDoubleInfixExp(f8.types.Double, f8.types.InfixExp)
	 */
	public Stackable prfDoubleInfixExp(Double a, InfixExp b) throws F8Exception {
		return new InfixExp(mkl.sum(a.getAST(), b.getAST()));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.Dispatch2#prfF8StringComplex(f8.kernel.types.F8String,
	 *      f8.kernel.types.Complex)
	 */
	public Stackable prfF8StringComplex(F8String a, Complex b)
			throws F8Exception {
		return new F8String(a.getContenido() + b.toString());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.Dispatch2#prfF8StringDouble(f8.kernel.types.F8String,
	 *      f8.kernel.types.Double)
	 */
	public Stackable prfF8StringDouble(F8String a, Double b) throws F8Exception {
		return new F8String(a.getContenido() + b.toString());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.Dispatch2#prfF8StringF8String(f8.kernel.types.F8String,
	 *      f8.kernel.types.F8String)
	 */
	public Stackable prfF8StringF8String(F8String a, F8String b)
			throws F8Exception {
		return new F8String(a.getContenido() + b.getContenido());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.Dispatch2#prfF8StringF8Vector(f8.kernel.types.F8String,
	 *      f8.kernel.types.F8Vector)
	 */
	public Stackable prfF8StringF8Vector(F8String a, F8Vector b)
			throws F8Exception {
		return new F8String(a.getContenido() + b.toString());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.Dispatch2#prfF8StringInfixExp(f8.kernel.types.F8String,
	 *      f8.kernel.types.InfixExp)
	 */
	public Stackable prfF8StringInfixExp(F8String a, InfixExp b)
			throws F8Exception {
		String ie=b.toString();
		return new F8String(a.getContenido() + ie.substring(1, ie.length()-1));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.Dispatch2#prfF8StringInt(f8.kernel.types.F8String,
	 *      f8.kernel.types.Int)
	 */
	public Stackable prfF8StringInt(F8String a, Int b) throws F8Exception {
		return new F8String(a.getContenido() + b.toString());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.Dispatch2#prfF8StringLista(f8.kernel.types.F8String,
	 *      f8.kernel.types.Lista)
	 */
	public Stackable prfF8StringLista(F8String a, Lista b) throws F8Exception {
		return new F8String(a.getContenido() + b.toString());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.Dispatch2#prfF8StringMatrix(f8.kernel.types.F8String,
	 *      f8.kernel.types.Matrix)
	 */
	public Stackable prfF8StringMatrix(F8String a, Matrix b) throws F8Exception {
		return new F8String(a.getContenido() + b.toString());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.Dispatch2#prfF8StringUnit(f8.kernel.types.F8String,
	 *      f8.kernel.types.Unit)
	 */
	public Stackable prfF8StringUnit(F8String a, Unit b) throws F8Exception {
		return new F8String(a.getContenido() + b.toString());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.Dispatch2#prfF8VectorF8Vector(f8.kernel.types.F8Vector,
	 *      f8.kernel.types.F8Vector)
	 */
	public Stackable prfF8VectorF8Vector(F8Vector a, F8Vector it)
			throws F8Exception {
		Ereal[] y = it.x;
		int nx = a.x.length;
		int ny = y.length;

		if (nx == ny) {
			Ereal[] z = new Ereal[nx];

			for (int i = 0; i < nx; i++) {
				z[i] = a.x[i].plus(y[i]);
			}

			return (new F8Vector(z));
		} else {
			throw new InvalidDimensionException(new PLUS());
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.Dispatch2#prfInfixExpComplex(f8.types.InfixExp, f8.types.Complex)
	 */
	public Stackable prfInfixExpComplex(InfixExp a, Complex b)
			throws F8Exception {
		return new InfixExp(mkl.sum(a.getAST(), b.getAST()));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.Dispatch2#prfInfixExpDouble(f8.types.InfixExp, f8.types.Double)
	 */
	public Stackable prfInfixExpDouble(InfixExp a, Double b) throws F8Exception {
		return new InfixExp(mkl.sum(a.getAST(), b.getAST()));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.Dispatch2#prfInfixExpInfixExp(f8.types.InfixExp,
	 *      f8.types.InfixExp)
	 */
	public Stackable prfInfixExpInfixExp(InfixExp a, InfixExp b)
			throws F8Exception {
		return new InfixExp(mkl.sum(a.getAST(), b.getAST()));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.Dispatch2#prfIntInt(f8.kernel.types.Int,
	 *      f8.kernel.types.Int)
	 */
	public Stackable prfIntInt(Int a, Int b) throws F8Exception {
		return new Int(a.n + b.n);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.Dispatch2#prfMatrixMatrix(f8.kernel.types.Matrix,
	 *      f8.kernel.types.Matrix)
	 */
	public Stackable prfMatrixMatrix(Matrix a, Matrix it) throws F8Exception {
		double[][] y = it.x;

		if ((a.r == it.r) && (a.c == it.c)) {
			double[][] z = new double[a.r][a.c];

			for (int i = 0; i < a.r; i++) {
				for (int j = 0; j < a.c; j++) {
					z[i][j] = a.x[i][j] + y[i][j];
				}
			}

			return (new Matrix(z));
		} else {
			throw new InvalidDimensionException(new PLUS());
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.Dispatch2#prfTempUnitTempUnit(f8.kernel.types.TempUnit,
	 *      f8.kernel.types.TempUnit)
	 */
	public Stackable prfTempUnitTempUnit(TempUnit a, TempUnit b)
			throws F8Exception {
		TempUnit res = new TempUnit(
				a.ubase().getValor().plus(b.ubase().getValor()), 
				mkl.fca("K", null));
		res.convert(b);
		return Core.pop();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.Dispatch2#prfUnitUnit(f8.kernel.types.Unit,
	 *      f8.kernel.types.Unit)
	 */
	public Stackable prfUnitUnit(Unit a, Unit b) throws F8Exception {
		Unit converted = b.convert(a);
		return (new Unit(a.getValor().plus(converted.getValor()), a.getUnidad()));
	}

	public void saveState(DOS ds)  throws IOException {
	}

	public String toString() {
		return ("+");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.Dispatch2#prfInfixExpUnit(f8.types.InfixExp, f8.types.Unit)
	 */
	public Stackable prfInfixExpUnit(InfixExp a, Unit b) throws F8Exception {
		return new InfixExp(mkl.sum(a.getAST(), b.getAST()));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.Dispatch2#prfUnitInfixExp(f8.types.Unit, f8.types.InfixExp)
	 */
	public Stackable prfUnitInfixExp(Unit a, InfixExp b) throws F8Exception {
		return new InfixExp(mkl.sum(a.getAST(), b.getAST()));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.Dispatch2#prfListaComplex(f8.types.Lista, f8.types.Complex)
	 */
	public Stackable prfListaComplex(Lista a, Complex b) throws F8Exception {
		a.add(b);
		return a;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.Dispatch2#prfListaDouble(f8.types.Lista, f8.types.Double)
	 */
	public Stackable prfListaDouble(Lista a, Double b) throws F8Exception {
		a.add(b);
		return a;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.Dispatch2#prfListaF8String(f8.types.Lista, f8.types.F8String)
	 */
	public Stackable prfListaF8String(Lista a, F8String b) throws F8Exception {
		a.add(b);
		return a;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.Dispatch2#prfListaF8Vector(f8.types.Lista, f8.types.F8Vector)
	 */
	public Stackable prfListaF8Vector(Lista a, F8Vector b) throws F8Exception {
		a.add(b);
		return a;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.Dispatch2#prfListaInfixExp(f8.types.Lista, f8.types.InfixExp)
	 */
	public Stackable prfListaInfixExp(Lista a, InfixExp b) throws F8Exception {
		a.add(b);
		return a;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.Dispatch2#prfListaInt(f8.types.Lista, f8.types.Int)
	 */
	public Stackable prfListaInt(Lista a, Int b) throws F8Exception {
		a.add(b);
		return a;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.Dispatch2#prfListaLista(f8.types.Lista, f8.types.Lista)
	 */
	public Stackable prfListaLista(Lista a, Lista b) throws F8Exception {
		a.add(b);
		return a;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.Dispatch2#prfListaMatrix(f8.types.Lista, f8.types.Matrix)
	 */
	public Stackable prfListaMatrix(Lista a, Matrix b) throws F8Exception {
		a.add(b);
		return a;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.Dispatch2#prfListaUnit(f8.types.Lista, f8.types.Unit)
	 */
	public Stackable prfListaUnit(Lista a, Unit b) throws F8Exception {
		a.add(b);
		return a;
	}

}
