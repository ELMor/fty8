/*
 * Created on 03-oct-2003
 *
 
 
 */
package f8.commands.prog.debug;

import f8.Core;
import f8.commands.Operation;
import f8.exceptions.BadArgumentTypeException;
import f8.exceptions.F8Exception;
import f8.exceptions.TooFewArgumentsException;
import f8.platform.Calc;

/**
 * @author elinares
 * 
 * 
 * 
 */
public class DBUG extends Operation {

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.Operation#exec()
	 */
	public void exec(Object extern) throws F8Exception {
		if (Core.check(1)) {
			try {
				Calc.ref.flush();
				Debuggable dtask = (Debuggable) Core.pop();
				Core.setProcDebug(dtask);
				//Core.temporaryLabels(null, dtask.nextCommand());
			} catch (Exception e) {
				throw new BadArgumentTypeException("Not a program");
			}
		} else {
			throw new TooFewArgumentsException("No program to debug");
		}
	}

}
