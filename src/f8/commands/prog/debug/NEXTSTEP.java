/*
 * Created on 03-oct-2003
 *
 
 
 */
package f8.commands.prog.debug;

import f8.Core;
import f8.commands.Operation;
import f8.exceptions.F8Exception;
import f8.exceptions.TooFewArgumentsException;

/**
 * @author elinares
 * 
 * 
 * 
 */
public class NEXTSTEP extends Operation {

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.Operation#exec()
	 */
	public void exec(Object extern) throws F8Exception {
		Debuggable dtask = Core.getProcDebug();
		if (dtask != null) {
			//Core.temporaryLabels(null, dtask.nextCommand());
		} else {
			throw new TooFewArgumentsException("No program to debug");
		}
	}

}
