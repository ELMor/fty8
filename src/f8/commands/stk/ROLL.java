package f8.commands.stk;

import java.io.IOException;
import java.util.Vector;

import f8.Core;
import f8.commands.NonAlgebraic;
import f8.commands.Storable;
import f8.exceptions.F8Exception;
import f8.exceptions.TooFewArgumentsException;
import f8.objects.Stackable;
import f8.objects.types.Double;
import f8.platform.io.DIS;
import f8.platform.io.DOS;

public final class ROLL extends NonAlgebraic {
	public ROLL() {
		// Aqui estaba systemInstall
	}

	public int getID() {
		return 7;
	}

	public Storable getInstance() {
		return new ROLL();
	}

	public void loadState(DIS ds) throws IOException {
	}

	public void saveState(DOS ds)  throws IOException {
	}

	public void exec(Object extern) throws F8Exception {
		int rot;
		if (Core.check(1)) {
			Stackable a = Core.pop();
			if (a instanceof Double) {
				rot = ((Double) a).doubleValue().toInt();
				if (Core.check(rot)) {
					Vector gb = new Vector();
					for (int i = 0; i < rot; i++) {
						gb.addElement(Core.pop());
					}
					for (int i = 1; i < rot; i++) {
						Core.push((Stackable) gb.elementAt(i));
					}
					Core.push((Stackable) gb.elementAt(0));
				} else {
					throw new TooFewArgumentsException(this);
				}
			}
		} else {
			throw new TooFewArgumentsException(this);
		}
	}

	public String toString() {
		return ("ROLL");
	}
}
