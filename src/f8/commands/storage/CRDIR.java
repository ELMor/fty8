/*
 * Created on 07-ago-2003
 *
 
 
 */
package f8.commands.storage;

import java.io.IOException;

import f8.Core;
import f8.commands.Command;
import f8.commands.NonAlgebraic;
import f8.commands.Storable;
import f8.exceptions.BadArgumentTypeException;
import f8.exceptions.F8Exception;
import f8.exceptions.TooFewArgumentsException;
import f8.objects.types.Directory;
import f8.objects.types.InfixExp;
import f8.objects.types.Literal;
import f8.platform.io.DIS;
import f8.platform.io.DOS;

/**
 * @author elinares
 * 
 * 
 * 
 */
public final class CRDIR extends NonAlgebraic {
	public CRDIR() {
		// Aqui estaba systemInstall
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.platform.io.Storable#getID()
	 */
	public int getID() {
		return 125;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.platform.io.Storable#getInstance()
	 */
	public Storable getInstance() {
		return new CRDIR();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.platform.io.Storable#saveState(f8.platform.io.DataStream)
	 */
	public void saveState(DOS ds)  throws IOException {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.platform.io.Storable#loadState(f8.platform.io.DataStream)
	 */
	public void loadState(DIS ds) throws IOException {
	}

	public void exec(Object extern) throws F8Exception {

		if (Core.check(1)) {
			Command a = Core.peek(0);
			String name;

			// define a to be b
			if (a instanceof Literal) {
				name = a.toString();
				Core.pop(1);
				createDir(name);
			} else if ((name = InfixExp.isLiteral(a)) != null) {
				Core.pop(1);
				createDir(name);
			} else {
				throw new BadArgumentTypeException(this);
			}
		} else {
			throw new TooFewArgumentsException(this);
		}
	}

	public static void createDir(String name) {
		new Directory(Core.currentDict(), name);
		//Core.refreshProgMenu();
	}

	public String toString() {
		return "CRDIR";
	}
}
