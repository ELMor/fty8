package f8.commands.storage;

import java.io.IOException;

import f8.Core;
import f8.commands.Command;
import f8.commands.NonAlgebraic;
import f8.commands.Storable;
import f8.exceptions.BadArgumentTypeException;
import f8.exceptions.F8Exception;
import f8.exceptions.TooFewArgumentsException;
import f8.keyboard.Menu;
import f8.objects.Stackable;
import f8.objects.types.Lista;
import f8.objects.types.Literal;
import f8.platform.io.DIS;
import f8.platform.io.DOS;

public final class MENU extends NonAlgebraic {
	public MENU() {
		// Aqui estaba systemInstall
	}

	public int getID() {
		return 436;
	}

	public Storable getInstance() {
		return new MENU();
	}

	public void loadState(DIS ds) throws IOException {
	}

	public void saveState(DOS ds)  throws IOException {
	}

	public void exec(Object extern) throws F8Exception {
		

		if (Core.check(1)) {
			Command a = Core.peek(0);
			if (a instanceof Lista) {
				Core.push(new Literal("CST"));
				new STO().exec(null);
				showCustomMenu();
			} else {
				throw new BadArgumentTypeException(this);
			}
		} else {
			throw new TooFewArgumentsException(this);
		}
	}

	public String toString() {
		return ("MENU");
	}

	public static void showCustomMenu() throws F8Exception {
		
		Stackable cst = (Stackable) Core.lookup("CST");
		if (cst instanceof Lista) {
			Menu.createAndDisplayCustomMenu((Lista) cst);
		} else {
			throw new BadArgumentTypeException(new MENU());
		}
	}
}
