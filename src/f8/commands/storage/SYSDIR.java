package f8.commands.storage;

import java.io.IOException;

import f8.Core;
import f8.commands.NonAlgebraic;
import f8.exceptions.F8Exception;
import f8.objects.types.F8String;
import f8.platform.io.DIS;
import f8.platform.io.DOS;
import f8.platform.io.StMan;

public class SYSDIR extends NonAlgebraic {

	public int getID() {
		return 7007;
	}

	public void loadState(DIS ds) throws IOException {

	}

	public void saveState(DOS ds) throws IOException {

	}

	public void exec(Object extern) throws F8Exception {
		Core.push(new F8String(StMan.getRoot()));
	}

	public String toString() {
		return "SYSDIR";
	}

}
