package f8.commands.symb;

import java.io.IOException;
import java.util.Vector;

import antlr.collections.AST;
import f8.Core;
import f8.commands.Command;
import f8.commands.Dispatch1;
import f8.commands.Dispatch2;
import f8.commands.math.mkl;
import f8.exceptions.BadArgumentTypeException;
import f8.exceptions.CanNotIsolateExpressionException;
import f8.exceptions.F8Exception;
import f8.exceptions.NotDependOnException;
import f8.kernel.yacc.ObjectParserTokenTypes;
import f8.objects.Stackable;
import f8.objects.types.InfixExp;
import f8.objects.types.Lista;
import f8.platform.io.DIS;
import f8.platform.io.DOS;

public class SOLVE extends Dispatch2 implements ObjectParserTokenTypes {

	public SOLVE() {
	}

	public int getID() {
		return 7015;
	}
	public boolean isAlgebraic() {
		// TODO Auto-generated method stub
		return false;
	}
	public boolean isInfix() {
		// TODO Auto-generated method stub
		return false;
	}
	public boolean isOperator() {
		return false;
	}
	public void loadState(DIS ds) throws IOException {
		// TODO Auto-generated method stub
	}
	public void saveState(DOS ds) throws IOException {
		// TODO Auto-generated method stub
	}
	public String toString(){
		return "SOLVE";
	}
	public Stackable prfInfixExpInfixExp(InfixExp a, InfixExp b) throws F8Exception {
		if(b.getAST().getType()!=ID)
			throw new BadArgumentTypeException(this);
		
		AST astExp=a.getAST();
		Vector incognitas=mkl.incogOf(astExp);
		String incog=b.getAST().getText();
		//Debe contener la incognita
		if(!incognitas.contains(incog))
			throw new NotDependOnException(this);
		//Comprobamos si es un polinomio en incog
		solutions=new Vector();
		solve(astExp,incog);
		if(solutions.size()>0){
			if(solutions.size()==1){
				return new InfixExp((AST)solutions.elementAt(0));
			}
			for(int i=0;i<solutions.size();i++){
				solutions.setElementAt(new InfixExp((AST)solutions.elementAt(i)), i);
			}
			return new Lista(solutions);
		}
		throw new CanNotIsolateExpressionException(this);
	}
	/**
	 * Conjunto de soluciones
	 */
	Vector solutions;
	
	/**
	 * Intenta resolver la ecuacion 'exp=0' para var
	 * @param exp Expresion en formato AST
	 * @param var VNombre de la variable
	 * @throws F8Exception
	 */
	public boolean solve(AST exp, String var) throws F8Exception {
		AST child=exp.getFirstChild();
		switch(exp.getType()){
		case ID:{ // variable=0
			if(exp.getText().equals(var)){ //var=0
				solutions.addElement(mkl.num(0));
				return true;
			}
			break;
		}
		case NUMBER: //La expresion no depende de la variable
			break;
		case EQ: //exp es exp1=exp2, hacemos un solve(exp1-exp2,var)
			return solve(mkl.nor(MINUS,"-",exp.getFirstChild(),exp.getFirstChild().getNextSibling()),var);
		case MULT://exp es un productorio e1*e2*...en. Cada ei=0 es solucion tambien de exp.
			boolean ret=false;
			for(;child!=null;child=child.getNextSibling()){
				ret|=solve(child,var);
			}
			return ret;
		case DIV: //exp es num/deno. La solucion num=0 es tambien de exp=0
			return solve(child,var);
		case MINUS: //Si uno de los sumandos no depende de la variable, lo enviamos a solveSimple
			AST nega=child.getNextSibling();
			if(!mkl.incogOf(child).contains(var))
				return solveSimple(nega, child, var);
			else if(!mkl.incogOf(nega).contains(var))
				return solveSimple(child, nega, var);
			break;
		case PLUS: //Dividimos en dos sumandos, los que dependen de var y los que no
			AST nodepend=mkl.num(0);
			AST depend=mkl.num(0);
			while(child!=null){
				if(!mkl.incogOf(child).contains(var))
					nodepend=mkl.sum(nodepend, child);
				else
					depend=mkl.sum(depend, child);
				child=child.getNextSibling();
			}
			//Enviamos a solveSimple
			return solveSimple(depend, mkl.neg(nodepend), var);
		default:
			//Simple
			if(solveSimple(exp,mkl.num(0),var))
				return true;
			//Reducir
			if(solveReduce(exp, var))
				return true;
			//Divide
			if(solveDivide(exp, var))
				return true;
			AST expColected=mkl.colect(exp);
			if(!mkl.opEquals(exp, expColected)){
				return solve(exp,var);
			}
		}
		return false;
	}
	/**
	 * Soluciones simples. 
	 * @param lft Contiene una expresino que depende de la variable
	 * @param rht Expresion que _no_ depende de la variable.
	 * @param var Variable
	 * @return true si se pudo resolver
	 */
	public boolean solveSimple(AST lft, AST rht, String var) throws F8Exception{
		AST child=lft.getFirstChild();
		switch(lft.getType()){
		case MINUS:{
			AST nega=child.getNextSibling();
			if(child.getType()==ID && child.getText().equals(var)){
				solutions.addElement(mkl.sum(rht, nega));
				return true;
			}
			if(nega.getType()==ID && nega.getText().equals(var)){
				solutions.addElement(mkl.res(rht, child));
				return true;
			}
			break;
		}
		case DIV:{
			AST deno=child.getNextSibling();
			if(!mkl.incogOf(deno).contains(var)){
				return solveSimple(child,mkl.mul(rht, deno),var);
			}
			if(!mkl.incogOf(child).contains(var)){
				return solveSimple(deno, mkl.div(child, rht), var);
			}
			break;
		}
		case PLUS:{
			AST nodepend=mkl.num(0);
			AST depend=mkl.num(0);
			while(child!=null){
				if(!mkl.incogOf(child).contains(var))
					nodepend=mkl.sum(nodepend, child);
				else
					depend=mkl.sum(depend, child);
				child=child.getNextSibling();
			}
			if(depend.getType()==ID && depend.getText().equals(var)){
				solutions.addElement(mkl.res(rht, nodepend));
				return true;
			}
			break;
		}
		case MULT:{
			AST nodepend=mkl.num(1);
			AST depend=mkl.num(1);
			while(child!=null){
				if(!mkl.incogOf(child).contains(var))
					nodepend=mkl.mul(nodepend, child);
				else
					depend=mkl.mul(depend, child);
				child=child.getNextSibling();
			}
			if(depend.getType()==ID && depend.getText().equals(var)){
				solutions.addElement(mkl.div(rht, nodepend));
				return true;
			}
			break;
		}
		case CARET:{
			if(child.getType()==ID && child.getText().equals(var)){
				solutions.addElement(
						mkl.ele(rht, mkl.inv(child.getNextSibling()))
					);
				return true;
			}else{
				if(!mkl.incogOf(child.getNextSibling()).contains(var)){
					return solveSimple(child, mkl.ele(rht, mkl.inv(child.getNextSibling())),var);
				}else if(!mkl.incogOf(child).contains(var)){
					return solveSimple(
							child.getNextSibling(), 
							mkl.div(mkl.fca("LN", rht), mkl.fca("LN", child)), 
							var);
				}
			}
			break;
		}
		case ID:{
			if(lft.getText().equals(var)){
				solutions.addElement(rht);
			}else{
				Command c=Core.lookup(lft.getText());
				if(c instanceof Dispatch1){
					Dispatch1 d1=(Dispatch1)c;
					if(d1.undo()!=null){
						return solve(mkl.res(child, mkl.fca(d1.undo(), rht)),var);
					}
				}
			}
			break;
		}
		}
		return false;
	}
	
	public boolean solveReduce(AST exp, String var){
		return false;
	}
	
	public boolean solveDivide(AST exp, String var) throws F8Exception {
		AST child=exp.getFirstChild();
		switch(exp.getType()){
		case MINUS: //Si uno de los sumandos no depende de la variable, lo enviamos a solveSimple
			AST nega=child.getNextSibling();
			solve(mkl.res(mkl.uno, mkl.div(nega, child)),var);
			break;
		case PLUS: //Dividimos en dos sumandos, los que dependen de var y los que no
			AST nodepend=mkl.num(0);
			AST depend=mkl.num(0);
			while(child!=null){
				if(!mkl.incogOf(child).contains(var))
					nodepend=mkl.sum(nodepend, child);
				else
					depend=mkl.sum(depend, child);
				child=child.getNextSibling();
			}
			//Enviamos a solveSimple
			return solve(mkl.sum(mkl.uno, mkl.div(nodepend, depend)), var);
		}
		return false;
	}
	
}
