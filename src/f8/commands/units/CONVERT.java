package f8.commands.units;

import java.io.IOException;

import f8.Core;
import f8.commands.NonAlgebraic;
import f8.commands.Storable;
import f8.exceptions.BadArgumentTypeException;
import f8.exceptions.F8Exception;
import f8.exceptions.TooFewArgumentsException;
import f8.objects.Stackable;
import f8.objects.types.TempUnit;
import f8.objects.types.Unit;
import f8.platform.io.DIS;
import f8.platform.io.DOS;

public final class CONVERT extends NonAlgebraic {
	public CONVERT() {
		// Aqui estaba systemInstall
	}

	public int getID() {
		return 104;
	}

	public Storable getInstance() {
		return new CONVERT();
	}

	public void loadState(DIS ds) throws IOException {
	}

	public void saveState(DOS ds)  throws IOException {
	}

	public String toString() {
		return ("CONVERT");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.StkOb#exec()
	 */
	public void exec(Object extern) throws F8Exception {
		
		if (Core.check(2)) {
			Stackable a = Core.peek(1);
			Stackable b = Core.peek(0);
			if ((a instanceof Unit || a instanceof TempUnit)
					&& (b instanceof Unit || b instanceof TempUnit)) {
				Unit from = (Unit) a;
				Unit to = (Unit) b;
				Unit converted = from.convert(to);
				Core.pop(2);
				Core.push(converted);
			} else {
				throw new BadArgumentTypeException(this);
			}
		} else {
			throw new TooFewArgumentsException(this);
		}
	}
}
