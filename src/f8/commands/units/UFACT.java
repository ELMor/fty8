package f8.commands.units;

import java.io.IOException;

import f8.Core;
import f8.commands.NonAlgebraic;
import f8.commands.Storable;
import f8.exceptions.F8Exception;
import f8.exceptions.TooFewArgumentsException;
import f8.objects.Stackable;
import f8.objects.types.Unit;
import f8.platform.io.DIS;
import f8.platform.io.DOS;

public final class UFACT extends NonAlgebraic {
	public UFACT() {
		// Aqui estaba systemInstall
	}

	public int getID() {
		return 103;
	}

	public Storable getInstance() {
		return new UFACT();
	}

	public void loadState(DIS ds) throws IOException {
	}

	public void saveState(DOS ds)  throws IOException {
	}

	public String toString() {
		return ("UFACT");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.StkOb#exec()
	 */
	public void exec(Object extern) throws F8Exception {
		
		Stackable[] arg = new Stackable[2];
		arg[0] = new Unit(null);
		arg[1] = arg[0];
		String dsp = Core.dispatch(arg);
		if (dsp != null) {
			Unit from = (Unit) arg[0];
			Unit to = (Unit) arg[1];
			Core.push(from.ufact(to));
		} else {
			throw new TooFewArgumentsException(this);
		}
	}

}
