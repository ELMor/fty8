/*
 * Created on 24-sep-2003
 *
 
 
 */
package f8.exceptions;

import f8.commands.Command;
import f8.keyboard.TeclaVirtual;

/**
 * @author elinares
 * 
 * 
 * 
 */
public abstract class F8Exception extends Exception implements ICalcErr {
	Command command = null;

	TeclaVirtual tvirt = null;

	public F8Exception(TeclaVirtual v) {
		tvirt = v;
	}

	/**
	 * 
	 */
	public F8Exception(Command c) {
		super();
		command = c;
	}

	/**
	 * @param message
	 */
	public F8Exception(String message) {
		super(message);
	}

	public String getPartic() {
		return (command == null ? "" : command.toString())
				+ (tvirt == null ? "" : tvirt.getTitle());
	}

	public abstract int getNdx();

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		return Name[getNdx()];
	}
}
