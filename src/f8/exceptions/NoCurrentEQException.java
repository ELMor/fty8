/*
 * Created on 24-sep-2003
 *
 
 
 */
package f8.exceptions;

import f8.commands.Command;
import f8.keyboard.TeclaVirtual;

;

/**
 * @author elinares
 * 
 * 
 * 
 */
public class NoCurrentEQException extends f8.exceptions.F8Exception {

	/**
	 * 
	 */
	public NoCurrentEQException(Command c) {
		super(c);

	}

	/**
	 * @param message
	 */
	public NoCurrentEQException(String message) {
		super(message);

	}

	/**
	 * @param v
	 */
	public NoCurrentEQException(TeclaVirtual v) {
		super(v);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.rtExceptions.F8Exception#getNdx()
	 */
	public int getNdx() {
		return ICalcErr.NoCurrentEQ;
	}

}
