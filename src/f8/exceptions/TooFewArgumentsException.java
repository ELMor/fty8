/*
 * Created on 24-sep-2003
 *
 
 
 */
package f8.exceptions;

import f8.commands.Command;
import f8.keyboard.TeclaVirtual;

;

/**
 * @author elinares
 * 
 * 
 * 
 */
public class TooFewArgumentsException extends f8.exceptions.F8Exception {

	/**
	 * 
	 */
	public TooFewArgumentsException(Command c) {
		super(c);

	}

	/**
	 * @param message
	 */
	public TooFewArgumentsException(String message) {
		super(message);

	}

	/**
	 * @param v
	 */
	public TooFewArgumentsException(TeclaVirtual v) {
		super(v);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.rtExceptions.F8Exception#getNdx()
	 */
	public int getNdx() {
		return ICalcErr.TooFewArguments;
	}

}
