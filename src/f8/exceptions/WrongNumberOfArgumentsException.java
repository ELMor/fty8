/*
 * Created on 24-sep-2003
 *
 
 
 */
package f8.exceptions;

import f8.commands.Command;
import f8.keyboard.TeclaVirtual;

;

/**
 * @author elinares
 * 
 * 
 * 
 */
public class WrongNumberOfArgumentsException extends f8.exceptions.F8Exception {

	/**
	 * 
	 */
	public WrongNumberOfArgumentsException(Command c) {
		super(c);

	}

	/**
	 * @param message
	 */
	public WrongNumberOfArgumentsException(String message) {
		super(message);

	}

	/**
	 * @param v
	 */
	public WrongNumberOfArgumentsException(TeclaVirtual v) {
		super(v);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.rtExceptions.F8Exception#getNdx()
	 */
	public int getNdx() {
		return ICalcErr.WrongNumberOfArg;
	}

}
