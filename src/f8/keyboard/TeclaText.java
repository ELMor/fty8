package f8.keyboard;

import f8.exceptions.F8Exception;
import f8.platform.Calc;

public class TeclaText implements TeclaVirtual {
	String name;
	int backPos=0;
	public TeclaText(String n){
		name=n;
	}
	public TeclaText(String n, int b){
		this(n);
		backPos=b;
	}
	public String getEmit() {
		return name;
	}

	public String getTitle() {
		// TODO Auto-generated method stub
		return name;
	}

	public boolean isDir() {
		return false;
	}

	public boolean isVarSolver() {
		return false;
	}

	public void pressed() throws F8Exception {
		Calc.ref.emit(name, backPos);
	}
	
}
