/*
 * Created on 28-jul-2003
 *
 
 
 */
package f8.keyboard;

import f8.Core;
import f8.commands.storage.STO;
import f8.exceptions.F8Exception;
import f8.exceptions.TooFewArgumentsException;
import f8.objects.Stackable;
import f8.objects.types.Literal;
import f8.platform.Calc;

/**
 * @author elinares
 * 
 * 
 * 
 */
public final class TeclaVar extends Tecla {
	boolean isDir = false;

	public TeclaVar(String s, boolean _isDir) {
		super(s);
		isDir = _isDir;
	}

	public void setMenu() {
		Core.changeToDir(getEmit());
		//Core.setMenu(1, new VAR(), null, null);
	}

	public void pressed() throws F8Exception {
		switch (Calc.ref.getShiftMode()) {
		case 0:
			super.pressed();
			break;
		case 1:
			Calc.ref.flush();
			if (Core.check(1)) {
				STO.storeVar(getTitle(), Core.pop());
			} else {
				throw new TooFewArgumentsException(this);
			}
			break;
		case 2:
			Calc.ref.flush();
			Stackable cont = (Stackable) Core.lookup(getTitle());
			if (cont != null) {
				Core.push(cont);
			} else {
				Core.push(new Literal(getTitle()));
			}
			break;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.ui.TeclaVirtual#isDir()
	 */
	public boolean isDir() {
		return isDir;
	}
}
