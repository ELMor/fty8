/*
 * Created on 23-jul-2003
 *
 
 
 */
package f8.keyboard;

import f8.Core;

/**
 * @author elinares
 * 
 * 
 * 
 */
public class WithEnter extends Tecla {
	int setting = -1;

	int value = -1;

	String grpCheck = null;

	public WithEnter(String name) {
		super(name);
	}

	public WithEnter(String name, String emit) {
		super(name, emit);
	}

	public WithEnter(String nombre, String emit, String grp, int s, int d) {
		this(nombre, emit);
		grpCheck = grp;
		setting = s;
		value = d;
	}

	public boolean isChecked() {
		if ((setting < 0) || (setting >= Core.settings.length)) {
			return false;
		}
		return Core.getSetting(setting) == value;
	}

	public void setChecked(boolean v) {
		if ((setting < 0) || (setting >= Core.settings.length)) {
			return;
		}
		Core.setSettings(setting, value);
	}

}
