/*
 * Created on 22-jul-2003
 *
 
 
 */
package f8.keyboard.hp48.menu;

import f8.ICalc;
import f8.keyboard.Menu;
import f8.keyboard.WithEnter;

/**
 * @author elinares
 * 
 * 
 * 
 */
public final class MODES extends Menu {
	public MODES() {
		super("MODES");
		appendKey(new IO("EMPTY"));
		appendKey(new FONTS());
		appendKey(new WithEnter("STD", null, "DM", ICalc.DOU_MOD, ICalc.STD));
		appendKey(new WithEnter("FIX", null, "DM", ICalc.DOU_MOD, ICalc.FIX));
		appendKey(new WithEnter("SCI", null, "DM", ICalc.DOU_MOD, ICalc.SCI));
		appendKey(new WithEnter("ENG", null, "DM", ICalc.DOU_MOD, ICalc.ENG));

		appendKey(new WithEnter("SYM", "ChkSYM_", "SY", ICalc.SYM_MOD, 1));
		appendKey(new WithEnter("BEEP", "ChkBEEP_", "BE", ICalc.BEEP, 1));

		appendKey(new WithEnter("STK", "ChkSTK_", "ST", ICalc.LAS_STA, 1));
		appendKey(new WithEnter("ARG", "ChkARG_", "AR", ICalc.LAS_ARG, 1));
		appendKey(new WithEnter("CMD", "ChkCMD_", "CM", ICalc.LAS_CMD, 1));
		appendKey(new WithEnter("CNCT", "ChkCNC_", "CN", ICalc.CNCT, 1));
		appendKey(new WithEnter("ML", "ChkML_", "ML", ICalc.MULTILI, 1));
		appendKey(new WithEnter("CLK", "ChkCLK_", "CK", ICalc.CLOCK, 1));

		appendKey(new WithEnter("DEG", null, "AM", ICalc.ANG_MOD, ICalc.DEG));
		appendKey(new WithEnter("RAD", null, "AM", ICalc.ANG_MOD, ICalc.RAD));
		appendKey(new WithEnter("GRAD", null, "AM", ICalc.ANG_MOD, ICalc.GRAD));

		appendKey(new WithEnter("XYZ", "3D_1", "3D", ICalc.MOD_3D, ICalc.XYZ));
		appendKey(new WithEnter("RAZ", "3D_2", "3D", ICalc.MOD_3D, ICalc.RAZ));
		appendKey(new WithEnter("RAA", "3D_3", "3D", ICalc.MOD_3D, ICalc.RAA));

		appendKey(new WithEnter("HEX", null, "IM", ICalc.INT_MOD, ICalc.HEX));
		appendKey(new WithEnter("DEC", null, "IM", ICalc.INT_MOD, ICalc.DEC));
		appendKey(new WithEnter("OCT", null, "IM", ICalc.INT_MOD, ICalc.OCT));
		appendKey(new WithEnter("BIN", null, "IM", ICalc.INT_MOD, ICalc.BIN));

		appendKey(new WithEnter("FM,",  "0_0", "FM", ICalc.PER_MOD, 1));

		appendKey(new WithEnter("BUTT", "0_1", "BU", ICalc.ICAL_BUTT, 1));

		appendKey(new WithEnter("EXACT", "0_2", "EM", ICalc.EXACT_MODE, 1));
}
}
