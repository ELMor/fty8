package f8.modes;

import java.io.IOException;

import f8.ICalc;
import f8.commands.Storable;
import f8.exceptions.F8Exception;
import f8.platform.io.DIS;
import f8.platform.io.DOS;

public final class ENG extends CheckGroup {
	public ENG() {
		// Aqui estaba systemInstall
	}

	public int getID() {
		return 505;
	}

	public Storable getInstance() {
		return new ENG();
	}

	public void loadState(DIS ds) throws IOException {
	}

	public void saveState(DOS ds)  throws IOException {
	}

	public boolean execBefore() throws F8Exception {
		return setTipeAndPrecission(ICalc.ENG);
	}

	public String toString() {
		return ("ENG");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.mod.CheckGroup#getGroupName()
	 */
	public String getGroupName() {
		return "DM";
	}
}
