package f8.modes;

import java.io.IOException;

import f8.Core;
import f8.ICalc;
import f8.commands.Storable;
import f8.platform.Calc;
import f8.platform.io.DIS;
import f8.platform.io.DOS;

public final class RefButtons extends CheckGroup {
	public RefButtons() {
		// Aqui estaba systemInstall
	}

	public int getID() {
		return 7010;
	}

	public Storable getInstance() {
		return new RefButtons();
	}

	public void loadState(DIS ds) throws IOException {
	}

	public void saveState(DOS ds)  throws IOException {
	}

	public boolean execBefore() {
		Core.setSettings(ICalc.ICAL_BUTT, !Core.isSetting(ICalc.ICAL_BUTT));
		Calc.ref.pantalla.keyboardNeedRefresh=true;
		Calc.ref.pantalla.dynamicNeedRefresh=true;
		return false;
	}

	public String toString() {
		return ("0_1");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.mod.CheckGroup#getGroupName()
	 */
	public String getGroupName() {
		return "BU";
	}
}
