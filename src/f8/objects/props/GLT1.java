package f8.objects.props;

import java.io.IOException;

import f8.Core;
import f8.commands.Command;
import f8.exceptions.F8Exception;
import f8.objects.Stackable;
import f8.objects.types.F8String;
import f8.platform.io.DIS;
import f8.platform.io.DOS;

public class GLT1 extends Command {

	public GLT1() {
		// TODO Auto-generated constructor stub
	}

	public int getID() {
		// TODO Auto-generated method stub
		return 7004;
	}

	public boolean isAlgebraic() {
		// TODO Auto-generated method stub
		return false;
	}

	public boolean isInfix() {
		// TODO Auto-generated method stub
		return false;
	}

	public boolean isOperator() {
		// TODO Auto-generated method stub
		return false;
	}

	public void loadState(DIS ds) throws IOException {
		// TODO Auto-generated method stub

	}

	public void saveState(DOS ds) throws IOException {
		// TODO Auto-generated method stub

	}

	public void exec(Object extern) throws F8Exception {
		Core.check(1);
		Stackable st=Core.pop();
		Core.push(new F8String(st.getLaTeX()));
	}

	public String toString() {
		return "GLT1";
	}

}
