package f8.objects.types;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.IOException;

import f8.exceptions.BadArgumentTypeException;
import f8.exceptions.F8Exception;
import f8.objects.Stackable;
import f8.platform.io.DIS;
import f8.platform.io.DOS;

public class Grob extends Stackable {
	int height;
	int width;
	String content; 
	BufferedImage raster=null;

	public Grob() {
		this((String)null);
	}
	
	public BufferedImage getImage(){
		return raster;
	}

	public Grob(String c){
		if(c==null)
			return;
		init(c);
	}

	public Grob(BufferedImage im){
		raster=im;
		if(im!=null){
			height=im.getHeight(null); 
			width=im.getWidth(null);
		}
	}
	private void init(String c) {
		content=c;
		int aux=c.indexOf(" ",5);
		width=Integer.parseInt(c.substring(5,aux));
		int aux2=c.indexOf(" ", aux+1);
		height=Integer.parseInt(c.substring(aux+1,aux2));
		raster=new BufferedImage(width,height,BufferedImage.TYPE_INT_BGR);
		Graphics g=raster.getGraphics();
		g.setColor(new Color(201,201,201));
		g.fillRect(0, 0, width, height);
		g.setColor(new Color(0));
		int index=aux2+1,y,x;
		for(y=0;y<height;y++){
			for(x=0;x<width;x+=4){
				int b;
				if(index<c.length())
					b=Integer.parseInt(c.substring(index,index+1),16);
				else
					b=0;
				if((b & 0x01) == 1)
					g.drawLine(x,y,x,y);
				b>>=1;
				if((b & 0x01) == 1)
					g.drawLine(x+1,y,x+1,y);
				b>>=1;
				if((b & 0x01) == 1)
					g.drawLine(x+2,y,x+2,y);
				b>>=1;
				if((b & 0x01) == 1)
					g.drawLine(x+3,y,x+3,y);
				index++;
			}
			if((index&0x01)==1)
				index++;
		}
	}
	
	public Stackable copia() {
		return new Grob(new String(content));
	}
	public boolean decompose() throws F8Exception {
		throw new BadArgumentTypeException("GROB");
	}
	public String getLaTeX() {
		return "GROB "+width+"x"+height;
	}
	public String getTypeName() {
		return "GROB";
	}
	public int getID() {
		return 7005;
	}
	public String toString(){
		return "GROB "+width+"x"+height;
	}
	public void loadState(DIS ds) throws IOException {
		super.loadState(ds);
		init(ds.readStringSafe());
	}
	public void saveState(DOS ds)  throws IOException {
		super.saveState(ds);
		ds.writeStringSafe(content);
	}
	public int getHeight(){
		return height;
	}
	public int getWidth(){
		return width;
	}
}
