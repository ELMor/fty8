package f8.objects.types;

import java.io.IOException;

import antlr.collections.AST;
import f8.Core;
import f8.ICalc;
import f8.commands.Storable;
import f8.commands.alg.IntValue;
import f8.objects.Stackable;
import f8.platform.F8Math;
import f8.platform.io.DIS;
import f8.platform.io.DOS;

public final class Int extends Double implements IntValue {
	public int n;
	
	public Int(){
		this(0);
	}
	
	public Int(int n) {
		this.n = n;
	}
	public String getLaTeX() {
		return toString();
	}


	public Int(AST definition) {
		String def = definition.getText();
		String digits = def.substring(1, def.length() - 1);
		char type = def.charAt(def.length() - 1);
		switch (type) {
		case 'D':
		case 'd':
			n = F8Math.fromDEC(digits);
			break;
		case 'H':
		case 'h':
			n = F8Math.fromHEX(digits);
			break;
		case 'O':
		case 'o':
			n = F8Math.fromOCT(digits);
			break;
		case 'B':
		case 'b':
			n = F8Math.fromBIN(digits);
		}
	}

	public Stackable copia() {
		return new Int(n);
	}

	public String getTypeName() {
		return "Int";
	}

	public int getID() {
		return 26;
	}

	public Storable getInstance() {
		return new Int(1);
	}

	public void loadState(DIS ds) throws IOException {
		super.loadState(ds);
		n = ds.readInt();
	}

	public void saveState(DOS ds)  throws IOException {
		super.saveState(ds);
		ds.writeInt(n);
	}

	public Ereal[] complexValue() {
		Ereal[] x = new Ereal[2];
		x[0] = new Ereal(n);
		x[1] = Ereal.cero;

		return (x);
	}

	public Ereal doubleValue() {
		return new Ereal(n);
	}

	public int intValue() {
		return (n);
	}

	public String toString() {
		
		String retAfter = F8Math.format(n, Core.getSetting(ICalc.INT_MOD));
		switch (Core.getSetting(ICalc.INT_MOD)) {
		case 2:
			return "#" + retAfter + "h";
		case 3:
			return "#" + retAfter + "o";
		case 4:
			return "#" + retAfter + "b";
		}
		String ret = getTag();
		if (!ret.equals("")) {
			ret += ":";
		}
		return ret + "#" + retAfter + "d";
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.StkObType#decompose(f8.kernel.CL)
	 */
	public boolean decompose() {
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	public boolean equals(Object obj) {
		if (obj instanceof Int) {
			return n == ((Int) obj).n;
		}
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.Stackable#tolerance(java.lang.Object, double)
	 */
	public boolean tolerance(Object obj, double tol) {
		return equals(obj);
	}

}
