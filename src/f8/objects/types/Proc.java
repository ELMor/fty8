package f8.objects.types;

import java.io.IOException;
import java.util.Vector;

import f8.commands.Command;
import f8.commands.Storable;
import f8.commands.alg.CommandSequence;
import f8.commands.alg.Ejecutable;
import f8.commands.prog.FLECHA;
import f8.objects.Stackable;
import f8.platform.io.DIS;
import f8.platform.io.DOS;

public final class Proc extends CommandSequence implements Ejecutable {
	public Proc(Vector a) {
		super(a);
	}
	public Proc(){
		this(null);
	}
	public Stackable copia() {
		Vector pCopy = new Vector();
		for (int i = 0; i < obList.size(); i++) {
			if (obList.elementAt(i) instanceof Stackable) {
				pCopy.addElement(((Stackable) obList.elementAt(i)).copia());
			} else {
				pCopy.addElement(obList.elementAt(i));
			}
		}
		return new Proc(pCopy);
	}

	public String getTypeName() {
		return "Proc";
	}

	public int getID() {
		return 40;
	}

	public Storable getInstance() {
		return new Proc(null);
	}

	public void loadState(DIS ds) throws IOException {
		super.loadState(ds);
		int sz = ds.readInt();
		obList = new Vector();
		for (int i = 0; i < sz; i++) {
			obList.addElement((Command) Command.loadFromStorage(ds));
		}
	}

	public void saveState(DOS ds)  throws IOException {
		super.saveState(ds);
		ds.writeInt(obList.size());
		for (int i = 0; i < obList.size(); i++) {
			Command c = ((Command) (obList.elementAt(i)));
			ds.writeInt(c.getID());
			c.saveState(ds);
		}
	}

	public Command item(int i) {
		return (Command) (obList.elementAt(i));
	}

	public String toString() {
		String s = "\u00AB ";
		if(obList!=null){
			for (int i = 0; i < obList.size(); i++) {
				Command next = (Command) obList.elementAt(i);
				s += (next.toString() + " ");
			}
	
			s += "\u00BB";
		}
		String ret = getTag();
		if (!ret.equals("")) {
			ret += ":";
		}
		return ret + s;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.StkObType#decompose(f8.kernel.CL)
	 */
	public boolean decompose() {
		return super.decompose();
	}

	public int numOfLocalVars() {
		if (obList.elementAt(0) instanceof FLECHA) {
			return ((FLECHA) obList.elementAt(0)).size();
		}
		return 0;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.types.utils.Ejecutable#getEjecutable()
	 */
	public CommandSequence getEjecutable() {
		return this;
	}

}
