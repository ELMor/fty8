package f8.platform;

//Se procesan los eventos en otra hebra, de manera que no se bloquee la gestion de eventos.
public class QueuedAction {
	boolean user;
	int key;
	Character ascii;
	
	public QueuedAction(int k, boolean b, Character c){
		user=b;
		key=k;
		ascii=c;
	}
	public boolean isUser(){
		return user;
	}
	public int getAction(){
		return key;
	}
	public Character getChar(){
		return ascii;
	}
}
