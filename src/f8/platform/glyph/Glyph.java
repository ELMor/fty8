package f8.platform.glyph;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.image.BufferedImage;



public abstract class Glyph {
	
	//Colores
	public static Color background=new Color((201*256+201)*256+201);
	public static Color fillColor=new Color((102*256+102)*256+102);
	public static Color borderColor=new Color((138*256+138)*256+138);
	
	private int offx=0,offy=0;
	private boolean isTheCenter=false;
	Glyph padre;
	
	/**
	 * Dibuja este glypth
	 * @param g
	 */
	public abstract void dibuja(Graphics g);
	/**
	 * Se devuelve la altura total del nuevo Glyph
	 * @return
	 */
	public abstract int getHeight();
	
	/**anchura
	 * Anchura 
	 * @return
	 */
	public abstract int getWidth();
	
	/**
	 * Base para encadenar a la derecha glypths
	 * @return
	 */
	public abstract int getBase();

	/**
	 * obtiene la posicion relativa de este glypth respecto del padre.
	 * @return
	 */
	public int getX() {
		return offx;
	}

	public int getY() {
		return offy;
	}

	public void setX(int xx) {
		offx=xx;
	}

	public void setY(int yy) {
		offy=yy;
	}
	

	public Glyph(Glyph pad){
		padre=pad;
	}	

	public Glyph(Glyph cpad, boolean iscentered){
		this(cpad);
		isTheCenter=iscentered;
	}
	/**
	 * Se emplea para determinar si este Glyph es el centro de una serie (Horiz o Vertical,
	 * Aunque normalmente ser� Vertical la serie que necesite especificar su centro).
	 * @return
	 */
	public boolean isTheCenter(){
		return isTheCenter;
	}
	
	public void setTheCenter(boolean t){
		isTheCenter=t;
	}
	
	public int realX(){
		int sx=getX();
		Glyph c=padre;
		while(c!=null){
			sx+=c.getX();
			c=c.padre;
		}
		return sx;
	}

	public int realY(){
		int sy=getY();
		Glyph c=padre;
		while(c!=null){
			sy+=c.getY();
			c=c.padre;
		}
		return sy;
	}
	
	public BufferedImage getImage(Color bkg){
		BufferedImage ret=new BufferedImage(getWidth()+1, getHeight()+1,BufferedImage.TYPE_INT_RGB);
		Graphics g=ret.getGraphics();
		g.setColor(bkg);
		g.fillRect(0, 0, ret.getWidth()+1, ret.getHeight()+1);
		dibuja(g);
		return ret;
	}
	protected int px,py;
	protected void setPoint(int x, int y){
		px=x;
		py=y;
	}
	protected void lineRel(Graphics g, int masx, int masy, boolean translate){
		lineRel(g,masx,masy,translate,1);
	}
	protected void lineRel(Graphics g, int masx, int masy, boolean translate,int w){
		if(w==1){
			g.drawLine(px, py, px+masx, py+masy);
		}else{
			if(masx==0){
				for(int j=-w/2;j<w/2;j++){
					g.drawLine(px+j, py, px+j, py+masy);
				}
			}else if(masy==0){
				for(int j=-w/2;j<w/2;j++){
					g.drawLine(px, py+j, px+masx, py+j);
				}
			}else{
				double tgt=masy/masx/w;
				for(int j=-w/2;j<w/2;j++){
					g.drawLine(
							(int)(px+j/tgt), 
							(int)(py+j*tgt), 
							(int)(px+j/tgt+masx), 
							(int)(py+j*tgt+masy)
							);
				}
			}
		}
		if(translate){
			px+=masx;
			py+=masy;
		}
	}

}
