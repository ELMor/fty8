package f8.platform.glyph;

import java.awt.Graphics;

/**
 * Serie horizontal de Gs. Se van apilando hacia la derecha.
 * La 'base' de la serie coincide con la m�xima base de los componentes.
 * @author elinares
 *
 */
public class GlyphHFlow extends GlyphFlow {
	public GlyphHFlow(Glyph cpad){
		super(cpad);
	}
	public void dibuja(Graphics g) {
		Glyph max=new GlyphEmpty(null);
		//Buscamos el glyph con la maxima base
		for(int i=0;i<elems.size();i++){
			Glyph elem=((Glyph)elems.elementAt(i));
			if(max.getBase()<elem.getBase()){
				max=elem;
			}
		}
		int cx=0;
		int maxBase=max.getBase();
		for(int i=0;i<elems.size();i++){
			Glyph elem=((Glyph)elems.elementAt(i));
			elem.setX(cx);
			cx+=1+elem.getWidth();
			elem.setY(maxBase-elem.getBase());
		}
		super.dibuja(g);
	}
	public int getBase() {
		Glyph max=new GlyphEmpty(null);
		for(int i=0;i<elems.size();i++){
			Glyph elem=((Glyph)elems.elementAt(i));
			if(max.getBase()<elem.getBase()){
				max=elem;
			}
		}
		return max.getBase();
	}
	public int getHeight() {
		int ret=0;
		for(int i=0;i<elems.size();i++){
			ret=Math.max(ret, ((Glyph)elems.elementAt(i)).getHeight());
		}
		return ret;
	}
	public int getWidth() {
		int ret=0;
		for(int i=0;i<elems.size();i++){
			ret+=((Glyph)elems.elementAt(i)).getWidth();
		}
		return ret+elems.size();
	}
}