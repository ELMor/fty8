package f8.platform.glyph;

import java.awt.Graphics;

import antlr.collections.AST;
import f8.commands.math.mkl;

/**
 * Dibuja una Raiz n-esima. 
 * @author elinares
 *
 */
public class Glyph_Root extends Glyph {

	Glyph gBase,gRoot;
	boolean isSquareRoot=false;
	private static final int sq=3;
	
	public Glyph_Root(Glyph cpad, AST astBase, AST astExp, int sz, boolean parbase){
		super(cpad);
		//Comprobamos si hay parentesis en la base
		AST astExpNumerador=astExp.getFirstChild();
		AST astExpDenominador=astExpNumerador.getNextSibling();
		if(!mkl.opEquals(astExpNumerador, mkl.uno)){
			//Metemos la potencia como base de la raiz
			AST newExp=mkl.ele(astBase, astExpNumerador);
			if(parbase){
				gBase=new Glyph_Parentesis(this,newExp,sz);
			}else{
				gBase=new Glyph_AST(this,newExp,sz);
			}
		}else{
			gBase=new Glyph_AST(this,astBase,sz);
		}
		gRoot=new Glyph_AST(this,astExpDenominador,sz-2);
		if((gRoot instanceof Glyph_AST && ((Glyph_AST)gRoot).isTwo)){
			isSquareRoot=true;
			gRoot=new GlyphEmpty(this,sq,2*sq,sq);
		}
	}

	public int getHeight() {
		int hr=gRoot.getHeight();
		int hb=gBase.getHeight();
		if(hr<hb/2)
			return sq+hb;
		else
			return hr+hb/2;
	}

	public int getWidth() {
		return gRoot.getWidth()+gBase.getWidth()+3*sq;
	}

	public int getBase() {
		return getHeight()-gBase.getBase();
	}

	public void dibuja(Graphics g) {
		gRoot.setX(0);
		gRoot.setY(0);
		gBase.setX(gRoot.getWidth()+3*sq);
		int hr=gRoot.getHeight();
		int hb=gBase.getHeight();
		if(hr<hb/2)
			gBase.setY(sq);
		else
			gBase.setY(hr-hb/2);
		gBase.dibuja(g);
		if(!isSquareRoot)
			gRoot.dibuja(g);
		dibujaRaiz(g);
	}
	
	private void dibujaRaiz(Graphics g){
		int x=realX();
		int y=realY();
		int y2=0;
		int hr=gRoot.getHeight();
		int hb=gBase.getHeight();
		if(hr>hb/2)
			y2=hr-hb/2;
		g.setColor(fillColor);
		setPoint(x+getWidth(), y+y2);
		lineRel(g,-sq-gBase.getWidth(),0,true,1);
		lineRel(g, -sq, getHeight()-y2-1, true,1);
		lineRel(g, -sq, -gBase.getHeight()/2+1,true,1);
		lineRel(g, -sq, 0,true,1);
	}

}
