package f8.platform.glyph;

import java.awt.Color;
import java.awt.Graphics;

import f8.platform.F8StaticFonts;


public class Glyph_String extends Glyph {
	
	String txt;
	int fontSize;
	Color background=null;
	int height=0,width=0,base=0;
	
	public void dibuja(Graphics g) {
		if(background!=null){
			g.setColor(background);
			g.fillRect(realX(), realY(), getWidth()	, getHeight());
		}
		g.setColor(new Color(0));
		F8StaticFonts.getF8Font(fontSize).getViewer(txt).paint(realX(), realY(), g);
	}

	public Glyph_String(Glyph padre, String t, int sz){
		this(padre,t,null,sz);
	}
	
	public Glyph_String(Glyph padre, String t, Color bkg, int s){
		super(padre);
		background=bkg;
		txt=t;
		fontSize=s;
		fontSize=Math.max(10,fontSize);
		fontSize=Math.min(20,fontSize);
		height=F8StaticFonts.getF8Font(fontSize).getViewer(t).getHeight();
		width=F8StaticFonts.getF8Font(fontSize).getViewer(t).getWidth();
		base=height/2;
	}
	public int getBase() {
		return base;
	}
	public int getHeight() {
		return height;
	}
	public int getWidth() {
		return width;
	}
}
