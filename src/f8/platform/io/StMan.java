package f8.platform.io;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintStream;

public class StMan {
	private static String root="";
	
	public static String getRoot(){
		return root;
	}
	
	public static void init(){
	}
	
	public static boolean removeDir(String data){
		File[] f=new File(data).listFiles();
		for(File d : f){
			if(d.isDirectory())
				removeDir(d.getAbsolutePath());
			else
				d.delete();
		}
		return true;
	}
	
	public static boolean mkDir(String dirname){
		File f=new File(root+dirname);
		f.mkdir();
		return true;
	}
	
	public static InputStream getIS(String name){
		try {
			FileInputStream fis=new FileInputStream(new File(root+name));
			return fis;
		} catch (IOException e) {
			return null;
		}
	}
	
	public static OutputStream getOS(String name){
		try {
			FileOutputStream fos=new FileOutputStream(new File(name));
			return fos;
		} catch (IOException e) {
			return null;
		}
	}
	
	public static DIS getDIS(String name){
		InputStream is=getIS(name);
		if(is==null)
			return null;
		return new DIS(new DataInputStream(is));
	}

	public static DOS getDOS(String name){
		OutputStream os=getOS(name);
		if(os==null)
			return null;
		return new DOS(new DataOutputStream(os));
	}
	
	public static boolean exists(String name){
		return new File(name).exists();
	}

	public static boolean delete(String name){
		new File(name).delete();
		return true;
	}
	public static File[] list(String name){
		File f=new File(name);
		if(!f.exists())
			return null;
		if(!f.isDirectory())
			return null;
		return f.listFiles();
	}


	public static String loadFrom(String name){
		StringBuffer sb=new StringBuffer();
		char buf[]=new char[128];
		try {
			InputStream is=getIS(name);
			InputStreamReader isr=new InputStreamReader(is,"ISO-8859-1");
			int sz=0;
			while((sz=isr.read(buf))!=-1)
				sb.append(buf, 0, sz);
			is.close();
			return sb.toString();
		}catch(IOException ioe){
			return null;
		}
	}
	
	public static boolean saveTo(String name,String cnt){
		OutputStream os=getOS(name);
		if(os==null)
			return false;
		PrintStream ps=new PrintStream(os);
		ps.print(cnt);
		ps.close();
		return true;
	}
}
