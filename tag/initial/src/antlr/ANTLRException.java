package antlr;

/* ANTLR Translator Generator
 * Project led by Terence Parr at http://www.jGuru.com
 * Software rights: http://www.antlr.org/RIGHTS.html
 *
 * $Id: ANTLRException.java,v 1.5 2008/04/15 17:19:39 elinares690715 Exp $
 */
public class ANTLRException extends Exception {
	public ANTLRException() {
		super();
	}

	public ANTLRException(String s) {
		super(s);
	}
}
