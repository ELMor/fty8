package antlr.collections.impl;

/* ANTLR Translator Generator
 * Project led by Terence Parr at http://www.jGuru.com
 * Software rights: http://www.antlr.org/RIGHTS.html
 *
 * $Id: IntRange.java,v 1.5 2008/04/15 17:21:00 elinares690715 Exp $
 */
public class IntRange {
	int begin;

	int end;

	public IntRange(int begin, int end) {
		this.begin = begin;
		this.end = end;
	}

	public String toString() {
		return begin + ".." + end;
	}
}
