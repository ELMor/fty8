package f8;

import java.io.IOException;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Stack;
import java.util.Vector;

import antlr.TokenStreamException;
import antlr.collections.AST;
import f8.commands.Command;
import f8.commands.CommandDict;
import f8.commands.Storable;
import f8.commands.prog.debug.Debuggable;
import f8.commands.stk.DUP;
import f8.exceptions.F8Exception;
import f8.exceptions.ICalcErr;
import f8.objects.Stackable;
import f8.objects.types.Directory;
import f8.objects.types.InfixExp;
import f8.objects.types.Literal;
import f8.platform.Calc;
import f8.platform.io.DIS;
import f8.platform.io.DOS;

public class Core {
	// pila de diccionarios de sistema y usuario
	private static Stack dirStack;

	// Diccionario de usuario
	public static Directory homeDir;

	// Diccionario de sistema
	public static Directory systemDir;
	
	//Guarda una copia de la ultima pila
	private static Stack lastStack = null;

	// Pila de diccionarios de variables locales
	private static Stack localVarStack;

	// operator stack: RPN operators and data to be processed
	public static Stack userStack;

	//Para depurar programas
	public static Stack debugStack = null;

	// Modos de representacion
	public static int[] settings = new int[] 
		 { 0,              //ALG_MOD
		   ICalc.RAD,      //ANG_MOD
		   1,              //BEEP
		   1,              //CLOCK
		   1,              //CNCT
		   ICalc.STD,      //DOU_MOD
		   0,              //DOU_PRE
		   ICalc.DEC,      //INT_MOD
		   1,              //LAS_ARG
		   1,              //LAS_CMD
		   1,              //LAS_STA
		   1,			   //MULTILI
		   ICalc.XYZ,      //MOD_3D
		   ICalc.COMMA,    //PER_MOD
		   0,              //PRG_MOD
		   1,              //SYM_MOD
		   1,              //ICAL_BUTT
		   1			   //EXACT_MODE
		   };

	//Para notificar en pantalla
	private static INotify notificador=null; 
	
	// ELM:Ojo
	public static void init(INotify noti, DIS ds) throws IOException,IllegalAccessException {
		notificador=noti;
		userStack = new Stack();
		dirStack = new Stack();
		localVarStack = new Stack();
		debugStack = new Stack();
		systemDir = new Directory(null, "System");
		homeDir = new Directory(systemDir, "HOME");
/*		 Para cargar el diccionario de comandos estaticos
 * 		se le llama con un 'false' como parametro pero sin que el obfuscator
 * 		pueda detectarlos como parametro...
 */
		new CommandDict(systemDir.getNombre().equals("Sstem")); 
		dirStack.push(systemDir);
		dirStack.push(homeDir);
		if (ds!=null) {
			loadFromStorage(ds);
		}
	}

	/**
	 * Cambia al directorio
	 * 
	 * @param name
	 *            nombre del directorio al que cambiar
	 */
	public static void changeToDir(String dname) {
		Directory dir = null;

		try {
			dir = (Directory) currentDict().getHT().get(dname);
		} catch (Exception e) {
			dir = null;
		}

		if (dir != null) {
			dirStack.push(dir);
			// refreshStatusLabels();
		}
	}

	/**
	 * Comprueba que existen StkObs en opStack
	 * 
	 * @param depth
	 *            numero de StkObs a comprobar
	 * @return true si al menos hay depth StkObs en opStack
	 */
	public static boolean check(int n) {
		if (userStack.size() < n) {
			return (false);
		} else {
			return (true);
		}
	}

	/**
	 * Obtener el diccionario de usuario actual
	 * 
	 * @return Hashtable con los pares (String,StkOb)
	 */
	public static Directory currentDict() {
		int N = dirStack.size();

		return ((Directory) dirStack.elementAt(N - 1));
	}
	/**
	 * Devuelve el Diccionario
	 * 
	 * @param n
	 *            Numero de diccionario a devolver
	 * @return Diccionario local a devolver
	 */
	public static Hashtable dictPeek(int n) {
		int N = dirStack.size();

		return ((Directory) dirStack.elementAt(N - n - 1)).getHT();
	}

	/**
	 * Numero de entornos locales definidos en la calculadora
	 * 
	 * @return Numero de entornos de variables locales definidos
	 */
	public static  int dictStackSize() {
		return (dirStack.size());
	}

	/**
	 * devuelve un String que contiene el tipo de los argumentos tomados de la
	 * pila
	 * 
	 * @param nArg
	 *            Numero de argumentos a seleccionar
	 * @param arg
	 *            matriz que contiene los elementos sacados de la pila
	 * @return Tipos concatenados.
	 */
	public static  String dispatch(Stackable[] arg) {
		String ret = "";

		if (userStack.size() < arg.length) {
			return null;
		}

		for (int i = arg.length - 1; i >= 0; i--) {
			String tn = null;
			if (arg[i] != null) {
				tn = arg[i].getTypeName();
			}
			arg[i] = (Stackable) pop();
			String tnr = arg[i].getTypeName();
			if ((tn != null) && !(tn.equals(tnr))) {
				error(ICalcErr.BadArgumentType);
				return null;
			}
			ret += arg[i].getTypeName();
		}

		return ret;
	}

	/**
	 * Marca fin de entrada y comienzo de ejecucion de la cadena en edicion
	 * 
	 */
	public static  void enter(String input) throws F8Exception {
		if (input.equals("")) {
			new DUP().exec(null);
			return;
		}

		Vector vItems = new Vector();
		try {
			Stackable.parse(input, vItems);
			while (vItems.size() > 0) {
				AST nxtObj = (AST) vItems.elementAt(0);
				if (nxtObj != null) {
					Command it = Command.createFromAST(nxtObj);
					if (!(it instanceof Stackable) || (it instanceof Literal)) {
						it.exec(null);
					} else {
						it.store();
					}
				}
				vItems.removeElementAt(0);
			}
		}catch(TokenStreamException tse){
			Calc.ref.output(tse.getMessage());
		}
	}

	/**
	 * Notifica un error al ejecutar un StkOb
	 * 
	 * @param it
	 *            StkOb en ejecucion
	 * @param errCode
	 *            Codigo de error
	 */
	public  static void error(Command it, int n) {
		error(ICalcErr.Name[n] + "\n with <" + it.toString() + ">");
	}

	public static  void error(F8Exception f8e) {
		String com = f8e.getPartic();
		error(com + " Error: |" + f8e.toString());
	}

	public static  void error(int i) {
		notificador.output(ICalcErr.Name[i]);
	}

	public  static void error(String msg) {
		notificador.output(msg);
	}

	/**
	 * Obtiene el nombre del directorio actual en la forma "{HOME DIR1 DIR2 ... }"
	 * 
	 * @return Una lista con los directorios
	 */
	public static  String getCurrentDirName() {
		String dir = "{";

		for (int i = 1; i < dirStack.size(); i++) {
			if(!dir.equals("{"))
				dir+=" ";
			dir += (((Directory) dirStack.elementAt(i)).getNombre());
		}

		return dir + "}";
	}

	/**
	 * Entorno local actual
	 * 
	 * @return Hashtable con los pares (String, F8Ob).
	 */
	public  static Hashtable getCurrentLocalEnvir() {
		if (0 == localEnvirCount()) {
			return null;
		}

		return (Hashtable) localVarStack.peek();
	}

	/**
	 * @return
	 */
	public static  Debuggable getProcDebug() {
		return (Debuggable) debugStack.pop();
	}

	/**
	 * Devuelve el valor del set. Equivalente a los flags de la 48
	 * 
	 * @param setting
	 * @return
	 */
	public static  int getSetting(int setting) {
		return settings[setting];
	}

	/**
	 * Diccionario de usuario
	 * 
	 * @return El diccionario de usuario, donde se graban las variables de
	 *         usuario.
	 */
	public static  Hashtable getUserDict() {
		return homeDir.getHT();
	}

	/**
	 * Cambia a HOME
	 */
	public static  void homeDir() {
		Directory dir=currentDict();
		while(!dir.getNombre().equals("HOME")){
			dirStack.pop();
			dir=currentDict();
		}
	}

	public static  void install(Command it) {
		currentDict().put(it.toString(), it);
	}

	/**
	 * Coloca el setting. ver valores SET_*
	 * 
	 * @param setting
	 * @return
	 */
	public static  boolean isSetting(int setting) {
		return (settings[setting] == 1) ? true : false;
	}

	/**
	 * Devuelve el numero de anidamientos locales
	 * 
	 * @return
	 */
	public static  int localEnvirCount() {
		return localVarStack.size();
	}

	/**
	 * Devuelve el StkOb nombrado como name. Se busca primero en los locales,
	 * luego en el diccionario de usuario y finalmente en el diccionario de
	 * sistema
	 * 
	 * @param name
	 *            Nombre del StkOb
	 * @return el StkOb si lo hay o null si no lo encuentra
	 */
	public  static Command lookup(String name) {
		int n = localVarStack.size();
		Command x = null;

		// Primero buscamos en la pila de diccionarios locales
		while ((x == null) && (n > 0)) {
			Hashtable h = ((Hashtable) localVarStack.elementAt(--n));
			x = (Stackable) h.get(name);
			if (x != null)
				return x;
		}

		// luego en la de usuarios y en la de sistema
		n = dictStackSize();
		while (n > 0) {
			Hashtable h = ((Directory) dirStack.elementAt(--n)).getHT();
			x = (Command) h.get(name);
			if (x != null)
				return x;
		}

		// Finalmente en el diccionario de sistema
		return CommandDict.getInstance(name);
	}

	/**
	 * Busca en los objetos de usuario; por orden primero las locales, luego en
	 * el directorio actual y luego en cualquier directorio padre.
	 * 
	 * @param name
	 *            Nombre del objeto
	 * @param user
	 *            En la cadena de directorios
	 * @param userDefault
	 *            Solo en el directorio actual
	 * @param local
	 *            entre las variables locales
	 * @return
	 */
	public static  Command lookupUser(String name, boolean user, boolean userDefault,
			boolean local) {
		int n;
		Command x = null;
		if (local) {
			n = localVarStack.size();
			x = null;
			// Primero buscamos en la pila de diccionarios locales
			while ((x == null) && (n > 0)) {
				Hashtable h = ((Hashtable) localVarStack.elementAt(--n));
				x = (Stackable) h.get(name);
			}
			if (x != null) {
				return x;
			}
		}

		n = dictStackSize();
		if (userDefault) {
			Hashtable h = ((Directory) dirStack.elementAt(--n)).getHT();
			x = (Command) h.get(name);
			if (x != null) {
				return x;
			}
		}
		if (user) {
			// luego en la de usuarios (no en la de sistema:n>1)
			while ((x == null) && (n > 1)) {
				Hashtable h = ((Directory) dirStack.elementAt(--n)).getHT();
				x = (Command) h.get(name);
			}
		}

		return x;
	}

	/**
	 * Saca informacion sobre StkOb
	 * 
	 * @param it
	 *            StkOb sobre el que informar
	 */
	public static  void output(Command it) {
		notificador.output(it.toString());
	}

	public static  void pause(int seconds) {
		seconds = seconds + 1;
	}

	/**
	 * Idem peek(0)
	 * 
	 * @return StkOb topmost
	 */
	public static  Stackable peek() {
		return peek(0);
	}

	/**
	 * Lee (sin quitar de la pila) el StkOb en la posicion at, 0 es el topmost
	 * 
	 * @param at
	 *            posicion
	 * @return el StkOb de esa posicion
	 */
	public  static Stackable peek(int i) {
		int n = userStack.size();
		Stackable it = (Stackable) userStack.elementAt(n - 1 - i);
		return (it);
	}

	/**
	 * Sustitute el StkOb element en la posicion at
	 * 
	 * @param at
	 *            posicion en la que colocar, 0=topmost
	 * @param element
	 *            StkOb a colocar
	 */
	public static  void poke(int i, Command it) {
		int n = userStack.size();
		userStack.setElementAt(it, n - 1 - i);
	}

	/**
	 * Inserta el StkOb element en la posicion at
	 * 
	 * @param at
	 *            posicion en la que colocar, 0=topmost
	 * @param element
	 *            StkOb a colocar
	 */
	public static  void insertAt(int i, Command it) {
		int n = userStack.size();
		userStack.insertElementAt(it, n-1-i);
	}
	/**
	 * Retira el StkOb topmost de la pila
	 * 
	 * @return el StkOb retirado
	 */
	public static  Stackable pop() {
		Stackable ret = (Stackable) userStack.pop();

		if (ret == null) {
			error(ICalcErr.TooFewArguments);
		}

		return ret;
	}

	/**
	 * Retira n StkObs topmost de la pila
	 * 
	 * @param n
	 *            numero de StkObs a retirar
	 */
	public static  void pop(int n) {
		for (int i = 0; i < n; i++)
			userStack.pop();
	}

	/**
	 * Borrar el diccionario/entorno de variables locales actual
	 * 
	 */
	public static  void popLocalEnvir() {
		localVarStack.pop();
	}

	/**
	 * Coloca el StkOb it arriba de la pila (como topmost)
	 * 
	 * @param it
	 *            El StkOb a colocar
	 */
	public static  void push(Stackable it) {
		Stackable copy;
		if (it instanceof Literal) {
			copy = new InfixExp(it.getAST());
		} else {
			copy = it.copia();
		}
		copy.setTag(it.getTag());
		userStack.push(copy);
	}

	/**
	 * Crea un nuevo entorno (inner) de variables locales. Los STO que se
	 * produzcan se meteran aqu�. La idea es que cuando se ejecute endLocal se
	 * borren todas.
	 * 
	 */
	public  static void pushLocalEnvir() {
		Hashtable h = new Hashtable();
		localVarStack.push(h);
	}

	/**
	 * Restaura la pila guardada si la hay (intercambiamos las pilas)
	 * 
	 */
	public static  void restoreStack() {
		if (lastStack != null) {
			Stack aux = userStack;
			userStack = lastStack;
			lastStack = aux;
		}
	}

	/**
	 * Guarda una copia por referencia de la pila
	 * 
	 */
	public static  void saveStack() {
		lastStack = new Stack();
		int sz=userStack.size();
		lastStack.setSize(sz);
		for (int i = 0; i < sz; i++) {
			lastStack.setElementAt(userStack.elementAt(i), i);
		}
	}
	/**
	 * Carga desde almacenamiento fijo los datos de la calculadora
	 * 
	 * @param ds
	 *            DataStream desde/hacia el que van los datos
	 * @return El contenido de la linea de edicion
	 */
	public  static void loadFromStorage(DIS ds) throws IOException {
		// Cargamos los modos
		for (int i = 0; i < settings.length; i++) {
			settings[i] = ds.readInt();
		}
		// Cargamos el diccionario de Datos
		int sz = ds.readInt();
		for (int i = 0; i < sz; i++) {
			String key = ds.readStringSafe();
			Storable val = Command.loadFromStorage(ds);
			homeDir.put(key, val);
		}
		// Y el Stack
		sz = ds.readInt();
		for (int i = 0; i < sz; i++) {
			userStack.push(Command.loadFromStorage(ds));
		}
	}

	/**
	 * Graba el estado y datos de la calculadora logica en el stream
	 * 
	 * @param edit
	 *            el contenido actual de la linea de edicion
	 * @param ds
	 *            DataStream desde/hacia el que van los datos
	 */
	public  static void saveToStorage(DOS ds) throws IOException {
		// Grabamos los modos
		for (int i = 0; i < settings.length; i++) {
			ds.writeInt(settings[i]);
		}
		// Grabamos el diccionario de datos
		Vector keys = new Vector();
		for (Enumeration e = homeDir.getHT().keys(); e.hasMoreElements();)
			keys.addElement(e.nextElement());
		int sz = keys.size();
		ds.writeInt(sz);
		for (int i = 0; i < sz; i++) {
			String skey = (String) keys.elementAt(i);
			ds.writeStringSafe(skey);
			Command value = (Command) homeDir.get(skey);
			ds.writeInt(value.getID());
			value.saveState(ds);
		}
		// Ahora opStack
		sz = userStack.size();
		ds.writeInt(sz);
		for (int i = 0; i < sz; i++) {
			Command it = (Command) userStack.elementAt(i);
			ds.writeInt(it.getID());
			it.saveState(ds);
		}
	}

	/**
	 * Sustituye el Stack de operaciones por opSt
	 * 
	 * @param opSt
	 */
	public static  void setOpStack(Stack ops) {
		userStack = ops;
	}

	/**
	 * @param proc
	 */
	public static  void setProcDebug(Debuggable proc) {
		debugStack.push(proc);
	}

	public static  void setSettings(int setting, boolean val) {
		settings[setting] = val ? 1 : 0;
	}

	public static  void setSettings(int setting, int val) {
		settings[setting] = val;
	}

	/**
	 * Devuelve el tama�o de la pila
	 * 
	 * @return Numero de StkObs en la pila
	 */
	public static  int size() {
		return (userStack.size());
	}

	public  static String stackToString() {
		StringBuffer s = new StringBuffer();

		for (int i = 0; i < userStack.size(); i++) {
			Command it = (Command) userStack.elementAt(i);
			s.append(it.toString());
			s.append('\n');
		}

		return (s.toString());
	}

	public String toString() {
		String s = new String();

		for (int i = 0; i < userStack.size(); i++) {
			Command it = (Command) userStack.elementAt(i);
			s += ((userStack.size() - i) + ": " + it.toString() + "\n");
		}

		return (s);
	}


	/**
	 * Sube un directorio
	 */
	public static  void upDir() {
		if (!currentDict().getNombre().equals("HOME")) {
			dirStack.pop();
		}
	}


}
