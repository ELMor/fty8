package f8;

public interface IMachineStatus {
	public final int SM_SHIFT=0;
	public final int SM_PGM=1;
	public final int SM_ALG=2;
	public final int SM_KEYBNDX=3;
	public final int SM_DYNPAG=4;
	public final int SM_DYNNDX=5;
	public final int SM_SCREENMODE=6;
	public final int SM_ISEDITING=7;
	
	int[] getMachineStatus();
}
