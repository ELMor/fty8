/*
 * Created on 08-oct-2003
 *
 
 
 */
package f8.apps.display;


import java.io.IOException;

import f8.commands.Command;
import f8.commands.Storable;
import f8.exceptions.F8Exception;
import f8.exceptions.UnimplementedException;
import f8.platform.io.DIS;
import f8.platform.io.DOS;

/**
 * @author elinares
 * 
 * 
 * 
 */
public class AUTO extends Command {
	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.platform.Storable#getID()
	 */
	public int getID() {
		return 6002;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.platform.Storable#getInstance()
	 */
	public Storable getInstance() {
		return this;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.platform.Storable#loadState(f8.platform.DataStream)
	 */
	public void loadState(DIS ds) throws IOException {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.platform.Storable#saveState(f8.platform.DataStream)
	 */
	public void saveState(DOS ds)  throws IOException {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.Command#isAlgebraic()
	 */
	public boolean isAlgebraic() {
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.Command#isInfix()
	 */
	public boolean isInfix() {
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.Command#isOperator()
	 */
	public boolean isOperator() {
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.Operation#exec()
	 */
	public void exec(Object extern) throws F8Exception {
		PPAR.initPPAR();
		throw new UnimplementedException(this);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		return "AUTO";
	}
}
