/*
 * Created on 08-oct-2003
 *
 
 
 */
package f8.apps.display;
import java.util.Hashtable;

import f8.exceptions.BadArgumentTypeException;
import f8.exceptions.F8Exception;
import f8.objects.types.Ereal;
import f8.objects.types.Matrix;

/**
 * @author elinares Gestiona sistemas de coordenadas de dimension generica y las
 *         trasformaciones de coordenadas de puntos y vectores. Supongamos el
 *         siguiente escenario que contiene 2 sistemas de coordenadas, uno "lcd"
 *         y otro "usr" x O1 +-------> | | ^ y | |y' .P | | | +-----> x' V O2
 * 
 * El punto P tiene dos representaciones de coordenadas, una usando O1 y otra
 * usando O2; respectivamente (Px,Py) y (Px',Py'). Esta clase gestiona la
 * transformación de las mismas de la siguente forma:
 * 
 * CSys lcd=new CSys("lcd"), usr=new CSys("usr");
 * 
 * Creamos la transformación:
 * 
 *                        [[1 0]   [[O2x] 
 * lcd.setTransform("usr", [0 -1]], [O2y]] );
 * 
 * Siendo las matrices de rotacion y desplazamiento respectivamente segun la ley
 * general del algebra.
 * 
 * Ejemplos de uso: CSys.Point p=lcd.coor(1,2); CSys.Vector v=usr.vector(0,2);
 * CSys.Point r=p.plus(v);
 * 
 * Estamos sumando a un punto 'p' de coordenadas 1,2 en el sistema de
 * coordenadas lcd, el vector v que esta definido en el sistema de coordenadas
 * usr. El resultado por defecto se transforma al sistema inicial (lcd).
 * 
 */
public class CSys {
	private static Hashtable linked = new Hashtable();

	private Vector[] esq = new Vector[4];

	private String nombre;

	public CSys(String name) {
		nombre = name;
		linked.put(nombre, this);
	}

	public static CSys getCoorSystem(String s) {
		return (CSys) linked.get(s);
	}

	public Point bottom() {
		return esq[2].plus(esq[3]).div(Ereal.dos).toPoint();
	}

	public Point center(){
		return esq[0].plus(esq[3]).div(Ereal.dos).toPoint();
	}
	
	public Point coor(int x, int y) {
		Ereal[] c = new Ereal[2];
		c[0] = new Ereal(x);
		c[1] = new Ereal(y);
		return new Point(c);
	}

	public Point coor(Ereal x, Ereal y, Ereal z) {
		Ereal[] c = new Ereal[3];
		c[0] = x;
		c[1] = y;
		c[2] = z;
		return new Point(c);
	}

	public Point coor(Ereal[] p) {
		Ereal[] dret = new Ereal[p.length];
		for (int i = 0; i < p.length; i++) {
			dret[i] = p[i];
		}
		return new Point(dret);
	}

	public String getCSysName() {
		return nombre;
	}

	public Transform getTransform(Point in) {
		return (Transform) linked.get(in.getCSysName() + "." + nombre);
	}

	public Transform getTransform(Vector in) {
		return (Transform) linked.get(in.getCSysName() + "." + nombre);
	}

	public Ereal height() {
		return esq[0].minus(esq[2]).abs();
	}

	public Point left() {
		return esq[0].plus(esq[2]).div(Ereal.dos).toPoint();
	}

	public Point right() {
		return esq[1].plus(esq[3]).div(Ereal.dos).toPoint();
	}

	public void setRectangle(Vector ul, Vector ur, Vector dl, Vector dr) {
		esq[0] = ul;
		esq[1] = ur;
		esq[2] = dl;
		esq[3] = dr;
	}

	public void setTransform(String other, double[][] rotate, double[][] despl)
			throws F8Exception {
		Transform direct = new Transform(rotate, despl);
		Transform inverse = direct.inv();
		linked.put(nombre + "." + other, direct);
		linked.put(other + "." + nombre, inverse);
	}

	public Point top() {
		return esq[0].plus(esq[1]).div(Ereal.dos).toPoint();
	}

	public Point transform(Point input){
		if (nombre.equals(input.getCSysName())) {
			return input;
		}
		Transform t = getTransform(input);
		Matrix Minput = input.getAsMatrix();
		try {
			return new Point(t.rotacion.times(Minput).plus(t.translacion));
		} catch (F8Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public Vector transform(Vector input)  {
		if (nombre.equals(input.getCSysName())) {
			return input;
		}
		Transform t = getTransform(input);
		Matrix Minput = input.getAsMatrix();
		Vector ret=null;
		try {
			ret=new Vector(t.rotacion.times(Minput));
		}catch(BadArgumentTypeException iae){
			iae.printStackTrace();
		}
		return ret; 
	}

	public Vector vector(int x, int y){
		Ereal[] c = new Ereal[2];
		c[0] = new Ereal(x);
		c[1] = new Ereal(y);
		return new Vector(c);
	}
	
	public Vector vector(Ereal x, Ereal y) {
		Ereal[] c = new Ereal[2];
		c[0] = x;
		c[1] = y;
		return new Vector(c);
	}

	public Vector vector(double x, double y){
		Ereal[] c = new Ereal[2];
		c[0] = new Ereal(""+x);
		c[1] = new Ereal(""+y);
		return new Vector(c);
	}
	
	public Vector vector(Ereal x, Ereal y, Ereal z) {
		Ereal[] c = new Ereal[3];
		c[0] = x;
		c[1] = y;
		c[2] = z;
		return new Vector(c);
	}

	public Ereal width() {
		return esq[0].minus(esq[1]).abs();
	}

	public abstract class BPV {
		public Ereal[] v;

		protected BPV(Ereal[] p) {
			v = p;
		}

		protected BPV(Matrix m) {
			v = new Ereal[m.r];
			for (int i = 0; i < m.r; i++) {
				v[i] = new Ereal(""+m.x[i][0]);
			}
		}

		public boolean equals(BPV c) {
			for (int i = 0; i < c.v.length; i++) {
				if (v[i] != c.v[i]) {
					return false;
				}
			}
			return true;
		}

		public Matrix getAsMatrix() {
			double[][] aux = new double[v.length][1];
			for (int i = 0; i < v.length; i++) {
				aux[i][0] = Double.parseDouble(v[i].toString());
			}
			return new Matrix(aux);
		}

		public String getCSysName() {
			return nombre;
		}
	}

	public class Point extends BPV {
		protected Point(Ereal[] p) {
			super(p);
		}

		protected Point(Matrix m) {
			super(m);
		}

		public Point copy() {
			Ereal[] dret = new Ereal[v.length];
			for (int i = 0; i < v.length; i++) {
				dret[i] = v[i];
			}
			return new Point(dret);
		}

		public Point minus(Vector coor) throws F8Exception {
			if (!nombre.equals(coor.getCSysName())) {
				coor = transform(coor);
			}
			Ereal[] dret = new Ereal[v.length];
			for (int i = 0; i < dret.length; i++) {
				dret[i] = v[i].minus(coor.v[i]);
			}
			return new Point(dret);
		}

		public Vector minus(Point p) {
			if (!nombre.equals(p.getCSysName())) {
				p = transform(p);
			}
			Ereal[] dret = new Ereal[v.length];
			for (int i = 0; i < dret.length; i++) {
				dret[i] = v[i].minus(p.v[i]);
			}
			return new Vector(dret);
		}
		
		public Point plus(Vector coor) {
			if (!nombre.equals(coor.getCSysName())) {
				coor = transform(coor);
			}
			Ereal[] dret = new Ereal[v.length];
			for (int i = 0; i < dret.length; i++) {
				dret[i] = v[i].plus(coor.v[i]);
			}
			return new Point(dret);
		}
		public Point nextLeft() {
			Vector up=left().minus(right());
			up=up.div(up.abs());
			return this.plus(up);
		}
		public Point nextRight(){
			Vector up=right().minus(left());
			up=up.div(up.abs());
			return this.plus(up);
		}
		public Point nextUp() {
			Vector up=top().minus(bottom());
			up=up.div(up.abs());
			return this.plus(up);
		}
		public Point nextDown() {
			Vector up=bottom().minus(top());
			up=up.div(up.abs());
			return this.plus(up);
		}
	}

	public class Transform {
		Matrix rotacion;

		Matrix translacion;

		public Transform(double[][] m, double[][] v) {
			rotacion = new Matrix(m);
			translacion = new Matrix(v);
		}

		public Transform inv() throws F8Exception {
			Matrix Tprima = rotacion.inverse();
			Matrix oprima = Tprima.times(translacion).uminus();
			return new Transform(Tprima.x, oprima.x);
		}
	}

	public class Vector extends BPV {
		protected Vector(Ereal[] p) {
			super(p);
		}

		protected Vector(Matrix m) {
			super(m);
		}

		public Ereal abs() {
			Ereal ret = Ereal.cero;
			for (int i = 0; i < v.length; i++) {
				ret = ret.plus(v[i].sqr());
			}
			return ret.sqrt();
		}

		public Vector div(Ereal d) {
			Ereal[] dret = new Ereal[v.length];
			for (int i = 0; i < dret.length; i++) {
				dret[i] = v[i].div(d);
			}
			return new Vector(dret);
		}

		public Vector minus(Vector coor) {
			if (!nombre.equals(coor.getCSysName())) {
				coor = transform(coor);
			}
			Ereal[] dret = new Ereal[v.length];
			for (int i = 0; i < dret.length; i++) {
				dret[i] = v[i].minus(coor.v[i]);
			}
			return new Vector(dret);
		}

		public Vector plus(Vector coor) {
			if (!nombre.equals(coor.getCSysName())) {
				coor = transform(coor);
			}
			Ereal[] dret = new Ereal[v.length];
			for (int i = 0; i < dret.length; i++) {
				dret[i] = v[i].plus(coor.v[i]);
			}
			return new Vector(dret);
		}

		public Vector mul(Ereal p) {
			Ereal[] dret = new Ereal[v.length];
			for (int i = 0; i < v.length; i++)
				dret[i] = v[i].mul(p);
			return new Vector(dret);
		}

		public Point toPoint() {
			return new Point(v);
		}
	}
}
