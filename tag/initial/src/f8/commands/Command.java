package f8.commands;

import java.io.IOException;
import java.util.Vector;

import antlr.CommonAST;
import antlr.collections.AST;
import f8.Core;
import f8.commands.alg.CommandSequence;
import f8.commands.prog.FLECHA;
import f8.commands.prog.brch.DOUNTIL;
import f8.commands.prog.brch.FORNEXT;
import f8.commands.prog.brch.IF;
import f8.commands.prog.brch.IFERR;
import f8.commands.prog.brch.WHILEREPEAT;
import f8.exceptions.F8Exception;
import f8.kernel.yacc.ObjectParserTokenTypes;
import f8.objects.Stackable;
import f8.objects.types.Complex;
import f8.objects.types.Directory;
import f8.objects.types.Double;
import f8.objects.types.Ereal;
import f8.objects.types.F8String;
import f8.objects.types.Grob;
import f8.objects.types.InfixExp;
import f8.objects.types.Int;
import f8.objects.types.Lista;
import f8.objects.types.Literal;
import f8.objects.types.Matrix;
import f8.objects.types.Proc;
import f8.objects.types.TempUnit;
import f8.objects.types.Unit;
import f8.platform.io.DIS;
import f8.platform.io.DOS;


public abstract class Command extends Operation 
	implements Storable, ObjectParserTokenTypes {

	/**
	 * Crea un array de objetos HP48 hijos de ast
	 * 
	 * @param ast
	 *            El padre cuyos hijos se van a devolver en un array
	 * @return Array de hijos de ast
	 */
	public static Vector arrayFromChilds(AST ast) {
		Vector f8oa = new Vector();
		for (ast = ast.getFirstChild(); ast != null; ast = ast.getNextSibling()) {
			f8oa.addElement(createFromAST(ast));
		}

		return f8oa;
	}

	/**
	 * Usada para crear un objeto HP48 desde un arbol de compilacion antlr
	 * 
	 * @param tree
	 *            Nodo raiz del arbol de compilacion
	 * @return Objeto HP48 que lo representa, o null si no se reconoce.
	 */
	public static Command createFromAST(AST def) {
		Command ret = null;
		
		AST child = def.getFirstChild();

		switch (def.getType()) {
		case GROB:
			ret=new Grob(def.getText());
			break;
		case TAG:
			String tag = child.getText();
			ret = createFromAST(child.getNextSibling());
			((Stackable) ret).setTag(tag);
			break;
		case UNIT:
			AST unit = child.getNextSibling();
			if ((unit.getNumberOfChildren() == 0)
					&& ((unit.getText().startsWith("�") && (unit.getText()
							.length() > 1)) || unit.getText().equals("K"))) {
				ret = new TempUnit(child);
			} else {
				ret = new Unit(child);
			}
			break;
		case OPERADOR:
			ret = Core.lookup(child.getText());
			if (ret == null) {
				ret = new Literal(child.getText());
			}

			break;
		case ID:
			ret = new Literal(def.getText());
			break;
		case INFIX:
			ret = new InfixExp(def);
			break;
		case INTNUMBER:
			ret = new Int(def);
			break;
		case LITERAL:
			ret = new Literal(child.getText());
			break;
		case LOCALVAR:
			ret = new FLECHA(def);
			break;
		case MATRIZ:
			double[][] v3 = new double
				 [def.getNumberOfChildren()]
				 [def.getFirstChild().getNumberOfChildren()];
			int i3 = 0;
			int j3 = 0;
			AST ia3;
			AST ja3;

			for (ia3 = def.getFirstChild(); ia3 != null; ia3 = ia3
					.getNextSibling(), i3++) {
				for (ja3 = ia3.getFirstChild(), j3 = 0; ja3 != null; ja3 = ja3
						.getNextSibling(), j3++) {
					v3[i3][j3] = java.lang.Double.parseDouble(ja3.getText());
				}
			}

			ret = new Matrix(v3);
			break;
		case NUMBER:
			ret = new Double(new Ereal(def.getText()));
			break;
		case LIST:
			ret = new Lista(arrayFromChilds(def));
			break;
		case PROGRAM:
			ret = new Proc(arrayFromChilds(child));
			break;
		case DIRECTORIO:
			ret = new Directory(arrayFromChilds(child));
			break;
		case STRING_LITERAL:
			ret = new F8String(def.getText().substring(1,
					def.getText().length() - 1));
			break;
		case VECTOR:
			Ereal[] v2 = new Ereal[def.getNumberOfChildren()];
			int i2 = 0;

			for (AST a2 = def.getFirstChild(); a2 != null; a2 = a2
					.getNextSibling(), i2++) {
				v2[i2] = new Ereal(a2.getText());
			}

			ret = new f8.objects.types.F8Vector(v2);

			break;
		case IF:
		case IFELSE:
			ret = new IF(def);
			break;
		case IFERR:
			ret = new IFERR(def);
			break;
		case FORNEXT:
		case FORSTEP:
			ret = new FORNEXT(def);
			break;
		case DOUNTIL:
			ret = new DOUNTIL(def);
			break;
		case WHILEREPEAT:
			ret = new WHILEREPEAT(def);
			break;
		case SEQOB:
			ret = new CommandSequence(def);
			break;
		case COMPLEX:
			ret = new Complex(
					new Ereal(child.getText()), 
					new Ereal(child.getNextSibling().getText())
					);
			break;
		default:
			ret = Core.lookup(def.getText());
			break;
		}

		return ret;
	}

	/**
	 * Para obtener listados de los codigos ID e Items ya definidos
	 */

	/*
	 * public static void listItems() { for (int i = 0; i < 128; i++) { if
	 * (classes[i] != null) { StkOb iy = (StkOb) classes[i];
	 * System.out.println(iy.getClass().getName() + "=" + i); } else {
	 * System.out.println(""); } } }
	 */

	/**
	 * Comportamiento por defecto de un load desde un DataStream
	 * 
	 * @param ds
	 *            DataStream desde el que cargar
	 * @return una interfaz Storable
	 */
	public static Command loadFromStorage(DIS ds) throws IOException {
		int id = ds.readInt();

		if (id == 0) {
			return null;
		}

		Command obj = CommandDict.getInstance(id);
		
		if(obj!=null){
			obj.loadState(ds);
		}else{
			obj=new F8String("Invalid OBJ.id:"+id);
		}
		return obj;
	}

	public static void loadState(DIS ds, AST[] node) throws IOException {
		byte check = ds.readByte();
		if (check == (byte) 0) {
			node[0] = null;
			return;
		}
		node[0] = new CommonAST();
		node[0].initialize(ds.readInt(), ds.readStringSafe());

		int numChilds = ds.readInt();

		AST[] child = new CommonAST[1];
		for (int i = 0; i < numChilds; i++) {
			loadState(ds, child);
			node[0].addChild(child[0]);
		}
	}

	public static void saveState(DOS ds, AST node) throws IOException{
		if (node == null) {
			ds.writeByte((byte) 0);
			return;
		} else {
			ds.writeByte((byte) 1);
		}
		ds.writeInt(node.getType());
		ds.writeStringSafe(node.getText());
		ds.writeInt(node.getNumberOfChildren());

		for (AST child = node.getFirstChild(); child != null; child = child
				.getNextSibling()) {
			saveState(ds, child);
		}
	}

	/**
	 * Comportamiento por defecto de exec es un push
	 * 
	 */
	public void exec(Object extern) throws F8Exception {
		Core.push((Stackable) this);
	}

	public abstract int getID();

	public abstract boolean isAlgebraic();

	public abstract boolean isInfix();

	public abstract boolean isOperator();

	public abstract void loadState(DIS ds) throws IOException ;

	public abstract void saveState(DOS ds) throws IOException;

	/**
	 * Comportamiento por defecto de un store es un push
	 * 
	 */
	public void store() throws F8Exception {
		Stackable c;
		try {
			c = (Stackable) this;
			Core.push(c);
		} catch (ClassCastException cce) {
			this.exec(null);
		}
	}
	/**
	 * Devuelve la representacion LaTeX de este comando
	 * @param child Posibles hijos del comando
	 * @return Cadena LaTeX
	 */
	public String getLaTeX(AST child){
		return toString()+"\\left("+InfixExp.getLaTex(child)+"\\right)";
	}
}
