package f8.commands;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Hashtable;

import f8.platform.Properties;
import f8.platform.io.StMan;


public final class CommandDict {
	private static Hashtable<String, Entry> dictID=new Hashtable<String, Entry>();
	private static Hashtable<String, Entry> dictToken=new Hashtable<String, Entry>();
	private static StringBuffer sb=null;
	private static int numClasses=0;
	private static boolean generatePropFile;
	
	public class Entry {
		public String className;
		public int tipo;
		public int id;
		public String token;

		public Entry(String className, int tipo, int id, String token) {
			this.className = className;
			this.tipo = tipo;
			this.id = id;
			this.token = token;
			Entry act=null;
			String ids=Integer.toString(id);
			if(generatePropFile){
				act=(Entry)dictID.get(ids);
				if(act!=null)
					System.err.println("ID Duplicados "+act.className+" y "+className);
				act=(Entry)dictToken.get(token);
				if(act!=null)
					System.err.println("Token Duplicados "+act.className+" y "+className);
				//Comprobamos que es instanciable por newInstance:
				try {
					Command k=(Command)Class.forName(className).newInstance();
				}catch(Exception e){
					System.err.println("No instanciable "+className+" falta constructor sin args");
				}
				String clasn="c."+(++numClasses)+".";
				sb.append(clasn+"na="+className+"\n");
				sb.append(clasn+"ti="+tipo+"\n");
				sb.append(clasn+"id="+id+"\n");
				sb.append(clasn+"to="+transform(token)+"\n");
			}
			//Indexamos por ID
			dictID.put(ids, this);
			//Indexamos por token
			dictToken.put(token,this);
		}
	};

	private String transform(String in){
		String ret="";
		for(int i=0;i<in.length();i++)
			ret+=Integer.toHexString(in.charAt(i));
		return ret;
	}
	
	private String retTransform(String in){
		String ret="";
		for(int i=0;i<in.length()/2;i++){
			String sc=in.substring(i*2,i*2+2);
			int cha=Integer.parseInt(sc, 16);
			ret+=(char)cha;
		}
		return ret;
	}
	
	public static Command getInstance(String token){
		try {
			Entry target=(Entry)dictToken.get(token);
			if(target==null){
				return null;
			}
			return (Command)Class.forName(target.className).newInstance();
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public static Command getInstance(int id){
		try {
			Entry target=(Entry)dictID.get(Integer.toString(id));
			if(target==null){
				return null;
			}
			return (Command)Class.forName(target.className).newInstance();
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public CommandDict(boolean pf) {
		generatePropFile=pf;
		if(generatePropFile){
			sb=new StringBuffer();
			new Entry(f8.apps.display.DRAW.class.getName(), 3, 6000, "DRAW");
			new Entry(f8.modes.DEC.class.getName(), 3, 482, "DEC");
			new Entry(f8.commands.math.parts.CEIL.class.getName(), 3, 3014, "CEIL");
			new Entry(f8.commands.math.base.AND.class.getName(), 3, 1, "AND");
			new Entry(f8.modes.DEG.class.getName(), 3, 489, "DEG");
			new Entry(f8.objects.build.TYPE.class.getName(), 3, 3065, "TYPE");
			new Entry(f8.commands.alg.matrx.DET.class.getName(), 3, 62, "DET");
			new Entry(f8.apps.display.CIRCLE.class.getName(), 3, 404, "CIRCLE");
			new Entry(f8.apps.display.AUTO.class.getName(), 3, 6002, "AUTO");
			new Entry(f8.commands.math.hyp.TANH.class.getName(), 3, 3028, "TANH");
			new Entry(f8.modes.FIX.class.getName(), 3, 75, "FIX");
			new Entry(f8.commands.math.parts.MOD.class.getName(), 3, 3006, "MOD");
			new Entry(f8.commands.math.parts.RND.class.getName(), 3, 3015, "RND");
			new Entry(f8.modes.ChkCNCT.class.getName(), 3, 498, "ChkCNC_");
			new Entry(f8.commands.math.trig.ACOS.class.getName(), 3, 506, "ACOS");
			new Entry(f8.commands.prog.brch.IF.class.getName(), 1, 23, "IF 0 THEN  END");
			new Entry(f8.modes.GRAD.class.getName(), 3, 487, "GRAD");
			new Entry(f8.modes.RAD.class.getName(), 3, 488, "RAD");
			new Entry(f8.commands.math.hyp.ASINH.class.getName(), 3, 3025, "ASINH");
			new Entry(f8.commands.storage.HOME.class.getName(), 1, 124, "HOME");
			new Entry(f8.commands.math.parts.ABS.class.getName(), 3, 80, "ABS");
			new Entry(f8.modes.SCI.class.getName(), 3, 78, "SCI");
			new Entry(f8.commands.stk.PICK.class.getName(), 3, 16, "PICK");
			new Entry(f8.commands.compare.GE.class.getName(), 3, 83, "\u008A");
			new Entry(f8.commands.math.parts.PCTT.class.getName(), 3, 3009, "%T");
			new Entry(f8.commands.math.exp.LOG.class.getName(), 3, 76, "LOG");
			new Entry(f8.objects.types.Comment.class.getName(), 3, 3, "//null");
			new Entry(f8.commands.alg.matrx.Gauss.class.getName(), 3, 64, "GAUSS");
			new Entry(f8.objects.types.Complex.class.getName(), 3, 60, "(0,0)");
			new Entry(f8.commands.compare.GT.class.getName(), 3, 84, ">");
			new Entry(f8.commands.math.parts.IM.class.getName(), 3, 50, "IM");
			new Entry(f8.commands.math.parts.IP.class.getName(), 3, 3012, "IP");
			new Entry(f8.apps.display.BOX.class.getName(), 3, 403, "BOX");
			new Entry(f8.commands.math.simple.MINUS.class.getName(), 3, 87, "-");
			new Entry(f8.commands.math.deriv.Integral.class.getName(), 3, 1118, "\u0084");
			new Entry(f8.objects.types.Double.class.getName(), 3, 73, "3.141592653589793");
			new Entry(f8.objects.constants.MAXR.class.getName(), 3, 2380, "MAXR");
			new Entry(f8.commands.math.trig.SIN.class.getName(), 3, 58, "SIN");
			new Entry(f8.commands.math.parts.XPON.class.getName(), 3, 3011, "XPON");
			new Entry(f8.commands.storage.PURGE.class.getName(), 3, 510, "PURGE");
			new Entry(f8.commands.alg.matrx.RowScale.class.getName(), 3, 71, "ROWSCALE");
			new Entry(f8.commands.symb.Sigma.class.getName(), 3, 1117, "\u0085");
			new Entry(f8.commands.math.deriv.DER.class.getName(), 3, 101, "\u0088");
			new Entry(f8.commands.prog.brch.STOP.class.getName(), 3, 47, "STOP");
			new Entry(f8.objects.build.TO_LIST.class.getName(), 3, 302, "\u008DLIST");
			new Entry(f8.commands.stk.DROP.class.getName(), 3, 8, "DROP");
			new Entry(f8.objects.constants.PI.class.getName(), 3, 77, "PI");
			new Entry(f8.commands.math.hyp.SINH.class.getName(), 3, 3024, "SINH");
			new Entry(f8.commands.math.exp.EXP.class.getName(), 3, 74, "EXP");
			new Entry(f8.commands.math.parts.TRNC.class.getName(), 3, 3016, "TRNC");
			new Entry(f8.apps.display.LABEL.class.getName(), 3, 6004, "LABEL");
			new Entry(f8.commands.math.prob.FACTORIAL.class.getName(), 3, 3021, "!");
			new Entry(f8.commands.math.trig.TAN.class.getName(), 3, 53, "TAN");
			new Entry(f8.commands.math.parts.FLOOR.class.getName(), 3, 18, "FLOOR");
			new Entry(f8.commands.alg.matrx.RowSwap.class.getName(), 3, 79, "ROWSWAP");
			new Entry(f8.commands.units.CONVERT.class.getName(), 3, 104, "CONVERT");
			new Entry(f8.commands.storage.CRDIR.class.getName(), 3, 125, "CRDIR");
			new Entry(f8.commands.stk.ROLLD.class.getName(), 3, 11, "ROLLD");
			new Entry(f8.modes.HEX.class.getName(), 3, 483, "HEX");
			new Entry(f8.commands.symb.COLECT.class.getName(), 3, 100, "COLECT");
			new Entry(f8.commands.math.simple.INV.class.getName(), 3, 105, "INV");
			new Entry(f8.commands.math.exp.SQ.class.getName(), 3, 512, "SQ");
			new Entry(f8.modes.ChkSTK.class.getName(), 3, 500, "ChkSTK_");
			new Entry(f8.objects.build.EQ_TO.class.getName(), 3, 3058, "EQ\u008D");
			new Entry(f8.commands.alg.vectr.DFLECHAR.class.getName(), 3, 3043, "D?R");
			new Entry(f8.objects.types.F8Vector.class.getName(), 3, 92,"[<empty vector>]");
			new Entry(f8.commands.stk.DROP2.class.getName(), 3, 21, "DROP2");
			new Entry(f8.commands.alg.matrx.DIM.class.getName(), 3, 63, "DIM");
			new Entry(f8.apps.display.XRNG.class.getName(), 3, 6003, "XRNG");
			new Entry(f8.commands.prog.brch.FORNEXT.class.getName(), 1, 117, "FOR i NEXT");
			new Entry(f8.commands.math.exp.ALOG.class.getName(), 3, 511, "ALOG");
			new Entry(f8.objects.types.F8String.class.getName(), 1, 48, "\"");
			new Entry(f8.modes.ChkARG.class.getName(), 3, 499, "ChkARG_");
			new Entry(f8.commands.units.UBASE.class.getName(), 3, 106, "UBASE");
			new Entry(f8.commands.stk.DROPN.class.getName(), 3, 22, "DROPN");
			new Entry(f8.commands.alg.vectr.CROSS.class.getName(), 3, 61, "CROSS");
			new Entry(f8.commands.math.parts.PCT.class.getName(), 3, 3007, "%");
			new Entry(f8.commands.units.UFACT.class.getName(), 3, 103, "UFACT");
			new Entry(f8.commands.stk.SWAP.class.getName(), 3, 49, "SWAP");
			new Entry(f8.commands.storage.STEQ.class.getName(), 3, 2094, "STEQ");
			new Entry(f8.objects.build.TO_ARRY.class.getName(), 3, 3059, "\u008DDARRY");
			new Entry(f8.commands.storage.UPDIR.class.getName(), 3, 123, "UPDIR");
			new Entry(f8.modes.ChkBEEP.class.getName(), 3, 502, "ChkBEEP_");
			new Entry(f8.commands.math.prob.COMB.class.getName(), 3, 3019, "COMB");
			new Entry(f8.commands.stk.COPY.class.getName(), 3, 4, "COPY");
			new Entry(f8.commands.math.parts.ARG.class.getName(), 3, 3003, "ARG");
			new Entry(f8.modes.ChkCLK.class.getName(), 3, 496, "ChkCLK_");
			new Entry(f8.objects.types.Int.class.getName(), 3, 26, "# 1 d");
			new Entry(f8.commands.math.parts.MIN.class.getName(), 3, 3004, "MIN");
			new Entry(f8.commands.math.prob.PERM.class.getName(), 3, 3020, "PERM");
			new Entry(f8.commands.alg.vectr.FLECHAV2.class.getName(), 3, 3041, "?V2");
			new Entry(f8.commands.alg.vectr.FLECHAV3.class.getName(), 3, 3042, "?V3");
			new Entry(f8.commands.compare.LE.class.getName(), 3, 85, "\u0089");
			new Entry(f8.commands.stk.ROLL.class.getName(), 3, 7, "ROLL");
			new Entry(f8.commands.stk.EVAL.class.getName(), 3, 15, "EVAL");
			new Entry(f8.commands.alg.matrx.RowSub.class.getName(), 3, 72, "ROWSUB");
			new Entry(f8.commands.stk.OVER.class.getName(), 3, 2, "OVER");
			new Entry(f8.objects.types.Proc.class.getName(), 1, 40, "� ");
			new Entry(f8.commands.units.UVAL.class.getName(), 3, 2484, "UVAL");
			new Entry(f8.objects.types.Lista.class.getName(), 3, 120, "new Entry(  )");
			new Entry(f8.commands.math.trig.ASIN.class.getName(), 3, 54, "ASIN");
			new Entry(f8.modes.ChkCMD.class.getName(), 3, 490, "ChkCMD_");
			new Entry(f8.commands.compare.LT.class.getName(), 3, 86, "<");
			new Entry(f8.objects.build.TO_STR.class.getName(), 3, 3060, "\u008DSTR");
			new Entry(f8.commands.math.hyp.EXPM.class.getName(), 3, 3030, "EXPM");
			new Entry(f8.modes.STD.class.getName(), 3, 504, "STD");
			new Entry(f8.commands.math.hyp.ACOSH.class.getName(), 3, 3027, "ACOSH");
			new Entry(f8.commands.math.parts.CONJ.class.getName(), 3, 3002, "CONJ");
			new Entry(f8.objects.types.Literal.class.getName(), 3, 32, "null");
			new Entry(f8.modes.ENG.class.getName(), 3, 505, "ENG");
			new Entry(f8.objects.types.Unit.class.getName(), 1, 118, "0_");
			new Entry(f8.commands.math.trig.COS.class.getName(), 3, 57, "COS");
			new Entry(f8.apps.display.LINE.class.getName(), 3, 402, "LINE");
			new Entry(f8.objects.types.TempUnit.class.getName(), 1, 430, "0_");
			new Entry(f8.objects.constants.E.class.getName(), 3, 2379, "e");
			new Entry(f8.commands.prog.FLECHA.class.getName(), 3, 901, "?");
			new Entry(f8.objects.build.TO_UNIT.class.getName(), 3, 508, "\u008DUNIT");
			new Entry(f8.objects.build.TO_TAG.class.getName(), 3, 600, "\u008DTAG");
			new Entry(f8.commands.stk.DUP2.class.getName(), 3, 24, "DUP2");
			new Entry(f8.commands.symb.ASSIGN.class.getName(), 3, 13, "=");
			new Entry(f8.objects.constants.I.class.getName(), 3, 65, "i");
			new Entry(f8.commands.math.exp.LN.class.getName(), 3, 102, "LN");
			new Entry(f8.commands.math.trig.ATAN.class.getName(), 3, 52, "ATAN");
			new Entry(f8.commands.alg.matrx.JORDAN.class.getName(), 3, 66, "JORDAN");
			new Entry(f8.commands.math.base.OR.class.getName(), 3, 38, "OR");
			new Entry(f8.commands.math.parts.PCTCH.class.getName(), 3, 3008, "%CH");
			new Entry(f8.commands.alg.vectr.RFLECHAD.class.getName(), 3, 3044, "R?D");
			new Entry(f8.commands.alg.matrx.MkMatrix.class.getName(), 3, 68, "MATRIX");
			new Entry(f8.objects.types.InfixExp.class.getName(), 1, 119, "''");
			new Entry(f8.commands.prog.brch.WHILEREPEAT.class.getName(), 1, 10, "WHILE 0 REPEAT  END");
			new Entry(f8.objects.build.R_TO_C.class.getName(), 3, 3061, "R\u008DC");
			new Entry(f8.apps.display.YRNG.class.getName(), 3, 6014, "YRNG");
			new Entry(f8.modes.BIN.class.getName(), 3, 480, "BIN");
			new Entry(f8.commands.stk.DUPN.class.getName(), 3, 19, "DUPN");
			new Entry(f8.objects.build.DTAG.class.getName(), 3, 3063, "DTAG");
			new Entry(f8.objects.build.VTYPE.class.getName(), 3, 3066, "VTYPE");
			new Entry(f8.commands.math.hyp.COSH.class.getName(), 3, 3026, "COSH");
			new Entry(f8.commands.math.base.NOT.class.getName(), 3, 37, "NOT");
			new Entry(f8.commands.math.simple.DIV.class.getName(), 3, 81, "/");
			new Entry(f8.commands.math.simple.PLUS.class.getName(), 3, 88, "+");
			new Entry(f8.modes.ChkSYM.class.getName(), 3, 501, "ChkSYM_");
			new Entry(f8.commands.math.simple.TIMES.class.getName(), 3, 91, "*");
			new Entry(f8.commands.alg.CommandSequence.class.getName(), 1, 20, "");
			new Entry(f8.objects.constants.MINR.class.getName(), 3, 2381, "MINR");
			new Entry(f8.commands.symb.EXPAND.class.getName(), 3, 107, "EXPAND");
			new Entry(f8.commands.math.exp.CARET.class.getName(), 3, 56, "^");
			new Entry(f8.commands.alg.vectr.VFLECHA.class.getName(), 3, 3040, "V?");
			new Entry(f8.commands.math.parts.MAX.class.getName(), 3, 3005, "MAX");
			new Entry(f8.commands.math.parts.MANT.class.getName(), 3, 3010, "MANT");
			new Entry(f8.commands.stk.ROT.class.getName(), 3, 12, "ROT");
			new Entry(f8.apps.display.MARK.class.getName(), 3, 401, "MARK");
			new Entry(f8.modes.OCT.class.getName(), 3, 481, "OCT");
			new Entry(f8.commands.math.exp.SQRT.class.getName(), 3, 59, "\u0083");
			new Entry(f8.commands.storage.PGDIR.class.getName(), 3, 509, "PGDIR");
			new Entry(f8.modes.D3Mode1.class.getName(), 3, 486, "3D_1");
			new Entry(f8.commands.storage.MENU.class.getName(), 3, 436, "MENU");
			new Entry(f8.commands.math.simple.CHS.class.getName(), 3, 801, "CHS");
			new Entry(f8.modes.D3Mode2.class.getName(), 3, 485, "3D_2");
			new Entry(f8.modes.D3Mode3.class.getName(), 3, 484, "3D_3");
			new Entry(f8.modes.FMMode.class.getName(), 3, 479, "0_0");
			new Entry(f8.commands.stk.DEPTH.class.getName(), 3, 17, "DEPTH");
			new Entry(f8.commands.prog.debug.HALT.class.getName(), 3, 2987, "HALT");
			new Entry(f8.commands.alg.matrx.MkVector.class.getName(), 3, 69, "VECTOR");
			new Entry(f8.commands.math.hyp.LNP1.class.getName(), 3, 3031, "LNP1");
			new Entry(f8.commands.math.parts.FP.class.getName(), 3, 3013, "FP");
			new Entry(f8.commands.math.hyp.ATANH.class.getName(), 3, 3029, "ATANH");
			new Entry(f8.commands.prog.brch.DOUNTIL.class.getName(), 1, 42, "DO UNTIL 1 END ");
			new Entry(f8.commands.math.parts.SIGN.class.getName(), 3, 3001, "SIGN");
			new Entry(f8.commands.storage.STO.class.getName(), 3, 5, "STO");
			new Entry(f8.commands.stk.DUP.class.getName(), 3, 9, "DUP");
			new Entry(f8.modes.ChkML.class.getName(), 3, 497, "ChkML_");
			new Entry(f8.commands.math.parts.RE.class.getName(), 3, 70, "RE");
			new Entry(f8.commands.math.base.XOR.class.getName(), 3, 1532, "XOR");
			new Entry(f8.objects.types.Matrix.class.getName(), 3, 67, "[[1 ]]");
			new Entry(f8.commands.compare.EQ.class.getName(), 3, 82, "==");
			new Entry(f8.objects.build.OBJ_TO.class.getName(), 3, 301, "OBJ\u008D");
			new Entry(f8.apps.display.ERASE.class.getName(), 3, 6001, "ERASE");
			new Entry(f8.objects.types.Directory.class.getName(), 1, 127, "DIR  END");
			new Entry(f8.objects.build.C_TO_R.class.getName(), 3, 3062, "C\u008DR");
			new Entry(f8.commands.math.exp.YrootX.class.getName(),3,7001,"YROOTX");
			new Entry(f8.commands.storage.DUMPALL.class.getName(),3,7002,"DUMPALL");
			new Entry(f8.commands.storage.LOADALL.class.getName(),3,7003,"LOADALL");
			new Entry(f8.objects.props.GLT1.class.getName(),3,7004,"GLT1");
			new Entry(f8.objects.types.Grob.class.getName(),3,7005,"GROB");
			new Entry(f8.commands.storage.RCL.class.getName(),3,7006,"RCL");
			new Entry(f8.commands.storage.SYSDIR.class.getName(),3,7007,"SYSDIR");
			new Entry(f8.commands.storage.LOAD.class.getName(),3,7008,"LOAD");
			new Entry(f8.modes.RefButtons.class.getName(),3,7010,"0_1");
			new Entry(f8.commands.math.ints.PRIME.class.getName(),3,7013,"PRIME");
			new Entry(f8.modes.ExactMode.class.getName(),3,7014,"0_2");
			new Entry(f8.commands.symb.SOLVE.class.getName(),3,7015,"SOLVE");
			try {
				StMan.delete("/load.properties");
				OutputStream os=StMan.getOS("/load.properties");
				os.write(("size="+numClasses+"\n").getBytes());
				os.write(sb.toString().getBytes());
				os.close();
				sb=null;
			} catch (IOException e) {
				e.printStackTrace();
			}
		}else{
			Properties p=new Properties("/load.properties");
			try {
				int sz=Integer.parseInt(p.getProperty("size"));
				for(int i=1;i<=sz;i++){
					String cn="c."+i+".";
					String className=p.getProperty(cn+"na");
					int tipo=Integer.parseInt(p.getProperty(cn+"ti"));
					int id=Integer.parseInt(p.getProperty(cn+"id"));
					String token=retTransform(p.getProperty(cn+"to"));
					new Entry(className,tipo,id,token==null?"":token);
				}
			} catch (NumberFormatException e) {
				e.printStackTrace();
			} catch (StringIndexOutOfBoundsException s){
				s.printStackTrace();
				for(int i=1;i<184;i++){
					String cn="c."+i+".";
					checkProp(p, i, cn, "na");
					checkProp(p, i, cn, "ti");
					checkProp(p, i, cn, "id");
					checkProp(p, i, cn, "to");
				}
				System.exit(-1);
			}
		}
	}

	private void checkProp(Properties p, int i, String cn, String field) {
		String pp=p.getProperty(cn+field);
		if(pp==null || pp.length()==0){
			System.out.println(field+"#"+i);
		}
	}
}
