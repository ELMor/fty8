package f8.commands.alg.matrx;


import java.io.IOException;

import f8.commands.Dispatch2;
import f8.commands.Storable;
import f8.exceptions.F8Exception;
import f8.exceptions.IndexRangeException;
import f8.objects.Stackable;
import f8.objects.types.Int;
import f8.objects.types.Matrix;
import f8.platform.io.DIS;
import f8.platform.io.DOS;

public final class JORDAN extends Dispatch2 {
	public JORDAN() {
		// Aqui estaba systemInstall
	}

	public int getID() {
		return 66;
	}

	public Storable getInstance() {
		return new JORDAN();
	}

	public void loadState(DIS ds) throws IOException {
	}

	public void saveState(DOS ds)  throws IOException {
	}

	void reduce(double[][] m, int R, int C) {
		int r = 0;
		int c = 0;

		while ((r < R) && (c < C)) {
			// find pivot in row r
			int j = c;

			while ((j < C) && (m[r][j] == 0)) {
				j++;
			}

			if (j < C) {
				for (int i = r - 1; i >= 0; i--) {
					double x = m[i][j];
					m[i][c] = 0;

					for (int k = c + 1; k < m[i].length; k++) {
						m[i][k] -= (x * m[r][k]);
					}
				}

				r++;
				c = j + 1;
			} else {
				c = C;
			}
		}
	}

	public String toString() {
		return ("JORDAN");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.Dispatch2#prfIntMatrix(f8.kernel.types.Int,
	 *      f8.kernel.types.Matrix)
	 */
	public Stackable prfIntMatrix(Int a, Matrix b) throws F8Exception {
		Matrix M = b;
		int C = a.intValue();

		if ((C >= 0) && (C <= M.columnNumber())) {
			// leave the stack untouched!
			reduce(M.x, M.rowNumber(), C);
			return M;
		} else {
			throw new IndexRangeException(this);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.Command#isAlgebraic()
	 */
	public boolean isAlgebraic() {
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.Command#isInfix()
	 */
	public boolean isInfix() {
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.Command#isOperator()
	 */
	public boolean isOperator() {
		return false;
	}

}
