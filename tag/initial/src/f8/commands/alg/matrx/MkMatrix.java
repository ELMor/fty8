package f8.commands.alg.matrx;


import java.io.IOException;

import f8.commands.Dispatch2;
import f8.commands.Storable;
import f8.exceptions.F8Exception;
import f8.exceptions.InvalidDimensionException;
import f8.objects.Stackable;
import f8.objects.types.Double;
import f8.objects.types.Matrix;
import f8.platform.io.DIS;
import f8.platform.io.DOS;

public final class MkMatrix extends Dispatch2 {
	public MkMatrix() {
		// Aqui estaba systemInstall
	}

	public int getID() {
		return 68;
	}

	public Storable getInstance() {
		return new MkMatrix();
	}

	public void loadState(DIS ds) throws IOException {
	}

	public void saveState(DOS ds)  throws IOException {
	}

	public String toString() {
		return ("MATRIX");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.Dispatch2#prfDoubleDouble(f8.kernel.types.Double,
	 *      f8.kernel.types.Double)
	 */
	public Stackable prfDoubleDouble(Double a, Double b) throws F8Exception {
		int r = a.doubleValue().toInt();
		int c = b.doubleValue().toInt();

		if ((r >= 0) && (c >= 0)) {
			return (new Matrix(r, c));
		} else {
			throw new InvalidDimensionException(this);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.Command#isAlgebraic()
	 */
	public boolean isAlgebraic() {
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.Command#isInfix()
	 */
	public boolean isInfix() {
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.Command#isOperator()
	 */
	public boolean isOperator() {
		return false;
	}

}
