package f8.commands.alg.matrx;

import java.io.IOException;

import f8.Core;
import f8.commands.NonAlgebraic;
import f8.commands.Storable;
import f8.commands.alg.DoubleValue;
import f8.commands.alg.IntValue;
import f8.exceptions.BadArgumentTypeException;
import f8.exceptions.F8Exception;
import f8.exceptions.TooFewArgumentsException;
import f8.objects.Stackable;
import f8.objects.types.Ereal;
import f8.objects.types.Matrix;
import f8.platform.io.DIS;
import f8.platform.io.DOS;

public final class RowSub extends NonAlgebraic {
	public RowSub() {
		// Aqui estaba systemInstall
	}

	public int getID() {
		return 72;
	}

	public Storable getInstance() {
		return new RowSub();
	}

	public void loadState(DIS ds) throws IOException {
	}

	public void saveState(DOS ds)  throws IOException {
	}

	// m i j c rowsub
	public void exec(Object extern) throws F8Exception {
		if (Core.check(4)) {
			Stackable m = Core.peek(3);
			Stackable I = Core.peek(2);
			Stackable J = Core.peek(1);
			Stackable C = Core.peek(0);

			if (m instanceof Matrix && I instanceof IntValue
					&& J instanceof IntValue && C instanceof DoubleValue) {
				Matrix M = (Matrix) m;
				int i = ((IntValue) I).intValue();
				int j = ((IntValue) J).intValue();
				Ereal cc = ((DoubleValue) C).doubleValue();
				double c=Double.parseDouble(cc.toString());
				Core.pop(4);

				for (int k = 0; k < M.c; k++) {
					M.x[i][k] -= (c * M.x[j][k]);
				}

				Core.push(m);
			} else {
				throw new BadArgumentTypeException(this);
			}
		} else {
			throw new TooFewArgumentsException(this);
		}
	}

	public String toString() {
		return ("ROWSUB");
	}
}
