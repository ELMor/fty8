/*
 * Created on 29-sep-2003
 *
 
 
 */
package f8.commands.math;

import antlr.collections.AST;
import f8.Core;
import f8.ICalc;
import f8.commands.Dispatch1;
import f8.commands.math.deriv.Derivable;
import f8.exceptions.F8Exception;
import f8.objects.Stackable;
import f8.objects.constants.PI;
import f8.objects.types.Ereal;
import f8.objects.types.InfixExp;

/**
 * @author elinares
 * 
 * 
 * 
 */
public abstract class Trigonometric extends Dispatch1 implements Derivable {
	public abstract AST deriveWithArgs(AST expr, String var) throws F8Exception;

	public Stackable RAD2Settings(Stackable RADAngle) {
		switch (Core.getSetting(ICalc.ANG_MOD)) {
		case ICalc.DEG:
			return new InfixExp(mkl.mul(RADAngle.getAST(), mkl.div(
					mkl.num(180), new PI().getAST())));
		case ICalc.GRAD:
			return new InfixExp(mkl.mul(RADAngle.getAST(), mkl.div(
					mkl.num(200), new PI().getAST())));
		}
		return RADAngle;
	}

	public Ereal RAD2Settings(Ereal RADAngle) {
		switch (Core.getSetting(ICalc.ANG_MOD)) {
		case ICalc.DEG:
			return RADAngle.mul(180).div(Ereal.PI);
		case ICalc.GRAD:
			return RADAngle.mul(200).div(Ereal.PI);
		}
		return RADAngle;
	}

	public Stackable Settings2RAD(Stackable UnkAngle) {
		switch (Core.getSetting(ICalc.ANG_MOD)) {
		case ICalc.DEG:
			return new InfixExp(mkl.mul(UnkAngle.getAST(), mkl.div(new PI()
					.getAST(), mkl.num(180))));
		case ICalc.GRAD:
			return new InfixExp(mkl.mul(UnkAngle.getAST(), mkl.div(new PI()
					.getAST(), mkl.num(200))));
		}
		return UnkAngle;
	}

	public Ereal Settings2RAD(Ereal UnkAngle) {
		switch (Core.getSetting(ICalc.ANG_MOD)) {
		case ICalc.DEG:
			return Ereal.PI.div(180).mul(UnkAngle);
		case ICalc.GRAD:
			return Ereal.PI.div(200).mul(UnkAngle);
		}
		return UnkAngle;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.Command#isAlgebraic()
	 */
	public boolean isAlgebraic() {
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.Command#isInfix()
	 */
	public boolean isInfix() {
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.Command#isOperator()
	 */
	public boolean isOperator() {
		return false;
	}

}
