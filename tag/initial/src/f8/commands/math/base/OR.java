package f8.commands.math.base;


import java.io.IOException;

import f8.commands.Storable;
import f8.objects.types.Ereal;
import f8.platform.io.DIS;
import f8.platform.io.DOS;


public final class OR extends Logical2 {
	public OR() {
		// Aqui estaba systemInstall
	}

	public int getID() {
		return 38;
	}

	public Storable getInstance() {
		return new OR();
	}

	public void loadState(DIS ds) throws IOException {
	}

	public void saveState(DOS ds)  throws IOException {
	}

	public String toString() {
		return ("OR");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.op.boo.Logical2#logic(double, double)
	 */
	public Ereal logic(Ereal l, Ereal r) {
		return l.or(r);
	}

}
