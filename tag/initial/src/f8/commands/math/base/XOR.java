/*
 * Created on 01-oct-2003
 *
 
 
 */
package f8.commands.math.base;


import java.io.IOException;

import f8.commands.Storable;
import f8.objects.types.Ereal;
import f8.platform.io.DIS;
import f8.platform.io.DOS;


/**
 * @author elinares
 * 
 * 
 * 
 */
public class XOR extends Logical2 {

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.op.boo.Logical2#logic(double, double)
	 */
	public Ereal logic(Ereal l, Ereal r) {
		return l.xor(r);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.platform.io.Storable#getID()
	 */
	public int getID() {
		return 1532;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.platform.io.Storable#getInstance()
	 */
	public Storable getInstance() {
		return this;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.platform.io.Storable#loadState(f8.platform.io.DataStream)
	 */
	public void loadState(DIS ds) throws IOException {

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.platform.io.Storable#saveState(f8.platform.io.DataStream)
	 */
	public void saveState(DOS ds)  throws IOException {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		return "XOR";
	}

}
