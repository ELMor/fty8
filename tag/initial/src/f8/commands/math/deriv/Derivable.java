/*
 * Created on 31-ago-2003
 *
 
 
 */
package f8.commands.math.deriv;

import antlr.collections.AST;
import f8.exceptions.F8Exception;

/**
 * @author elinares
 * 
 * 
 * 
 */
public interface Derivable {
	/**
	 * Devuelve el arbol de derivacion
	 * 
	 * @param args
	 *            Lista de argumentos de la funcion
	 * @return Arbol de derivacion
	 */
	public AST deriveWithArgs(AST expr, String var) throws F8Exception;
}
