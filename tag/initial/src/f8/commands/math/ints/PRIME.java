package f8.commands.math.ints;

import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Vector;

import antlr.collections.AST;
import f8.commands.Dispatch1;
import f8.exceptions.BadArgumentTypeException;
import f8.exceptions.BadArgumentValueException;
import f8.exceptions.F8Exception;
import f8.objects.Stackable;
import f8.objects.types.Double;
import f8.objects.types.InfixExp;
import f8.objects.types.Lista;
import f8.platform.io.DIS;
import f8.platform.io.DOS;

public class PRIME extends Dispatch1 {

	private static int ps[]=new int[1000];
	static {
		InputStream is="".getClass().getResourceAsStream("/Primos.BIN");
		DataInputStream d=new DataInputStream(is);
		DIS dis=new DIS(d);
		try {
			for(int i=0;i<1000;i++)
				ps[i]=dis.readInt();
			dis.close();
		} catch (IOException e) {
			ps=null;
			e.printStackTrace();
		}
	}

	
	public static int[][] primeDecomp(int n) throws BadArgumentValueException {
		Vector v=new Vector();
		if(n<0){
			n=-n;
			v.addElement(new Integer(-1));
			v.addElement(new Integer(1));
		}
		int nn=n;
		for(int pn=0;pn<ps.length && nn!=1;pn++){
			int veces=0;
			while(nn % ps[pn] == 0 ){
				veces++;
				nn/=ps[pn];
			}
			if(veces>0){
				v.addElement(new Integer(ps[pn]));
				v.addElement(new Integer(veces));
			}
		}
		if(nn!=1){
			v.addElement(new Integer(nn));
			v.addElement(new Integer(1));
		}
		int sz=v.size()/2;
		int ret[][]=new int[2][sz];
		for(int i=0;i<sz;i++){
			ret[0][i]=((Integer)v.elementAt(i*2)).intValue();
			ret[1][i]=((Integer)v.elementAt(i*2+1)).intValue();
		}
		return ret;
	}
	
	public Stackable prfDouble(Double x) throws F8Exception {
		if(x.equals(new Double(x.intValue()))){
			int p[][]=primeDecomp(x.intValue());
			Vector v=new Vector();
			for(int i=0;i<p[0].length;i++){
				v.addElement(new Double(p[0][i]));
				v.addElement(new Double(p[1][i]));
			}
			return new Lista(v);
		}else throw new BadArgumentTypeException(this);
	}

	public int getID() {
		// TODO Auto-generated method stub
		return 7013;
	}

	public void loadState(DIS ds) throws IOException {
	}

	public void saveState(DOS ds) throws IOException {
	}
	public Stackable prfInfix(InfixExp a) throws F8Exception {
		AST nodo=a.getAST().getFirstChild();
		if(nodo.getType()==NUMBER){
			int n;
			try {
				n=Integer.parseInt(nodo.getText());
				return prfDouble(new Double(n));
			} catch (NumberFormatException e) {
				throw new BadArgumentTypeException(a);
			}
		}else
			throw new BadArgumentTypeException(a);
		
	}
	public String toString(){
		return "PRIME";
	}
}
