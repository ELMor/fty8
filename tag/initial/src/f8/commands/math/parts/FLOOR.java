package f8.commands.math.parts;


import java.io.IOException;

import f8.commands.Dispatch1;
import f8.commands.Storable;
import f8.exceptions.F8Exception;
import f8.objects.Stackable;
import f8.objects.types.Double;
import f8.objects.types.Int;
import f8.platform.io.DIS;
import f8.platform.io.DOS;

public final class FLOOR extends Dispatch1 {
	public FLOOR() {
		// Aqui estaba systemInstall
	}

	public int getID() {
		return 18;
	}

	public Storable getInstance() {
		return new FLOOR();
	}

	public void loadState(DIS ds) throws IOException {
	}

	public void saveState(DOS ds)  throws IOException {
	}

	public String toString() {
		return ("FLOOR");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.Dispatch1#prfDouble(f8.kernel.types.Double)
	 */
	public Stackable prfDouble(Double a) throws F8Exception {
		return (new Double(a.doubleValue().floor()));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.Dispatch1#prfInt(f8.kernel.types.Int)
	 */
	public Stackable prfInt(Int x) throws F8Exception {
		return x;
	}

}
