package f8.commands.math.parts;


import java.io.IOException;

import f8.commands.Dispatch2;
import f8.commands.Storable;
import f8.exceptions.F8Exception;
import f8.objects.Stackable;
import f8.objects.types.Double;
import f8.objects.types.Ereal;
import f8.objects.types.Int;
import f8.platform.io.DIS;
import f8.platform.io.DOS;

public final class PCTT extends Dispatch2 {
	public PCTT() {
		// Aqui estaba systemInstall
	}

	public int getID() {
		return 3009;
	}

	public String toString() {
		return ("%T");
	}

	public Storable getInstance() {
		return new PCTT();
	}

	public void loadState(DIS ds) throws IOException {
	}

	public void saveState(DOS ds)  throws IOException {
	}

	public boolean isAlgebraic() {
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.Dispatch2#prfDoubleDouble(f8.types.Double, f8.types.Double)
	 */
	
	public static Ereal cien=new Ereal(100);
	
	public Stackable prfDoubleDouble(Double a, Double b) throws F8Exception {
		return new Double(cien.div(a.doubleValue()).mul(b.doubleValue()));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.Dispatch2#prfDoubleInt(f8.types.Double, f8.types.Int)
	 */
	public Stackable prfDoubleInt(Double a, Int b) throws F8Exception {
		return new Double(cien.div(a.doubleValue()).mul(b.n));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.Dispatch2#prfIntDouble(f8.types.Int, f8.types.Double)
	 */
	public Stackable prfIntDouble(Int a, Double b) throws F8Exception {
		return new Double(cien.div(a.n).mul(b.doubleValue()));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.Dispatch2#prfIntInt(f8.types.Int, f8.types.Int)
	 */
	public Stackable prfIntInt(Int a, Int b) throws F8Exception {
		return new Double(100 / a.n * b.n);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.Command#isInfix()
	 */
	public boolean isInfix() {
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.Command#isOperator()
	 */
	public boolean isOperator() {
		return false;
	}

}
