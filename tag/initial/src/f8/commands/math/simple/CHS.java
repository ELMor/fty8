package f8.commands.math.simple;


import java.io.IOException;

import antlr.collections.AST;
import f8.commands.Dispatch1;
import f8.commands.Storable;
import f8.commands.math.mkl;
import f8.commands.math.deriv.DER;
import f8.commands.math.deriv.Derivable;
import f8.exceptions.F8Exception;
import f8.objects.Stackable;
import f8.objects.types.Complex;
import f8.objects.types.Double;
import f8.objects.types.F8Vector;
import f8.objects.types.InfixExp;
import f8.objects.types.Int;
import f8.platform.io.DIS;
import f8.platform.io.DOS;

public final class CHS extends Dispatch1 implements Derivable{
	public AST deriveWithArgs(AST expr, String var) throws F8Exception {
		return mkl.neg(DER.deriveFunction(expr, var));
	}

	public CHS() {
		// Aqui estaba systemInstall
	}

	public int getID() {
		return 801;
	}

	public Storable getInstance() {
		return new CHS();
	}

	public void loadState(DIS ds) throws IOException {
	}

	public void saveState(DOS ds)  throws IOException {
	}

	public String toString() {
		return ("CHS");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.Dispatch1#prfInt(f8.kernel.types.Int)
	 */
	public Stackable prfInt(Int a) throws F8Exception {
		int A = a.intValue();
		A = -A;
		return new Int(A);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.Dispatch1#prfDouble(f8.kernel.types.Double)
	 */
	public Stackable prfDouble(Double a) throws F8Exception {
		return new Double(a.doubleValue().neg());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.Dispatch1#prfComplex(f8.kernel.types.Complex)
	 */
	public Stackable prfComplex(Complex a) throws F8Exception {
		return new Complex(a.re.neg(),a.im.neg());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.Dispatch1#prfVector(f8.kernel.types.F8Vector)
	 */
	public Stackable prfVector(F8Vector a) throws F8Exception {
		for (int i = 0; i < a.x.length; i++)
			a.x[i] = a.x[i].neg();
		return new F8Vector(a.x);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.Dispatch1#prfInfix(f8.kernel.types.InfixExp)
	 */
	public Stackable prfInfix(InfixExp a) throws F8Exception {
		return new InfixExp(mkl.neg(a.getAST()));
	}

}
