package f8.commands.math.trig;

import java.io.IOException;

import antlr.collections.AST;
import f8.commands.Storable;
import f8.commands.math.Trigonometric;
import f8.commands.math.mkl;
import f8.commands.math.deriv.DER;
import f8.exceptions.F8Exception;
import f8.objects.Stackable;
import f8.objects.types.Complex;
import f8.objects.types.Double;
import f8.objects.types.Ereal;
import f8.objects.types.Unit;
import f8.platform.io.DIS;
import f8.platform.io.DOS;

public final class ATAN extends Trigonometric {
	public ATAN() {
		// Aqui estaba systemInstall
	}

	public int getID() {
		return 52;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.Command#isAlgebraic()
	 */
	public boolean isAlgebraic() {
		return true;
	}

	public Storable getInstance() {
		return new ATAN();
	}

	public void loadState(DIS ds) throws IOException {
	}

	public void saveState(DOS ds)  throws IOException {
	}

	public Stackable prfDouble(Double x) {
		if ( x.doubleValue().lt(-1) || x.doubleValue().gt(1) ) {
			return new Complex(x.doubleValue(), Ereal.cero).atan();
		}
		return new Double(RAD2Settings(x.doubleValue().atan()));
	}

	public String toString() {
		return ("ATAN");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.op.symb.Derivable#derive(antlr.collections.AST,
	 *      java.lang.String)
	 */
	public AST deriveWithArgs(AST args, String var) throws F8Exception {
		return mkl.mul(DER.deriveFunction(args, var), mkl.div(mkl.num(1), mkl
				.sum(mkl.num(1), mkl.ele(args, mkl.num(2)))));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.op.num.Fcn#f(f8.kernel.types.Unit)
	 */
	public Unit perform(Unit a) {
		// TODO arcotangente con unidades
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.op.num.Fcn#undo()
	 */
	public String undo() {
		return "TAN";
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.Dispatch1#prfComplex(f8.kernel.types.Complex)
	 */
	public Stackable prfComplex(Complex x) throws F8Exception {
		return x.atan();
	}

}
