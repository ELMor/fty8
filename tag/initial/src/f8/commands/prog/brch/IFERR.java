package f8.commands.prog.brch;

import java.io.IOException;

import antlr.collections.AST;
import f8.commands.Command;
import f8.commands.NonAlgebraic;
import f8.commands.Storable;
import f8.exceptions.F8Exception;
import f8.platform.io.DIS;
import f8.platform.io.DOS;

public final class IFERR extends NonAlgebraic {
	Command cond;

	Command thenBody;

	public IFERR(AST def) {
		if (def == null) {
			return;
		}

		AST aCond = def.getFirstChild();
		AST aThen = aCond.getNextSibling();
		cond = Command.createFromAST(aCond);
		thenBody = Command.createFromAST(aThen);

	}

	public int getID() {
		return 499;
	}

	public Storable getInstance() {
		return new IFERR(null);
	}

	public void loadState(DIS ds) throws IOException {
		cond = Command.loadFromStorage(ds);
		thenBody = Command.loadFromStorage(ds);
	}

	public void saveState(DOS ds)  throws IOException {
		ds.writeInt(cond.getID());
		cond.saveState(ds);
		ds.writeInt(thenBody.getID());
		thenBody.saveState(ds);

	}

	public void exec(Object extern) throws F8Exception {
		try {
			cond.exec(null);
		} catch (F8Exception e) {
			thenBody.exec(null);
		}

	}

	public String toString() {
		return "IFERR " + cond.toString() + " THEN " + thenBody.toString()
				+ " END";
	}
}
