package f8.commands.stk;

import java.io.IOException;
import java.util.Vector;

import f8.Core;
import f8.commands.Command;
import f8.commands.NonAlgebraic;
import f8.commands.Storable;
import f8.exceptions.F8Exception;
import f8.exceptions.TooFewArgumentsException;
import f8.objects.Stackable;
import f8.objects.types.Double;
import f8.platform.io.DIS;
import f8.platform.io.DOS;

public final class ROLLD extends NonAlgebraic {
	public ROLLD() {
		// Aqui estaba systemInstall
	}

	public int getID() {
		return 11;
	}

	public Storable getInstance() {
		return new ROLLD();
	}

	public void loadState(DIS ds) throws IOException {
	}

	public void saveState(DOS ds)  throws IOException {
	}

	public void exec(Object extern) throws F8Exception {
		int rot;
		if (Core.check(1)) {
			Command a = Core.pop();
			if (a instanceof Double) {
				rot = ((Double) a).doubleValue().toInt();
				if (Core.check(rot)) {
					Vector gb = new Vector();
					for (int i = 0; i < rot; i++) {
						gb.addElement(Core.pop());
					}
					Core.push((Stackable) gb.elementAt(rot - 1));
					for (int i = 0; i < (rot - 1); i++) {
						Core.push((Stackable) gb.elementAt(i));
					}
				} else {
					throw new TooFewArgumentsException(this);
				}
			}
		} else {
			throw new TooFewArgumentsException(this);
		}
	}

	public String toString() {
		return ("ROLLD");
	}
}
