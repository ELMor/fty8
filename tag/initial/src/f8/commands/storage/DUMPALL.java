package f8.commands.storage;

import java.io.IOException;
import java.util.Enumeration;
import java.util.Vector;

import f8.Core;
import f8.commands.Command;
import f8.commands.NonAlgebraic;
import f8.exceptions.F8Exception;
import f8.platform.io.DIS;
import f8.platform.io.DOS;
import f8.platform.io.StMan;

public class DUMPALL extends NonAlgebraic {

	public int getID() {
		// TODO Auto-generated method stub
		return 7002;
	}

	public void loadState(DIS ds) throws IOException {
		// TODO Auto-generated method stub

	}

	public void saveState(DOS ds) throws IOException {
		// TODO Auto-generated method stub

	}

	public void exec(Object extern) throws F8Exception {
		//Comprobamos los directorios y los borramos:
		//SETTINGS, HOME y STACK
		StMan.removeDir("dump"); 
		StMan.mkDir("dump");
		// Grabamos los modos
		for (int i = 0; i < Core.settings.length; i++) {
			StMan.saveTo("dump/set"+i+".f8", ""+Core.settings[i]);
		}
		// Grabamos el diccionario de datos
		Vector keys = new Vector();
		for (Enumeration e = Core.homeDir.getHT().keys(); e.hasMoreElements();)
			keys.addElement(e.nextElement());
		int sz = keys.size();
		for (int i = 0; i < sz; i++) {
			String skey = (String) keys.elementAt(i);
			Command value = (Command) Core.homeDir.get(skey);
			StMan.saveTo("dump/"+skey, value.toString());
		}
		// Ahora opStack
		sz = Core.userStack.size();
		for (int i = 0; i < sz; i++) {
			Command it = (Command) Core.userStack.elementAt(i);
			StMan.saveTo("dump/stack"+i+".f8", it.toString());
		}
	}

	public String toString(){
		return "DUMPALL";
	}
	
}
