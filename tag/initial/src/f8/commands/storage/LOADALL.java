package f8.commands.storage;

import java.io.File;
import java.io.IOException;

import f8.commands.NonAlgebraic;
import f8.exceptions.BadArgumentValueException;
import f8.exceptions.F8Exception;
import f8.platform.io.DIS;
import f8.platform.io.DOS;

public class LOADALL extends NonAlgebraic {

	public int getID() {
		// TODO Auto-generated method stub
		return 7003;
	}

	public void loadState(DIS ds) throws IOException {

	}

	public void saveState(DOS ds) throws IOException {

	}

	public void exec(Object extern) throws F8Exception {
		// Listamos dump
		String errores="";
		File dump=new File("dump");
		if(dump.exists() && dump.isDirectory()){
			File dumped[]=dump.listFiles();
			for(File c:dumped){
				String name=c.getAbsolutePath();
				errores = LOAD.loadFileFromDumpDir(errores, name);
			}
			if(errores.length()>0){
				throw new BadArgumentValueException(errores); 
			}
		}
	}


	public String toString(){
		return "LOADALL";
	}
	
}
