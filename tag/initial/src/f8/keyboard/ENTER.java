/*
 * Created on 22-jul-2003
 *
 
 
 */
package f8.keyboard;

import f8.Core;
import f8.commands.stk.DUP;
import f8.exceptions.F8Exception;
import f8.exceptions.ICalcErr;
import f8.platform.Calc;

/**
 * @author elinares
 * 
 * 
 * 
 */
public final class ENTER extends Tecla {
	public ENTER() {
		super("ENTER");
	}

	public void pressed() throws F8Exception {
		Core.saveStack();
		if (Calc.ref.isEditing()) {
			Calc.ref.flush();
		} else {
			if (Core.check(1)) {
				Calc.ref.flush();
				new DUP().exec(null);
			} else {
				Calc.ref.output(ICalcErr.TooFewArguments);
			}
		}
	}
}
