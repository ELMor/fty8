/*
 * Created on 28-jul-2003
 *
 
 
 */
package f8.keyboard;

import f8.Core;
import f8.commands.Command;
import f8.exceptions.F8Exception;
import f8.objects.types.Lista;
import f8.objects.types.Literal;
import f8.objects.types.Unit;
import f8.platform.Calc;

/**
 * @author elinares
 * 
 * 
 * 
 */
public final class TeclaCST extends UnitMenu {
	Command main;

	Lista acc = null;

	public TeclaCST(Command ob) {
		super((ob instanceof Unit) ? ((Unit) ob).getUnidad().toString() : ob
				.toString());
		main = ob;
	}

	public TeclaCST(Command ob, Lista l) {
		this(ob);
		acc = l;
	}

	public Command getAction(int shiftMode) throws F8Exception {
		
		if (acc == null) {
			Command lo = Core.lookup(toString());
			return lo == null ? new Literal(toString()) : lo;
		}
		if (acc.size() >= shiftMode) {
			return acc.get(shiftMode);
		}
		return new Literal(toString());
	}

	public void pressed() throws F8Exception {
		int index = Calc.ref.getShiftMode();
		if ((acc == null) || (acc.size() == 0) || (index > acc.size())) {
			if (main instanceof Unit) {
				super.pressed();
			} else {
				main.exec(null);
			}
		} else {
			acc.get(index).exec(null);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.ui.TeclaVirtual#isDir()
	 */
	public boolean isDir() {
		return false;
	}
}
