/*
 * Created on 25-ago-2003
 *
 
 
 */
package f8.keyboard;

import f8.Core;
import f8.commands.math.simple.DIV;
import f8.commands.math.simple.TIMES;
import f8.commands.units.CONVERT;
import f8.exceptions.F8Exception;
import f8.platform.Calc;

/**
 * @author elinares
 * 
 * 
 * 
 */
public class UnitMenu extends WithEnter {
	public UnitMenu(String name) {
		super(name);
	}

	public void pressed() throws F8Exception {
		switch (Calc.ref.getShiftMode()) {
		case 0: // no shift
			Calc.ref.flush();
			Core.enter(Calc.ref.getEditText() + " 1_" + getTitle());
			new TIMES().exec(null);
			break;
		case 1: // left
			Calc.ref.flush();
			Core.enter(Calc.ref.getEditText() + " 1_" + getTitle());
			new CONVERT().exec(null);
			break;
		case 2: // right
			Calc.ref.flush();
			Core.enter(Calc.ref.getEditText() + " 1_" + getTitle());
			new DIV().exec(null);
			break;
		}
	}
}
