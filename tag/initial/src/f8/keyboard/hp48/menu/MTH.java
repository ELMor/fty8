/*
 * Created on 22-jul-2003
 *
 
 
 */
package f8.keyboard.hp48.menu;

import f8.ICalc;
import f8.keyboard.Menu;
import f8.keyboard.TeclaVirtual;
import f8.keyboard.WithEnter;

/**
 * @author elinares
 * 
 * 
 * 
 */
public final class MTH extends Menu {
	public MTH() {
		super("MTH"); // Tecla MTH
		appendKey(partsMenu());
		appendKey(probMenu());
		appendKey(hypMenu());
		appendKey(matrMenu());
		appendKey(vectrMenu());
		appendKey(baseMenu());
	}

	private TeclaVirtual partsMenu() {
		Menu m = new Menu("PART");
		m.appendKey(new WithEnter("ABS"));
		m.appendKey(new WithEnter("SIGN"));
		m.appendKey(new WithEnter("CONJ"));
		m.appendKey(new WithEnter("ARG"));
		m.appendKey(new WithEnter("RE"));
		m.appendKey(new WithEnter("IM"));
		m.appendKey(new WithEnter("MIN"));
		m.appendKey(new WithEnter("MAX"));
		m.appendKey(new WithEnter("MOD"));
		m.appendKey(new WithEnter("%"));
		m.appendKey(new WithEnter("%CH"));
		m.appendKey(new WithEnter("%T"));
		m.appendKey(new WithEnter("MANT"));
		m.appendKey(new WithEnter("XPON"));
		m.appendKey(new WithEnter("IP"));
		m.appendKey(new WithEnter("FP"));
		m.appendKey(new WithEnter("FLOOR"));
		m.appendKey(new WithEnter("CEIL"));
		m.appendKey(new WithEnter("RND"));
		m.appendKey(new WithEnter("TRNC"));
		m.appendKey(new WithEnter("MAXR"));
		m.appendKey(new WithEnter("MINR"));
		m.appendKey(new WithEnter("PRIME"));
		return m;
	}

	private TeclaVirtual probMenu() {
		Menu m = new Menu("PROB");
		m.appendKey(new WithEnter("COMB"));
		m.appendKey(new WithEnter("PERM"));
		m.appendKey(new WithEnter("!"));
		m.appendKey(new WithEnter("RAND"));
		m.appendKey(new WithEnter("RDZ"));
		return m;
	}

	private TeclaVirtual hypMenu() {
		Menu m = new Menu("HYP");
		m.appendKey(new WithEnter("SINH"));
		m.appendKey(new WithEnter("ASINH"));
		m.appendKey(new WithEnter("COSH"));
		m.appendKey(new WithEnter("ACOSH"));
		m.appendKey(new WithEnter("TANH"));
		m.appendKey(new WithEnter("ATAN"));
		m.appendKey(new WithEnter("EXPM"));
		m.appendKey(new WithEnter("LNP1"));
		return m;
	}

	private TeclaVirtual matrMenu() {
		Menu m = new Menu("MATR");
		m.appendKey(new WithEnter("CON"));
		m.appendKey(new WithEnter("IDN"));
		m.appendKey(new WithEnter("TRN"));
		m.appendKey(new WithEnter("RDM"));
		m.appendKey(new WithEnter("DET"));
		m.appendKey(new WithEnter("RSD"));
		m.appendKey(new WithEnter("ABS"));
		m.appendKey(new WithEnter("RNRM"));
		m.appendKey(new WithEnter("CNRM"));
		return m;
	}

	private TeclaVirtual vectrMenu() {
		Menu m = new Menu("VECT");
		m.appendKey(new WithEnter("XYZ", "3D_1", "3D", ICalc.MOD_3D, ICalc.XYZ));
		m.appendKey(new WithEnter("RAZ", "3D_2", "3D", ICalc.MOD_3D, ICalc.RAZ));
		m.appendKey(new WithEnter("RAA", "3D_3", "3D", ICalc.MOD_3D, ICalc.RAA));
		m.appendKey(new WithEnter("CROSS"));
		m.appendKey(new WithEnter("DOT"));
		m.appendKey(new WithEnter("ABS"));
		m.appendKey(new WithEnter("V\u008D"));
		m.appendKey(new WithEnter("\u008DV2"));
		m.appendKey(new WithEnter("\u008DV3"));
		m.appendKey(new WithEnter("D\u008DR"));
		m.appendKey(new WithEnter("R\u008DD"));
		return m;
	}

	private TeclaVirtual baseMenu() {
		Menu m = new Menu("BASE");
		m.appendKey(new WithEnter("HEX", null, "IM", ICalc.INT_MOD, ICalc.HEX));
		m.appendKey(new WithEnter("DEC", null, "IM", ICalc.INT_MOD, ICalc.DEC));
		m.appendKey(new WithEnter("OCT", null, "IM", ICalc.INT_MOD, ICalc.OCT));
		m.appendKey(new WithEnter("BIN", null, "IM", ICalc.INT_MOD, ICalc.BIN));
		m.appendKey(new WithEnter("STWS"));
		m.appendKey(new WithEnter("RCWS"));
		m.appendKey(new WithEnter("RL"));
		m.appendKey(new WithEnter("RR"));
		m.appendKey(new WithEnter("RLB"));
		m.appendKey(new WithEnter("RRB"));
		m.appendKey(new WithEnter("R\u008DB"));
		m.appendKey(new WithEnter("B\u008DR"));
		m.appendKey(new WithEnter("SL"));
		m.appendKey(new WithEnter("SR"));
		m.appendKey(new WithEnter("SLB"));
		m.appendKey(new WithEnter("SRB"));
		m.appendKey(new WithEnter("ASR"));
		m.appendKey(new WithEnter(" "));
		m.appendKey(new WithEnter("AND"));
		m.appendKey(new WithEnter("OR"));
		m.appendKey(new WithEnter("XOR"));
		m.appendKey(new WithEnter("NOT"));
		return m;
	}
}
