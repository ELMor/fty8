/*
 * Created on 22-jul-2003
 *
 
 
 */
package f8.keyboard.hp48.menu;

import f8.keyboard.Menu;
import f8.keyboard.Tecla;

/**
 * @author elinares
 * 
 * 
 * 
 */
public final class PRINT extends Menu {
	public PRINT() {
		super("PRINT");
		appendKey(new Tecla(" "));
	}
}
