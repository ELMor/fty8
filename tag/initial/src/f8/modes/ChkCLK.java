package f8.modes;

import java.io.IOException;

import f8.Core;
import f8.ICalc;
import f8.commands.Storable;
import f8.platform.io.DIS;
import f8.platform.io.DOS;

public final class ChkCLK extends CheckGroup {
	public ChkCLK() {
		// Aqui estaba systemInstall
	}

	public int getID() {
		return 496;
	}

	public Storable getInstance() {
		return new ChkCLK();
	}

	public void loadState(DIS ds) throws IOException {
	}

	public void saveState(DOS ds)  throws IOException {
	}

	public boolean execBefore() {
		
		Core.setSettings(ICalc.CLOCK, !Core.isSetting(ICalc.CLOCK));
		return false;
	}

	public String toString() {
		return ("ChkCLK_");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.mod.CheckGroup#getGroupName()
	 */
	public String getGroupName() {
		return "CK";
	}
}
