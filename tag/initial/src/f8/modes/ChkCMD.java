package f8.modes;

import java.io.IOException;

import f8.Core;
import f8.ICalc;
import f8.commands.Storable;
import f8.platform.io.DIS;
import f8.platform.io.DOS;

public final class ChkCMD extends CheckGroup {
	public ChkCMD() {
		// Aqui estaba systemInstall
	}

	public int getID() {
		return 490;
	}

	public Storable getInstance() {
		return new ChkCMD();
	}

	public void loadState(DIS ds) throws IOException {
	}

	public void saveState(DOS ds)  throws IOException {
	}

	public boolean execBefore() {
		
		Core.setSettings(ICalc.LAS_CMD, !Core.isSetting(ICalc.LAS_CMD));
		return false;
	}

	public String toString() {
		return ("ChkCMD_");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.mod.CheckGroup#getGroupName()
	 */
	public String getGroupName() {
		return "CM";
	}
}
