package f8.modes;

import java.io.IOException;

import f8.Core;
import f8.ICalc;
import f8.commands.Storable;
import f8.platform.io.DIS;
import f8.platform.io.DOS;

public final class GRAD extends CheckGroup {
	public GRAD() {
		// Aqui estaba systemInstall
	}

	public int getID() {
		return 487;
	}

	public Storable getInstance() {
		return new GRAD();
	}

	public void loadState(DIS ds) throws IOException {
	}

	public void saveState(DOS ds)  throws IOException {
	}

	public boolean execBefore() {
		
		Core.setSettings(ICalc.ANG_MOD, ICalc.GRAD);
		return false;
	}

	public String toString() {
		return ("GRAD");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.mod.CheckGroup#getGroupName()
	 */
	public String getGroupName() {
		return "AM";
	}
}
