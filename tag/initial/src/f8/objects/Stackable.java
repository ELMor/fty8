/*
 * Created on 22-ago-2003
 *
 
 
 */
package f8.objects;

import java.io.IOException;
import java.util.Vector;

import antlr.CommonAST;
import antlr.RecognitionException;
import antlr.TokenStreamException;
import antlr.collections.AST;
import f8.commands.Command;
import f8.commands.alg.Copiable;
import f8.exceptions.F8Exception;
import f8.exceptions.ParseException;
import f8.kernel.yacc.ObjectLexer;
import f8.kernel.yacc.ObjectParser;
import f8.platform.io.DIS;
import f8.platform.io.DOS;

/**
 * @author elinares
 * 
 * 
 * 
 */
public abstract class Stackable extends Command implements Copiable {
	public String tag = null;

	public abstract String getTypeName();

	public abstract Stackable copia();

	/**
	 * Descompone el objeto en sus elementos y los mete en la pila
	 * 
	 * @param cl
	 *            CalcLogica
	 * @return true si hubo un error
	 * @throws F8Exception
	 */
	public abstract boolean decompose() throws F8Exception;

	public static void parse(String input, Vector<CommonAST> vItems) throws F8Exception,TokenStreamException {
		ObjectLexer lexer = new ObjectLexer(new StringReader(input)) ;
		ObjectParser parser = new ObjectParser(lexer);
		try {
			parser.hp48Object(vItems);
		} catch (RecognitionException e) {
			throw new ParseException(e.getMessage());
		}
	}

	public AST getAST() {
		Vector<CommonAST> v = new Vector<CommonAST>();
		try {
			parse(toString(), v);
		} catch (F8Exception e) {
			return null;
		} catch(TokenStreamException tse){
			return null;
		}
		return (AST) v.elementAt(0);
	}

	/**
	 * @return
	 */
	public String getTag() {
		return (tag == null) ? "" : tag;
	}

	/**
	 * @param string
	 */
	public void setTag(String string) {
		if (string == null) {
			tag = "";
			return;
		}
		if (string.startsWith("\"")) {
			string = string.substring(1);
		}
		if (string.endsWith("\"")) {
			string = string.substring(0, string.length() - 1);
		}

		tag = string;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.platform.io.Storable#loadState(f8.platform.io.DataStream)
	 */
	public void loadState(DIS ds)  throws IOException {
		tag = ds.readStringSafe();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.platform.io.Storable#saveState(f8.platform.io.DataStream)
	 */
	public void saveState(DOS ds)  throws IOException {
		ds.writeStringSafe(tag);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.Command#isAlgebraic()
	 */
	public boolean isAlgebraic() {
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.Command#isInfix()
	 */
	public boolean isInfix() {
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.Command#isOperator()
	 */
	public boolean isOperator() {
		return false;
	}

	public abstract String getLaTeX();
}
