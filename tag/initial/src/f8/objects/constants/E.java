package f8.objects.constants;

import java.io.IOException;

import f8.commands.Storable;
import f8.commands.alg.Const;
import f8.objects.Stackable;
import f8.objects.types.Double;
import f8.objects.types.Ereal;
import f8.platform.io.DIS;
import f8.platform.io.DOS;

public final class E extends Const {
	public E() {
		// Aqui estaba systemInstall
	}

	public int getID() {
		return 2379;
	}

	public Storable getInstance() {
		return new E();
	}

	public void loadState(DIS ds) throws IOException {
	}

	public void saveState(DOS ds)  throws IOException {
	}

	public Stackable value() {
		return new Double(Ereal.E);
	}

	public String toString() {
		return ("e");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.StkObType#decompose(f8.kernel.CL)
	 */
	public boolean decompose() {
		return false;
	}

}
