package f8.objects.constants;

import java.io.IOException;

import f8.Core;
import f8.commands.Storable;
import f8.commands.alg.Const;
import f8.exceptions.F8Exception;
import f8.objects.Stackable;
import f8.objects.types.Complex;
import f8.objects.types.Ereal;
import f8.platform.io.DIS;
import f8.platform.io.DOS;

public final class I extends Const {
	public I() {
		// Aqui estaba systemInstall
	}

	public int getID() {
		return 65;
	}

	public Storable getInstance() {
		return new I();
	}

	public void loadState(DIS ds) throws IOException {
	}

	public void saveState(DOS ds)  throws IOException {
	}

	public void exec(Object extern) {
		
		Core.push(new Complex(Ereal.cero, Ereal.uno));
	}

	public String toString() {
		return ("i");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.Command#isAlgebraic()
	 */
	public boolean isAlgebraic() {
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.Command#isInfix()
	 */
	public boolean isInfix() {
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.Command#isOperator()
	 */
	public boolean isOperator() {
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.types.utils.Const#value()
	 */
	public Stackable value() {
		return new Complex(Ereal.cero, Ereal.uno);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.Stackable#decompose(f8.CL)
	 */
	public boolean decompose() throws F8Exception {
		return false;
	}

}
