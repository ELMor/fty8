package f8.objects.constants;

import java.io.IOException;

import f8.commands.Storable;
import f8.commands.alg.Const;
import f8.objects.Stackable;
import f8.objects.types.Double;
import f8.objects.types.Ereal;
import f8.platform.io.DIS;
import f8.platform.io.DOS;

public final class MAXR extends Const {
	public MAXR() {
		// Aqui estaba systemInstall
	}

	public int getID() {
		return 2380;
	}

	public Storable getInstance() {
		return new MAXR();
	}

	public void loadState(DIS ds) throws IOException {
	}

	public void saveState(DOS ds)  throws IOException {
	}

	public Stackable value() {
		return new Double(Ereal.MAXR);
	}

	public String toString() {
		return ("MAXR");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.StkObType#decompose(f8.kernel.CL)
	 */
	public boolean decompose() {
		return false;
	}

}
