package f8.objects.types;

import f8.commands.Command;
import f8.exceptions.F8Exception;
import f8.exceptions.IndexRangeException;
import f8.objects.Stackable;

public abstract class Array extends Stackable {

	abstract public int size();

	abstract public void put(Command v, int n) throws F8Exception;

	abstract public Command get(int n) throws IndexRangeException;

	abstract public Stackable copia();

	public String getTypeName() {
		return "Array";
	}

}
