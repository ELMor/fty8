package f8.objects.types;

import java.io.IOException;

import f8.commands.Storable;
import f8.objects.Stackable;
import f8.platform.io.DIS;
import f8.platform.io.DOS;

public final class Comment extends Stackable {
	String s;

	public Comment(String s) {
		this.s = s;
	}

	public Comment(){
		this(null);
	}
	
	public Stackable copia() {
		return new Comment(new String(s));
	}

	public String getTypeName() {
		return "Comment";
	}

	public int getID() {
		return 3;
	}

	public Storable getInstance() {
		return new Comment("");
	}

	public void loadState(DIS ds) throws IOException {
		super.loadState(ds);
		s = ds.readStringSafe();
	}

	public void saveState(DOS ds)  throws IOException {
		super.saveState(ds);
		ds.writeStringSafe(s);
	}

	public void exec(Object extern) {
	}

	public void store() {
	}

	public String toString() {
		return ("@" + s);
	}

	public String getLaTeX(){
		return toString();
	}
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.StkObType#decompose()
	 */
	public boolean decompose() {
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	public boolean equals(Object obj) {
		if (obj instanceof Comment) {
			return s.equals(((Comment) obj).s);
		}
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.Stackable#tolerance(java.lang.Object, double)
	 */
	public boolean tolerance(Object obj, double tol) {
		return equals(obj);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.Stackable#sign()
	 */
	public Stackable sign() {
		return new Double(0);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.Stackable#zero()
	 */
	public Stackable zero() {
		return zeroComment;
	}

	public static Comment zeroComment = new Comment("");

}
