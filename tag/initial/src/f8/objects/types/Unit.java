/*
 * Created on 16-ago-2003
 *
 
 
 */
package f8.objects.types;

import java.io.IOException;

import antlr.CommonAST;
import antlr.collections.AST;
import f8.Core;
import f8.apps.solver.Solverable;
import f8.commands.Command;
import f8.commands.Storable;
import f8.commands.math.mkl;
import f8.commands.math.exp.CARET;
import f8.commands.math.simple.DIV;
import f8.commands.math.simple.TIMES;
import f8.commands.units.UnitManager;
import f8.exceptions.F8Exception;
import f8.exceptions.InconsistentUnitsException;
import f8.exceptions.UndefinedException;
import f8.kernel.yacc.ObjectParserTokenTypes;
import f8.objects.Stackable;
import f8.platform.io.DIS;
import f8.platform.io.DOS;

/**
 * @author elinares
 * 
 * 
 * 
 */
public class Unit extends Stackable implements ObjectParserTokenTypes,
		Solverable {

	AST unidad = null;

	Ereal valor = new Ereal(0);

	public Unit(AST def) {
		if (def == null) {
			return;
		} else {
			valor = new Ereal(def.getText());
			unidad = normNumbers(def.getNextSibling());
		}
	}

	public Unit(){
		this(null);
	}
	
	public Unit(Ereal v, AST u) {
		valor = v;
		if (u == null) {
			return;
		}
		unidad = normNumbers(mkl.copyAST(u));
	}

	public Unit(double v, AST u){
		valor=new Ereal(""+v);
		if (u == null) {
			return;
		}
		unidad = normNumbers(mkl.copyAST(u));
	}
	
	public Unit(String value, AST unit) {
		this(new Ereal(value), unit);
	}

	public static Unit recEvalUnit(Unit inputUnit) throws F8Exception {
		AST nod = inputUnit.getUnidad();
		AST ch1 = null;
		AST ch2 = null;
		ch1 = nod.getFirstChild();
		if (ch1 != null) {
			ch2 = ch1.getNextSibling();
		}
		switch (nod.getType()) {
		case MULT:
			Core.push(recEvalUnit(new Unit(new Ereal(1), ch1)));
			Core.push(recEvalUnit(new Unit(new Ereal(1), ch2)));
			new TIMES().exec(null);
			return (Unit) Core.pop();
		case DIV:
			if (ch1.getType() == NUMBER) {
				Core.push((Stackable) Command.createFromAST(ch1));
			} else {
				Core.push(recEvalUnit(new Unit(new Ereal(1), ch1)));
			}
			if (ch2.getType() == NUMBER) {
				Core.push((Stackable) Command.createFromAST(ch1));
			} else {
				Core.push(recEvalUnit(new Unit(new Ereal(1), ch2)));
			}
			new DIV().exec(null);
			return (Unit) Core.pop();
		case CARET:
			Core.push(recEvalUnit(new Unit(new Ereal(1), ch1)));
			Core.push((Stackable) Command.createFromAST(ch2));
			try {
				new CARET().exec(null);
			} catch (F8Exception e) {
			}
			return (Unit) Core.pop();
		case ID:
			Unit id = UnitManager.getUnitDef(nod.getText());
			if (id == null) {
				throw new UndefinedException(nod.getText());
			} else {
				if (id.getUnidad() == null) {
					return new Unit(inputUnit.valor, mkl
							.copyAST(inputUnit.unidad));
				} else {
					return id;
				}
			}
		}
		return null;
	}

	public Unit convert(Unit to) throws F8Exception {
		Unit thisBase = ubase();
		Unit toBase = new Unit(new Ereal(1), to.unidad).ubase();
		if (!mkl.opEquals(thisBase.getUnidad(), toBase.getUnidad())) {
			throw new InconsistentUnitsException(this);
		}
		return new Unit(thisBase.valor.div(toBase.valor), to.getUnidad());
	}

	public Stackable copia() {
		return new Unit(valor, mkl.copyAST(unidad));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.kernel.StkObType#decompose(f8.kernel.CL)
	 */
	public boolean decompose() {
		Core.push(new Double(valor));
		Core.push(new Unit(new Ereal(1), unidad));
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.pfr.io.Storable#getID()
	 */
	public int getID() {
		return 118;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.pfr.io.Storable#getInstance()
	 */
	public Storable getInstance() {
		return new Unit(null);
	}

	public String getTypeName() {
		return "Unit";
	}

	public AST getUnidad() {
		return unidad;
	}

	public Ereal getValor() {
		return valor;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.pfr.io.Storable#loadState(f8.pfr.io.DataStream)
	 */
	public void loadState(DIS ds) throws IOException {
		super.loadState(ds);
		valor = new Ereal(ds.readStringSafe());
		AST[] un = new CommonAST[1];
		Command.loadState(ds, un);
		unidad = un[0];
	}

	public AST normNumbers(AST nval) {
		if (nval == null) {
			return null;
		}
		switch (nval.getType()) {
		case NUMBER:
			nval.setText("1");
			return nval;
		case CARET:
			AST base = nval.getFirstChild();
			mkl.subst(nval, base, normNumbers(base));
			return nval;
		default:
			AST child = nval.getFirstChild();
			for (; child != null; child = child.getNextSibling()) {
				mkl.subst(nval, child, normNumbers(child));
			}

			return nval;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.pfr.io.Storable#saveState(f8.pfr.io.DataStream)
	 */
	public void saveState(DOS ds)  throws IOException {
		super.saveState(ds);
		ds.writeStringSafe(valor.toString());
		Command.saveState(ds, unidad);
	}

	/**
	 * @param d
	 */
	public void setValor(Ereal d) {
		valor = new Ereal(d);
	}

	public String toString() {
		String ret = getTag();
		if (!ret.equals("")) {
			ret += ":";
		}
		return ret+Double.dtos(new Double(valor))+unit2String();
	}

	private String unit2String() {
		if(unidad==null)
			return "_";
		if (mkl.opEquals(unidad, mkl.uno)) {
			return "";
		}
		String primary = InfixExp.forma(unidad);
		if (primary.equals("angulo")) {
			return "";
		} else if (primary.equals("arcos")) {
			return "";
		} else {
			return "_" + primary;
		}
	}

	public Unit ubase() throws F8Exception {
		Ereal mul = getValor();
		setValor(new Ereal(1));
		Unit res = recEvalUnit(this);
		setValor(mul);
		if (res != null) {
			return new Unit(res.getValor().mul(mul), res.getUnidad());
		} else {
			return this;
		}
	}

	public Unit ufact(Unit to) throws F8Exception {
		AST toTree = mkl.copyAST(to.getUnidad());
		Core.push(ubase());
		to.setValor(new Ereal(1));
		Core.push(to.ubase());
		new DIV().exec(null);
		Core.push(new Unit(new Ereal(1), mkl.colect(toTree)));
		new TIMES().exec(null);
		Unit u = (Unit) Core.pop();
		return new Unit(u.valor, u.getUnidad());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	public boolean equals(Object obj) {
		if (obj instanceof TempUnit || obj instanceof Unit) {
			Unit cobj = (Unit) obj, ub1, ub2;
			try {
				ub1 = ubase();
				ub2 = cobj.ubase();
			} catch (F8Exception g) {
				return false;
			}
			return ub1.valor == ub2.valor
					&& mkl.opEquals(ub1.unidad, ub2.unidad);
		} else {
			return false;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.Stackable#tolerance(java.lang.Object, double)
	 */
	public boolean tolerance(Solverable obj, Ereal tol) {
		if (obj instanceof TempUnit || obj instanceof Unit) {
			Unit cobj = (Unit) obj, ub1, ub2;
			try {
				ub1 = ubase();
				ub2 = cobj.ubase();
			} catch (F8Exception g) {
				return false;
			}
			return (ub1.valor.minus(ub2.valor).lt(tol))
					&& mkl.opEquals(ub1.unidad, ub2.unidad);
		} else {
			return false;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.symbolic.Solverable#sign()
	 */
	public int sign() {
		Ereal v = new Ereal(0);
		try {
			v = ubase().valor;
		} catch (F8Exception e) {

		}
		return v.isNegative() ? -1 : (v.isZero()? 0 : 1);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.symbolic.Solverable#zero()
	 */
	public Solverable zero() {
		return new Unit(new Ereal(0), unidad);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.symbolic.Solverable#amplia(double)
	 */
	public Solverable amplia(Ereal pct) {
		Ereal k = new Ereal(valor);
		if (k.isZero())
			return new Unit(pct, unidad);
		if (k.isNegative())
		    k=k.mul(new Ereal(1).minus(pct));
		else
		    k=k.mul(new Ereal(1).plus(pct));
		return new Unit(k, unidad);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.symbolic.Solverable#compare(f8.symbolic.Solverable)
	 */
	public Ereal compare(Solverable obj) {
		if (obj instanceof Unit) {
			Unit o = (Unit) obj;
			try {
				Unit u1 = ubase(), u2 = o.ubase();
				return u1.valor.minus(u2.valor);
			} catch (F8Exception e) {
			}
		}
		return new Ereal(0);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.symbolic.Solverable#mean(f8.symbolic.Solverable,
	 *      f8.symbolic.Solverable)
	 */
	public Solverable mean(Solverable o1, Solverable o2) {
		if (o1 instanceof Unit && o2 instanceof Unit) {
			Unit a = (Unit) o1, b = (Unit) o2;
			try {
				Unit converted = b.convert(a);
				return new Unit(a.valor.plus(b.valor).div(new Ereal(2)), a.unidad);
			} catch (F8Exception e) {
			}
		} else if (o1 instanceof Unit && o2 instanceof Unit) {
			Unit a = (TempUnit) o1, b = (TempUnit) o2;
			try {
				Unit converted = b.convert(a);
				return new Unit(a.valor.plus(b.valor).div(new Ereal(2)), a.unidad);
			} catch (F8Exception e) {
				e.printStackTrace();
			}
		}
		return new Double(0);
	}

	public String getLaTeX() {
		String tmp=""+valor;
		if(tmp.endsWith(".0"))
			tmp=tmp.substring(0,tmp.length()-2);
		return tmp+"\\cdot"+InfixExp.getLaTex(unidad);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see f8.Stackable#getAST()
	 */
	public AST getAST() {
		AST ret = new CommonAST();
		ret.setText("unit");
		ret.setType(UNIT);
		ret.addChild(mkl.num(valor));
		ret.addChild(mkl.copyAST(unidad));
		return ret;
	}

}
