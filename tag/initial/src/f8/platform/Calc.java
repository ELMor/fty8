/*
 * Created on 12-jun-2003
 *
 
 
 */
package f8.platform;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.Graphics;
import java.awt.KeyEventDispatcher;
import java.awt.KeyboardFocusManager;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.Date;
import java.util.Hashtable;
import java.util.Vector;

import antlr.CommonAST;
import antlr.TokenStreamException;
import antlr.collections.AST;
import f8.Core;
import f8.ICalc;
import f8.IMachineStatus;
import f8.INotify;
import f8.apps.display.CSys;
import f8.commands.Command;
import f8.exceptions.DebugHaltException;
import f8.exceptions.F8Exception;
import f8.exceptions.ICalcErr;
import f8.keyboard.ENTER;
import f8.keyboard.Menu;
import f8.keyboard.ShiftKey;
import f8.keyboard.TeclaVirtual;
import f8.keyboard.WithEnter;
import f8.keyboard.hp48.Keyboard;
import f8.keyboard.hp48.menu.MTH;
import f8.objects.Stackable;
import f8.objects.types.Complex;
import f8.objects.types.Directory;
import f8.objects.types.Ereal;
import f8.objects.types.Grob;
import f8.objects.types.Real;
import f8.platform.F8Font._Viewer;
import f8.platform.glyph.Glyph;
import f8.platform.io.DIS;
import f8.platform.io.DOS;
import f8.platform.io.StMan;

/**
 * @author  elinares
 * 
 */
public final class Calc 
	extends Frame 
	implements ICalcErr, ICalc, INotify, IMachineStatus, Runnable 
{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 166071808639804662L;

	public static final boolean debug=true;
	
	/**
	 * Dibuja toda la calculadora
	 */
	public Screen pantalla;

	/**
	 * Referencia estatica a la unica calculadora
	 */
	public static Calc ref = null;

	/**
	 * true si se ha de mostrar un Alert hasta que se presione una tecla
	 */
	boolean alertDisp = false;
	/**
	 * contiene System.currentTimeMillis();
	 */
	long alertBeginNow=-1;
	/**
	 * Contenido del mensaje de alerta
	 */
	String alertMsg = "";
	/**
	 * Clase que contiene la funcionalidad de dibujar funciones
	 */
	static Plot plot = null;
	/**
	 * texto que se encuentra en edicion actualmente
	 */
	public String editText = "";
	/**
	 * Posicion del cursor de edicion dentro de editText
	 */
	public int editTextInsertPos = 0;
	/**
	 * Numero de menus din�micos por pagina 
	 */
	private static int kpp = 6;
	/**
	 * true si se est� introducionendo por linea de edicion un programa
	 */
	private static boolean pgmMode = false;
	/**
	 * Pagina de teclado actual
	 * 0 numerica
	 * 1 menus
	 * 2 alfanumerica
	 * 3 letras griegas
	 */
	public static int keybPage = 0;
	/**
	 * Continene el teclado
	 */
	public static Keyboard keyb = null;

	/**
	 * posicion de la pila que se esta editando
	 */
	public static int interStackEditing=0;
	public static final int SCRMODE_STACK = 1;
	public static final int SCRMODE_PLOT = 2;
	public static final int SCRMODE_INTERACTIVE=3;
	
	/**
	 * Modo de display de la pila
	 */
	public static int scrMode = SCRMODE_STACK;
	
	/**
	 * Indica si la calculadora esta ocupada
	 */
	private int busyCount=0;
	
	private static final String saveState="savestate.f8";
	
	int sf=16;
	int mf=14;
	int nf=12;
	int yf=10;

    /**
     * Control del Raton
     */
	int gestureInitX=-1;
	int gestureInitY=-1;
    
	private class Raton implements MouseListener {
		@Override
		public void mouseClicked(MouseEvent e) {
			
		}
		@Override
		public void mousePressed(MouseEvent e) {
			gestureInitX=e.getX();
			gestureInitY=e.getY();
		}
		@Override
		public void mouseReleased(MouseEvent e) {
			int x=e.getX();
			int y=e.getY();
			int dx=Math.abs(x-gestureInitX);
			int dy=Math.abs(y-gestureInitY);
			if(dx<10 && dy<10){//Poca diferencia entre pendown y penup
				pantalla.clickScreen(x, y);
			}else if(dx>40 || dy>40){ //gesture
				pantalla.gestureScreen(x, y, dx, dy);
			}
		}
		@Override
		public void mouseEntered(MouseEvent e) {
			
		}
		@Override
		public void mouseExited(MouseEvent e) {
			
		}
		
	}
	
	
	/**
	 * Control del teclado
	 */
	int keyAction = -999;
	Character charAction = null;
	boolean longPress = false;
	long beginKeyPress = 0;

    private class Teclado implements KeyEventDispatcher {
        @Override
        public boolean dispatchKeyEvent(KeyEvent e) {
            if (e.getID() == KeyEvent.KEY_PRESSED) {
				longPress = false;
				beginKeyPress = System.currentTimeMillis();
				keyAction = e.getKeyCode();
				switch(e.getKeyCode()){
					case KeyEvent.VK_UP:	  	
					case KeyEvent.VK_DOWN:	  	
					case KeyEvent.VK_LEFT:	  	
					case KeyEvent.VK_RIGHT:	  	
					case KeyEvent.VK_BACK_SPACE:
						charAction=null;
						break;
					default:
						charAction= e.getKeyChar();
				}
            } else if (e.getID() == KeyEvent.KEY_RELEASED) {
				if(!longPress){
					enqueueKeyAction(e.getKeyCode(),false,charAction);
				}
				longPress=false;
				beginKeyPress=0;
            } else if (e.getID() == KeyEvent.KEY_TYPED) {
            }
            return false;
        }
    } 
	/*
	 * Control de salida de la aplicacion
	 */
    
    class F8WindowExiting implements WindowListener{
		@Override
		public void windowOpened(WindowEvent e) {
		}
		
		@Override
		public void windowIconified(WindowEvent e) {
		}
		
		@Override
		public void windowDeiconified(WindowEvent e) {
		}
		
		@Override
		public void windowDeactivated(WindowEvent e) {
			
		}
		@Override
		public void windowClosing(WindowEvent e) {
			destroyApp(true);
			try {
				Thread.sleep(200);
			} catch (InterruptedException e1) {
				e1.printStackTrace();
			}
			System.exit(0);
		}
		
		@Override
		public void windowClosed(WindowEvent e) {
		}
		
		@Override
		public void windowActivated(WindowEvent e) {
		}
    }
    
	public Calc() {
		// Siempre primero hay que instanciar la logica PRIMERO
		ref = this;
		StMan.init();
		DIS loader = null;
		// Escogemos el primer root
		// Nos aseguramos de que existe el archivo
		if(StMan.exists(saveState))
			loader=StMan.getDIS(saveState);
		else 
			loader=null;
		// CARGA PRINCIPAL
		int pcts=60;
		int kbsc=2;
		int kbsr=2;
		try {
			Core.init(this, loader);
			if (loader != null) {
				dynamicSelected=loader.readBoolean();
				dynamic=Menu.getMenuNumber(loader.readInt());
				dynamic.page=loader.readInt();
				dynamic.index=loader.readInt();
				editTextInsertPos = loader.readInt();
				editText = loader.readStringSafe();
				if(editText==null)
					editText="";
				pcts=loader.readInt();
				yf=loader.readInt();
				nf=loader.readInt();
				mf=loader.readInt();
				sf=loader.readInt();
				kbsc=loader.readInt();
				kbsr=loader.readInt();
				loader.close();
			}
		} catch (IOException e1) {
			output(e1.getMessage());
			loader = null;
		} catch (IllegalAccessException iae) {
			output(iae.getMessage());
			loader = null;
		} catch(RuntimeException rte){
			output(rte.getMessage());
			loader=null;
		}
		// CREACION DE MENUS
		HP48();
		// PRESENTACION
		pantalla = new Screen(pcts,kbsc,kbsr);
		add(pantalla);
		//Creacion de hebra de procesamiento
		new Thread(this).start();
		//Escuchamos el Teclado
        KeyboardFocusManager manager = KeyboardFocusManager.getCurrentKeyboardFocusManager();
        manager.addKeyEventDispatcher(new Teclado());
        //Exit
        addWindowListener(new F8WindowExiting());
	}

	int sizeOfStackFont,sizeOfMenuFont,sizeOfNormalFont,sizeOfNotifyFont;
	F8Font notifFont,dynamicBlack,dynamicWhite,normal,stackFont;

	public void initFonts(){
		initFonts(yf,nf,mf,sf);
	}
	
	public void initFonts(int n1, int n2, int dynButtonFontSize, int n4) {
		try {
		if(n1>0){
			sizeOfNotifyFont=n1;
			notifFont = new F8Font(this.getGraphics(), sizeOfNotifyFont, false);
		}
		if(n2>0){
			sizeOfNormalFont=n2;
			normal = new F8Font(this.getGraphics(), sizeOfNormalFont,false);
		}
		if(dynButtonFontSize>0){
			sizeOfMenuFont=dynButtonFontSize;
			dynamicBlack=new F8Font(this.getGraphics(), sizeOfMenuFont,false);
			dynamicWhite=new F8Font(this.getGraphics(), sizeOfMenuFont,true);
		}
		if(n4>0){
			sizeOfStackFont=n4;
			stackFont = new F8Font(this.getGraphics(), sizeOfStackFont,false);
			//Invalidamos la cache de imagenes
			pantalla.imageCache=new ImageCache(10);
		}
		for (int i = 0; i < pantalla.wmetrics.length; i++) {
			pantalla.wmetrics[i] = normal.getViewer(pantalla.buttNames[i]).getWidth();
		}
		pantalla.initGeometry(pantalla.pctScreen);
		F8StaticFonts.init(this.getGraphics());
		}catch(Exception e){
			throw new RuntimeException("No puedo cargar la fuente");
		}
	}

	boolean processingEvents=true;
	
	protected void destroyApp(boolean arg0) {
		//Terminamos las hebras de control de eventos
		processingEvents=false;
		pantalla.keepgoing=false;
		//Borramos el anterior fichero
		StMan.delete(saveState);
		DOS ds =StMan.getDOS(saveState);
		if (ds != null){
			try {
				Core.saveToStorage(ds);
				ds.writeBoolean(dynamicSelected);
				ds.writeInt(Menu.getNumberMenu(dynamic));
				ds.writeInt(dynamic.page);
				ds.writeInt(dynamic.index);
				ds.writeInt(editTextInsertPos);
				ds.writeStringSafe(editText);
				ds.writeInt(pantalla.pctScreen);
				ds.writeInt(sizeOfNotifyFont);
				ds.writeInt(sizeOfNormalFont);
				ds.writeInt(sizeOfMenuFont);
				ds.writeInt(sizeOfStackFont);
				ds.writeInt(pantalla.kbSelectedCol);
				ds.writeInt(pantalla.kbSelectedRow);
				ds.close();
			} catch (IOException e) {
			}
		}
	}

	protected void pauseApp() {
		//
	}


	public int[] getMachineStatus(){
		int ret[]=new int[8];
		ret[SM_SHIFT]=0;
		ret[SM_PGM]=pgmMode?1:0;
		ret[SM_ALG]=0;
		ret[SM_KEYBNDX]=keybPage;
		if(dynamic!=null){
			ret[SM_DYNPAG]=dynamic.page;
			ret[SM_DYNNDX]=dynamic.index;
		}
		ret[SM_SCREENMODE]=scrMode;
		ret[SM_ISEDITING]=(forceEditInteractive || isEditing()) ? 1 : 0;
		return ret;
	}
	
	
	public boolean isEditing() {
		return editText.length() > 0;
	}

	public String getEditText() {
		return editText;
	}

	public void setEditText(String txt) {
		editText = txt;
	}

	public int getEditTextInserPos() {
		return editTextInsertPos;
	}

	public void setEditTextInserPos(int i) {
		editTextInsertPos = i;
	}

	public void emit(String str, int backSpaces) {
		if ((str.indexOf("<<") >= 0) || (str.indexOf("\u00AB") >= 0)
				|| (str.indexOf("{") >= 0)) {
			Core.setSettings(PRG_MOD, true);
		}
		if (str.indexOf("'") >= 0) {
			Core.setSettings(ALG_MOD, true);
		}

		String oldContent = editText;
		editText = (oldContent.substring(0, editTextInsertPos) + str + oldContent
				.substring(editTextInsertPos));
		editTextInsertPos = (editTextInsertPos + str.length()) - backSpaces;
	}

	public boolean flush() {
		boolean ret = false;
		try {
			if (!editText.equals("")) {
				String holder = editText + "";
				editText = "";
				editTextInsertPos = 0;
				Core.enter(holder);
				Core.setSettings(ICalc.ALG_MOD, false);
				Core.setSettings(ICalc.PRG_MOD, false);
			}
		} catch (F8Exception e) {
			Core.error(e);
			ret = true;
		}
		return ret;
	}

	public Plot getLCD() {
		return plot;
	}

	public void log(String txt) {
		// Vm.debug(txt);
	}

	public void output(int errNumber) {
		output(ICalcErr.Name[errNumber]);
	}

	public void output(String msg) {
		alertMsg = msg;
		alertDisp = true;
		alertBeginNow=System.currentTimeMillis();
	}

	private static Real.NumberFormat numberFormat=null;
	public Real.NumberFormat getNumberFormat(){
		if(numberFormat==null){
			numberFormat=new Real.NumberFormat();
			loadNumberFormat();
		}
		return numberFormat;
	}

	public void loadNumberFormat() {
		switch(Core.getSetting(ICalc.DOU_MOD)){
		case ICalc.STD: numberFormat.fse=Real.NumberFormat.FSE_NONE; break;
		case ICalc.SCI: numberFormat.fse=Real.NumberFormat.FSE_SCI; break;
		case ICalc.ENG: numberFormat.fse=Real.NumberFormat.FSE_ENG; break;
		case ICalc.FIX: numberFormat.fse=Real.NumberFormat.FSE_FIX; break;
		}
		numberFormat.precision=Core.getSetting(ICalc.DOU_PRE);
		if(Core.getSetting(ICalc.PER_MOD)==ICalc.COMMA){
			numberFormat.point=',';
			numberFormat.thousand='.';
		}else{
			numberFormat.point='.';
			numberFormat.thousand=',';
		}
		//Hay que repintar
		pantalla.imageCache.invalidateImageCache();
	}
	
	static final String busyChars=".oOo";
	int busyPos=0,busyLen=busyChars.length();
	public String getStatusLabel(int labelNumber) {
		switch (labelNumber) {
		case 0:
			// Lef o Right
			return "";
		case 1:
			if(busyCount>0)
				return busyChars.substring(busyPos,busyPos+1);
			return "";
		case 2:
			// RAD, DEG, GRAD
			switch (Core.getSetting(ICalc.ANG_MOD)) {
			case ICalc.RAD:
				return "RAD";
			case ICalc.DEG:
				return "DEG";
			case ICalc.GRAD:
				return "GRAD";
			}
			return "";
		case 3:
			// XYZ, RAZ, RAA
			switch (Core.getSetting(ICalc.MOD_3D)) {
			case ICalc.XYZ:
				return "XYZ";
			case ICalc.RAZ:
				return "RAZ";
			case ICalc.RAA:
				return "RAA";
			}
			return "";
		case 4:
			// ALG, PRG
			if (Core.isSetting(ICalc.ALG_MOD))
				return "ALG";
			if (Core.isSetting(ICalc.PRG_MOD))
				return "PRG";
			return "";
			// CLOCK
		case 5:
			if (Core.isSetting(ICalc.CLOCK)) {
				String ret = new Date().toString();
				int ndx = ret.indexOf("UTC");
				if (ndx > -1)
					ret = ret.substring(0, ndx);
				ndx = ret.indexOf("GMT");
				if (ndx > -1)
					ret = ret.substring(0, ndx);
				return ret.substring(0, ret.length());
			}else{
				return "";
			}
		}
		return "";
	}

	public void setBusy(){
		busyCount++;
	}
	
	public void clearBusy(){
		busyCount--;
	}
	
	public void setInitialEdit(String s) {
		editText = s;
	}

	public Keyboard getKeyb() {
		return keyb;
	}

	public int getKeybPage() {
		return keybPage;
	}

	/**
	 * Si se ha seleccionado el menu dinamico (teclas de pantalla)
	 */
	public boolean dynamicSelected = false;

	public static Menu dynamic = null;

	/**
	 * Edicion de la pila
	 */
	public boolean stackEditing = false;

	public int StackEditingLevel = 0;

	public void HP48() {
		keyb = new Keyboard(this,12);
		if(dynamic==null)
			dynamic = new MTH();
	}

	public void setDynamic(Menu m) {
		dynamic = m;
		pantalla.dynamicNeedRefresh=true;
	}

	public boolean isDynSelected() {
		return dynamicSelected;
	}

	public int getDynIndex() {
		return dynamic.index;
	}

	public void nxtDynPage() {
		int ums = dynamic.size();
		if (ums > kpp) {//Si hay mas de una pagina
			dynamic.page++;
			if(dynamic.page*kpp>ums)//ciclico
				dynamic.page=0;
		}
	}

	public void preDynPage() {
		int ums = dynamic.size();
		if (ums > kpp) { //Si hay mas de una pagina
			dynamic.page--;
			if(dynamic.page<0){
				dynamic.page=ums/kpp;
				dynamic.index=ums-dynamic.page*kpp-1;
			}
		}
	}

	private void nxtDynNdx() {
		if (++dynamic.index >= kpp) {
			dynamic.index = 0;
			nxtDynPage();
		}
		if(dynamic.page*kpp+dynamic.index>=dynamic.size()){
			dynamic.index=dynamic.page=0;
		}
	}

	private void preDynNdx() {
		if (--dynamic.index < 0) {
			dynamic.index = kpp-1;
			preDynPage();
		}
	}

	public boolean getPgmMode() {
		return pgmMode;
	}

	public TeclaVirtual[] getUpArray() {
		dynamic.notifyChangeStatus();
		return dynamic.getContentArray(dynamic.page * kpp, kpp);
	}

	boolean forceEditInteractive=false;
	/**
	 * Ejecuta la tecla de usuario ENTER
	 */
	public void keyENTER(int shi) {
		if (dynamicSelected) {
			//Menu dinamicos
			//dynamicSelected = false;
			TeclaVirtual tv = dynamic.get(dynamic.page * kpp + dynamic.index);
			setShiftMode(shi);
			press(tv);
			setShiftMode(0);
			pantalla.dynamicNeedRefresh=true;
			pantalla.stackNeedRefresh=true;
			pantalla.keyboardNeedRefresh=true;
			dynamicSelected=false;
		}else if(scrMode==SCRMODE_INTERACTIVE){
			if(isEditing()){
				//Colocamos el objeto editado en su sitio
				Vector<CommonAST> vItems = new Vector<CommonAST>();
				try {
					Stackable.parse(editText, vItems);
					boolean first=true;
					while (vItems.size() > 0) {
						AST nxtObj = (AST) vItems.elementAt(0);
						if (nxtObj != null) {
							Command it = Command.createFromAST(nxtObj);
							if (it instanceof Stackable) {
								if(first)
									Core.poke(interStackEditing, it);
								else
									Core.insertAt(interStackEditing, it);
							}
						}
						vItems.removeElementAt(0);
						first=false;
					}
					editText="";
					scrMode=SCRMODE_STACK;
					interStackEditing=0;
					forceEditInteractive=false;
				} catch (F8Exception e) {
					output(e.getMessage());
				} catch (TokenStreamException tse){
					output(tse.getMessage());
				}
			}else{
				//Seleccionar el objeto de la pila para editar
				editText=((Stackable)(Core.peek(interStackEditing))).toString();
				editTextInsertPos=editText.length();
				forceEditInteractive=true;
			}
			pantalla.stackNeedRefresh=true;
		}  else if(scrMode==SCRMODE_PLOT){
			Core.push(new Complex(
					new Ereal(""+plot.cursor.v[0]),
					new Ereal(""+plot.cursor.v[1])
					             )
			);
		} else if(isLongPress()){
			try {
				new ENTER().pressed();
			}catch(F8Exception e){
				
			}
			pantalla.stackNeedRefresh=true;
		}else {
			int tn=pantalla.kbSelectedCol+3*pantalla.kbSelectedRow; 
			TeclaVirtual tv = keyb.get(tn).get(shi);
			shi = 0;
			press(tv);
			pantalla.stackNeedRefresh=true;
		}
	}

	public void press(TeclaVirtual vk) {
		try {
			vk.pressed();
		} catch (DebugHaltException h) {
			// setMenu(1, PRG.ctrlMenu(), null, null);
		} catch (F8Exception e) {
			Core.error(e);
		}

		// Comprobamos estado de Shift y Alpha status
		if (vk instanceof ShiftKey) {
			return;
		}

		if (getShiftMode() > 0) {
			setShiftMode(0);
			// setMenu(0, null, Keyboard.normMiddle(), Keyboard.normDown());
		}
	}

	public void setPgmMode(boolean em) {
		pgmMode = em;
	}

	private int shiftMode=0;
	public void setShiftMode(int smode) {
		shiftMode=smode;
	}
	public int getShiftMode() {
		return shiftMode;
	}
	/**
	 * @return
	 */
	public Menu getDynamic() {
		return dynamic;
	}

	/**
	 * @param menu
	 */
	public void setDynMenu(Menu menu) {
		dynamic = menu;
		dynamic.notifyChangeStatus();
	}

	public boolean[] getUpIsDirectory() {
		int dynPage=dynamic.page;
		boolean[] ret = new boolean[kpp];
		for (int i = dynPage * kpp; i < ((dynPage * kpp) + kpp); i++) {
			ret[i - (dynPage * kpp)] = dynamic.get(i)
					.isDir();
		}
		return ret;
	}

	public boolean[] getUpVarSolver() {
		int dynPage=dynamic.page;
		boolean[] col = new boolean[kpp];
		for (int i = dynPage * kpp; i < ((dynPage * kpp) + kpp); i++) {
			TeclaVirtual m = dynamic.get(i);
			col[i - (dynPage * kpp)] = m.isVarSolver();
		}
		return col;
	}

	public boolean[] getUpIsOn() {
		int dynPage=dynamic.page;
		boolean[] ret = new boolean[kpp];
		for (int i = dynPage * kpp; i < ((dynPage * kpp) + kpp); i++) {
			Object tec = dynamic.get(i);
			if (tec instanceof WithEnter) {
				ret[i - (dynPage * kpp)] = ((WithEnter) tec).isChecked();
			} else {
				ret[i - (dynPage * kpp)] = false;
			}
		}
		return ret;
	}

	/**
	 * @return
	 */
	public int getDynPage() {
		return dynamic.page;
	}

	/**
	 * @param i
	 */
	public void setUpMenuPage(int i) {
		dynamic.page = i;
	}

	public boolean isLongPress() {
		return pantalla.isLongPress();
	}

	private void movePlotCursor(CSys.Point d) {
		if (!isLongPress()) {
			plot.cursor = d;
		} else {
			plot.cursor = plot.cursor.plus(d.minus(plot.cursor).mul(new Ereal(5)));
		}
	}

	public void objReceived(String objDefinition){
		Vector<CommonAST> vItems=new Vector<CommonAST>();
		try {
			Stackable.parse(objDefinition, vItems);
			int j=0;
			String obxName;
			Hashtable<String, Directory> hDir=Core.homeDir.getHT();
			Directory obxd=null;
			try {
				obxd = (Directory)hDir.get("OBEXDIR");
				if(obxd==null){
					obxd=new Directory(Core.homeDir,"OBEXDIR");
					hDir.put("OBEXDIR", obxd);
				}
			} catch (ClassCastException e) {
				e.printStackTrace();
				Calc.ref.output("OBEXDIR no es un directorio");
				throw e;
			}
			for(int i=0;i<vItems.size();i++){
				do {
					obxName="RCV"+(++j);
				}while(obxd.get(obxName)!=null);
				obxd.put(obxName, 
						Command.createFromAST((AST)vItems.elementAt(i)));
			}
		} catch (F8Exception e) {
			output(e.getMessage());
		} catch(TokenStreamException tse){
			output(tse.getMessage());
		}
	}

	public class Screen extends Canvas implements Runnable {
		/**
		 * 
		 */
		private static final long serialVersionUID = -927626789405672585L;
		/**
		 * poner a true si el area de notificacion se necesita repintar
		 */
		public boolean notifyNeedRefresh = true;
		/**
		 * Poner a true si la pila necesita repintar
		 */
		public boolean stackNeedRefresh = true;
		/**
		 * Poner a true si los menus dinamicos necesitan repintar
		 */
		public boolean dynamicNeedRefresh = true;
		/**
		 * Poner a true si el teclado necesita repintar
		 */
		public boolean keyboardNeedRefresh = true;
		/**
		 * Porcentaje de pantalla dedicado a la pila
		 */
		int pctScreen;
		/**
		 * Tama�o del area de notificacion (height)
		 */
		int notifyAreaSize;
		/**
		 * Inicio (pixel y) de la pila
		 */
		int stackY;
		/**
		 * Altura en pixels del area de la pila
		 */
		int stackAreaSize;
		/**
		 * tama�o (altura) del area de edicion.
		 */
		int editAreaSize;
		/**
		 * Boton del teclado seleccionado
		 */
		int kbSelectedRow=2;
		int kbSelectedCol=2;
		/**
		 * Coordenada y de la esquina superior del area del teclado
		 */
		int keyboardY;
		/**
		 * altura en pixels del area del teclado
		 */
		int keyboardAreaSize;
		/**
		 * Posicion y del area de menus
		 */
		int dynButtY;
		/**
		 * altura del area de menus
		 */
		int dynButtAreaSize;
		/**
		 * Altura y anchura de la pantalla
		 */
		int altura, anchura;
		/**
		 * Numero de botones del menu dinamico
		 */
		int numDynButt = 6;
		/**
		 * numero de filas en el menu dinamico
		 */
		int numDynFilas=1;
		/**
		 * Numero de columnas en la botonera del teclado
		 */
		int colBot = 3;
		/**
		 * numero de filas en la botonera del teclado.
		 */
		int filBot = 4;
		/**
		 * Numero de lineas visibles de la pila
		 */
		int lineasVisibles;
		/**
		 * nombre de los botones. Aparecen dibujados en la esquina inferior
		 * derecha de cada boton del teclado con fondo rojo
		 */
		String buttNames[] = { "1", "2", "3", "4", "5", "6", "7", "8", "9",
				"*", "0", "#" };
		
		/**
		 * tama�o de cada uno de los nombres de los botones de buttNames
		 */
		int wmetrics[] = new int[buttNames.length];
		/**
		 * Color de resalto mde los botones
		 */
		final Color circleButtonsColor = new Color(0xAB0C22);
		/**
		 * Color de resalto de los menus dinamicos
		 */
		final Color circleSelectedDyn = new Color((97 * 256 + 182) * 256 + 191);
		/**
		 * Color del borde de los botones left y right
		 */
		final Color circleSelected = new Color(0x00);
		
		final Color bkgIndexButtonsColor = new Color((252 * 256 + 104) * 256 + 118);

		final Color bkgNotifycolor = new Color((180 * 256 + 180) * 256 + 180);

		final Color bkgLeftShift = new Color((238 * 256 + 156) * 256 + 110);

		final Color bkgRightShift = new Color((98 * 256 + 250) * 256 + 243);

		final Color bkgButton = new Color((139 * 256 + 151) * 256 + 179);

		final Color bkgStack = new Color((201 * 256 + 201) * 256 + 201);

		final Color bkgDyn = new Color(0x0);
		
		final Color scrollColor = new Color((58*256+24)*256+202);

		final Color bkgDynVarSolv = new Color(0xffffff);

		Color bkgEditing = bkgStack;

		Color bkgPlot = bkgStack;

		Color circleNonSelectedDyn = bkgStack;

		Color bkgAlert = new Color((215 * 256 + 204) * 256 + 187);

		Color cursorOnColor = new Color(0x0);

		public static final int lShift = -6;

		public static final int rShift = -7;

		boolean keepgoing = true;

		/**
		 *  Numero de lineas de edici�n
		 */
		int totalLineasEdicion = 0;
		
		int primerLineaVisibleEdicion = 0;

		String editArray[] = new String[250];

		String firstLevel[] = new String[20];

		int cursorY, cursorX, cursorLine;

		boolean blinkCursorStatus = false;

		/**
		 * posiciones en la barra de notificaciones
		 */
		int notifLabPos[] = { 2, 5, 14, 23, 32, 40 };
		public Screen(int pct, int selectedKbColc, int selectedKbRow){
			//Pantalla Completa
			//Background de los glyphs
			Glyph.background=bkgStack;
			//Cargamos las fuentes
			//Calculamos tama�os de las partes de lapantalla
			pctScreen=pct;
			kbSelectedCol=selectedKbColc;
			kbSelectedRow=selectedKbRow;
			new Thread(this).start();
	        //El rat�n
	        addMouseListener(new Raton());
		}

		public boolean isLongPress() {
			return longPress;
		}

		public void initGeometry(int pctScr) {
			anchura = getWidth();
			altura = getHeight();
			pctScreen=pctScr;
			//Dos lineas para el area de notificacion
			notifyAreaSize = 2 * notifFont.getFontHeight() + 4;
			//Una linea para los menus dinamicos
			dynButtAreaSize =  (dynamicBlack.getFontHeight() + 2);
			//Posicion del stack justo debajo de la de notificacion
			stackY = notifyAreaSize;
			//Maximo numero de lineas que pueden entrar
			stackAreaSize = (altura - notifyAreaSize - dynButtAreaSize) * pctScreen / 100;
			lineasVisibles = stackAreaSize / stackFont.getFontHeight();
			stackAreaSize = lineasVisibles * stackFont.getFontHeight();
			//Posicion del menu dinamico debajo de la de notificacion
			dynButtY = stackY + stackAreaSize;
			//Posicion del teclado debajo 
			keyboardY = dynButtY + dynButtAreaSize;
			//Tama�o del teclado: lo que queda
			keyboardAreaSize = altura - notifyAreaSize - stackAreaSize - dynButtAreaSize;
			plot = new Plot(0, stackY, anchura, stackAreaSize, normal, bkgPlot);
			notifyNeedRefresh=true;
			stackNeedRefresh=true;
			dynamicNeedRefresh=true;
			keyboardNeedRefresh=true;
		}

		public void update(Graphics g) {
			boolean busy=isBusy();
			if (notifyNeedRefresh)
				drawNotify(g);
			if (!busy && stackNeedRefresh && !alertDisp)
				drawScreen(g);
			if (!busy && dynamicNeedRefresh)
				drawDynamicButtons(g);
			if (!busy && keyboardNeedRefresh)
				drawKeyboard(g);
			if (alertDisp && !alertIsShowing)
				drawAlert(g);
		}

		private boolean alertIsShowing=false;
		private void drawAlert(Graphics g) {
			String fullTxt = alertMsg + "|";
			Vector<String> linStr=new Vector<String>();
			for (int p = fullTxt.indexOf("|"); p != -1; p = fullTxt.indexOf("|")) {
				String t = fullTxt.substring(0, p);
				String arr[] = new String[10];
				LineSplitter ls=new LineSplitter(t, anchura - 14, arr, normal);
				int el = ls.split(lineasVisibles);
				for (int j = 0; j < el; j++) {
					linStr.addElement(arr[j]);
				}
				fullTxt = fullTxt.substring(p + 1, fullTxt.length());
			}
			g.setColor(bkgAlert);
			g.fillRoundRect(
					10, stackY + 10, 
					anchura - 20, Math.min(altura - 20, 10 + linStr.size()*normal.getFontHeight()),
					15, 15);
			g.setColor(new Color(0));
			g.drawRoundRect(
					10, stackY + 10, 
					anchura - 20, Math.min(altura - 20,10 + linStr.size()*normal.getFontHeight()),
					15, 15);
			for(int linea=0;linea<linStr.size();linea++){
				String txt=(String)linStr.elementAt(linea);
				normal.getViewer(txt).paint(13, stackY + 13 + linea * normal.getFontHeight(), g);
			}
			alertIsShowing=true;
		}

		private void drawNotify(Graphics g) {
			notifyNeedRefresh = false;
			g.setColor(bkgNotifycolor);
			g.fillRoundRect(0, 0, anchura, notifyAreaSize, 2, 2);
			for (int i = 0; i < notifLabPos.length; i++){
				String statusLabel=getStatusLabel(i);
				while(notifFont==null)
					try {
						Thread.sleep(100);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				_Viewer v=notifFont.getViewer(statusLabel);
				v.paint(anchura * notifLabPos[i] / 100, 0, g);
			}
			notifFont.getViewer(Core.getCurrentDirName()).paint(0,notifyAreaSize / 2 - 1, g);
		}

		/**
		 * Depender� si estamos editando, mostraremos un campo abajo con un
		 * scroll hacia arriba.
		 * 
		 * @param g
		 */
		private void drawScreen(Graphics g) {
			stackNeedRefresh = false;
			switch (scrMode) {
			case SCRMODE_INTERACTIVE:
			case SCRMODE_STACK:
				drawStack(g);
				break;
			case SCRMODE_PLOT:
				drawPlot(g);
				break;
			}
		}

		private void drawPlot(Graphics g) {
			plot.paint(g);
		}		
		private void drawStack(Graphics g) {
			//Determina si dibujamos la pila tradicional o LaTeX
			boolean isML=Core.isSetting(ICalc.MULTILI);
			// Borramos
			int hei = stackFont.getFontHeight();
			g.setColor(bkgStack);
			//g.fillRoundRect(0, stackY, anchura, stackAreaSize - totalLineasEdicion * hei, 2, 2);
			g.fillRoundRect(0, stackY, anchura, stackAreaSize , 2, 2);
			
			// Si se est� editando, se quita espacio de la pila.
			// En interactiveStack no hay multilinea en el level0
			// Necesitamos conocer el tama�o del area de edicion
			if( isEditing()||forceEditInteractive){
				LineSplitter ls=new LineSplitter(getEditText(),  anchura, editArray, stackFont);
				totalLineasEdicion = ls.split(lineasVisibles); 
			}else{
				totalLineasEdicion = 0;
			}
			if (totalLineasEdicion > lineasVisibles) {
				primerLineaVisibleEdicion = totalLineasEdicion - lineasVisibles;
				totalLineasEdicion = lineasVisibles;
			} else {
				primerLineaVisibleEdicion = 0;
			}
			editAreaSize=totalLineasEdicion*hei;
			int maxHeight = stackAreaSize-editAreaSize; //en pixels
			//Se puede dibujar como texto o como graficos LaTeX
			boolean interMod=(scrMode==SCRMODE_INTERACTIVE);
			int fontHeight=stackFont.getFontHeight();
			// Bucle por cada linea de la pila. Se dibujan las lineas de abajo a
			// arriba
			// es decir linea=0 es la ultima linea de abajo.
			//Si estamos editando interactivamente la pila es posible que nos
			//encontremos en un objeto fuera del area inicial de visualizacion,
			//por lo que el nivel m�s bajo ha de ser corregido.
			int primLinea=!interMod?0:Math.max(0, interStackEditing-lineasVisibles+1);
			for (int nivPil = primLinea, currHeight = 0; currHeight+fontHeight <= maxHeight; nivPil++) {
				//true si estamos en level 1:
				boolean emp= (nivPil>=Core.size());
				boolean interSel=interMod && (nivPil==interStackEditing);
				// Ahora dibujamos el contenido si lo hay
				String marca = "" + (nivPil + 1) + (interSel?"\u0086":":");
				//Lo de quitarle 6 es a  ojo
				int nslw = stackFont.stringWidth(marca)-6;
				int mw=anchura-nslw;
				BufferedImage sLev=null;
				if(!emp){
					Stackable obj=Core.peek(nivPil);
					if(isML){
						switch(obj.getID()){
						case 7005: //GROB
							sLev=imageCache.put(""+obj.hashCode(),((Grob)obj).getImage());
							break;
						case 67:  //Matrix
						case 118: //Unit
						case 119: //InfixExp
						case 92:  //F8Vector
							sLev=imageCache.cacheManager(anchura,stackAreaSize,lineasVisibles,true, obj, mw, bkgStack, stackFont,sizeOfStackFont);
							break;
						default:
							sLev=imageCache.cacheManager(anchura,stackAreaSize,lineasVisibles,false, obj, mw, bkgStack, stackFont,sizeOfStackFont);
							break;
						}
					}else{
						sLev=imageCache.cacheManager(anchura,stackAreaSize,lineasVisibles,false, obj, mw, bkgStack, stackFont,sizeOfStackFont);
					}
				}else{
					sLev=imageCache.cacheManager(anchura,stackAreaSize,lineasVisibles,false, null, mw, bkgStack, stackFont,sizeOfStackFont);
				}
				/**
				 * Tama�o de la imagen
				 */
				int imgWidth=sLev.getWidth();
				int imgHeigh=sLev.getHeight();
				int levelY1=stackY+maxHeight-currHeight-imgHeigh;
				/**
				 * Scrolls por si se sale de la pantalla....
				 */
				ImageCache.ScrollPos sp=imageCache.getScrollPos(sLev);
				if(sp!=null && !sp.isInit()){
					sp.init(Math.max(0, imgWidth+nslw-anchura), 
							Math.max(0,imgHeigh-maxHeight+currHeight));
				}
				sp.calNextScrollPos();
				int sx=sp.getX();
				int sy=sp.getY(Math.max(0,imgHeigh-maxHeight+currHeight));
				//Comprobamos las dimensiones...
				if(imgHeigh>maxHeight-currHeight){//No entramos en altura
					int mh=maxHeight-currHeight;
					if(imgWidth+nslw>anchura){//tampoco en anchura
						drawRegion(g,sLev, sx, sy, mw, mh, 0, nslw, stackY, 0);
						g.setColor(scrollColor);
						//Scroll Horizontal
						g.fillRect(nslw+sx*mw/imgWidth,stackY,mw*mw/imgWidth,2);
						//Scroll Vertical
						g.fillRect(nslw,stackY+sy*mh/imgHeigh,2,mh*mh/imgHeigh);
					}else{ //Pero si en anchura
						drawRegion(g,sLev, 0, sy, imgWidth, mh, 0, anchura-imgWidth, stackY, 0);
						g.setColor(scrollColor);
						//Scroll Horizontal
						g.fillRect(nslw,stackY+sy*mh/imgHeigh,2,mh*mh/imgHeigh);
					}
					levelY1=stackY;
				}else{ //Entramos en altura
					if(imgWidth+nslw>anchura){//No entramos en anchura
						drawRegion(g,sLev, sx, 0, mw, imgHeigh, 0, nslw, levelY1, 0);
						g.setColor(scrollColor);
						g.fillRect(nslw+sx*mw/imgWidth,levelY1,mw*mw/imgWidth,2);
					}else{ //Tambien entramos en anchura
						g.drawImage(sLev,anchura-imgWidth,levelY1, new Color(0),null);
					}
				}
				//Colocamos el nivel de la pila.
				stackFont.getViewer(marca).paint(0, levelY1, g);
				currHeight+=sLev.getHeight(null);
			}
			//Se dibuja hora el area de edicion
			if (isEditing() || forceEditInteractive) {
				//Borramos la linea de edicion
				g.setColor(bkgEditing);
				g.fillRoundRect(
						0, stackY + stackAreaSize - editAreaSize, 
						anchura, editAreaSize,
						2, 2);
				// Aprovechamos que vamos a pintar los textos para localizar el
				// cursor
				int ep = getEditTextInserPos(), accu = getEditText().length();
				for (int i = 0; i < totalLineasEdicion; i++) {
					int levelY = stackY + stackAreaSize - (i + 1) * hei;
					g.setColor(new Color(0));
					String aux = 
						editArray[primerLineaVisibleEdicion + totalLineasEdicion - i - 1];
					int auxLen = aux.length();
					stackFont.getViewer(aux).paint(0, levelY, g);
					accu -= auxLen;
					if (accu <= ep && ep <= accu + auxLen) {// El cursor est� en
															// esta linea
						cursorY = levelY + 1;
						cursorX = stackFont.stringWidth(aux.substring(0, ep - accu));
						cursorLine = totalLineasEdicion - i - 1;
						//Pintamos el cursor
						if (blinkCursorStatus) {
							g.setColor(cursorOnColor);
							g.drawRect(cursorX , cursorY, 1, hei - 2);
						}
					}
				}
			}
		}

		private void drawRegion(Graphics g, BufferedImage sLev, int sx, int sy, 
								int mw, int mh, int transform, int dest_x, int dest_y, int anchor){
			g.drawImage(sLev, 
					dest_x, dest_y, dest_x+mw, dest_y+mh, 
					sx, sy, sx+mw, sy+mh, 
					null);
		}
		
		public ImageCache imageCache=new ImageCache(10);

		public void editCursorLeft() {
			int tip = getEditTextInserPos();
			if (--tip >= 0)
				setEditTextInserPos(tip);
		}

		public void editCursorRight() {
			int tip = getEditTextInserPos();
			if (++tip <= getEditText().length())
				setEditTextInserPos(tip);
		}

		public void editCursorUp() {
			int tip = getEditTextInserPos();
			int cl = cursorLine;
			if (--cl >= 0) {
				tip -= editArray[cl].length();
				if (tip < 0)
					tip = 0;
				setEditTextInserPos(tip);
			}
		}

		public void editCursorDown() {
			int tip = getEditTextInserPos();
			int cl = cursorLine;
			if (++cl < totalLineasEdicion) {
				tip += editArray[cursorLine].length();
				int max = getEditText().length();
				if (tip > max)
					tip = max;
				setEditTextInserPos(tip);
			}
		}

		private void drawDynamicButtons(Graphics g) {
			dynamicNeedRefresh = false;
			g.setColor(bkgStack);
			g.fillRect(0, dynButtY, anchura, dynButtAreaSize);
			TeclaVirtual teclas[] = getUpArray();
			int bw = anchura / numDynButt;
			boolean dirs[] = getUpIsDirectory();
			boolean checks[]=getUpIsOn();
			for (int i = 0; i < numDynButt; i++) {
				boolean bSel = isDynSelected() && getDynIndex() == i;
				boolean tec = (teclas[i] != null);
				boolean tecvar = tec && teclas[i].isVarSolver();
				String title= tec ? teclas[i].getTitle() : "";
				title = checks[i] ? "\u00AE"+title : title;
				F8Font fuente= tecvar ? dynamicBlack : dynamicWhite;
				Color bkgCir=bSel ? circleSelectedDyn : circleNonSelectedDyn;
				Color bkgBut= tecvar ? bkgDynVarSolv	: bkgDyn;
				Color bkg2= bSel ? circleSelectedDyn : bkgBut;
				gBut(g, title,bkg2, new Color(0xffffff), i * bw, dynButtY, bw, dynButtAreaSize, true, bkgCir, fuente, 0, 0);
				if (dirs[i]) {
					g.setColor(new Color(0));
					g.fillRect(i * bw + 1, dynButtY, bw / 2, 1);
				}
			}
		}

		private void drawKeyboard(Graphics g) {
			keyboardNeedRefresh = false;
			int bw = anchura / colBot;
			int bh = keyboardAreaSize / filBot;
			g.setColor(bkgStack);
			g.fillRect(0, keyboardY, anchura, altura);
			Keyboard keyb = getKeyb();
			for (int row = 0; row < filBot; row++) {
				int y = keyboardY + row * (bh+1);
				for (int col = 0; col < colBot; col++) {
					int x = col * bw;
					// bkg del boton
					g.setColor(bkgButton);
					g.fillRoundRect(x, y, bw, bh, 20, 20);
					// Botones
					// Ahora dibujamos el left-shift:
					Keyboard.MultiTecla mKey=keyb.get(col + row * colBot);
					String leftName=mKey.get(1).getTitle();
					gBut(g, leftName, bkgLeftShift, new Color(0), x + 1, y +1,
							(int) (0.5 * bw) - 1, normal.getFontHeight(),
							getShiftMode() == 1, circleSelected, normal, 8, 0);
					// Ahora el right-shift
					String rightName = mKey.get(2).getTitle();
					gBut(g, rightName, bkgRightShift, new Color(0), x
							+ (int) (0.5 * bw) , y +1 ,
							(int) (0.5 * bw) - 1, normal.getFontHeight(),
							getShiftMode() == 2, circleSelected, normal, 8, 0);
					// Ahora el no shift
					String noName = mKey.get(0).getTitle();
					gBut(g, noName, bkgButton, new Color(0),
							x + (int) (0.1 * bw), y + normal.getFontHeight()
									+ 2, (int) (0.8 * bw), 
									normal.getFontHeight(), false, new Color(0), normal, 8, 3);
					if(row==kbSelectedRow && col==kbSelectedCol && !isDynSelected()){
						g.setColor(new Color(0xFBEC5D));
						g.drawRoundRect(x+1, y+1, bw-3, bh-3, 18, 18);
					}else{
						// Contorno del boton
						g.setColor(circleButtonsColor);
					}
					g.drawRoundRect(x, y, bw-1, bh-1, 20, 20);
				}
			}
		}

		private void gBut(Graphics g, 
				String texto, Color bkg,  Color foreground, int x, int y, int width, int height,
				boolean circled, Color circledColor, F8Font fuente, int arcSize, int hueco) {
			// Porcion que cabe
			int longitud = texto.length();
			int enPixels;
			while ((enPixels = fuente.stringWidth(texto.substring(0, longitud))) > width)
				longitud--;
			// Escribir el texto
			g.setColor(bkg);
			g.fillRoundRect(x+hueco, y+hueco, width-2*hueco, height-2*hueco, arcSize, arcSize);
			g.setColor(foreground);
			fuente.getViewer(texto.substring(0, longitud)).paint(x + (width - enPixels) / 2, y-1, g);
			// �Recuadrar?
			if (circled) {
				g.setColor(circledColor);
				g.drawRoundRect(x+hueco, y+hueco, width-2*hueco, height-2*hueco, arcSize, arcSize);
			}
		}

		private void gestureScreen(int x, int y, int dx, int dy) {
			int controlItem=0;
			if(dynButtY<gestureInitY && gestureInitY<keyboardY){
				dynamicSelected=true;
				controlItem=3;
			}else if(keyboardY<gestureInitY){
				controlItem=4;
			}
			if(controlItem!=0){
				longPress=true;
				if(dx>dy){//predomina horizontal
					if(x<gestureInitX){//izquierda
						joystickLEFT();
					}else{//Derecha
						joystickRIGHT();
					}
				}else{//vertical
					if(y<gestureInitY){//Arriba
						joystickUP();
					}else{//Abajo
						joystickDOWN();
					}
				}
			}
		}

		public void clickScreen(int x, int y) {
			//Determinamos en qu� zona se ha punteado:
			if(y<notifyAreaSize){//Area de notificacino
				return; //No se hace nada
			}else if(y<notifyAreaSize+stackAreaSize){//En la pila 
				if(y>notifyAreaSize+stackAreaSize-editAreaSize){// editArea
					//Bueno, aqui habria que situar el cursor donde
					//el usuario ha picado...
				}
				return; //No se hace nada
			}else if(y<notifyAreaSize+stackAreaSize+dynButtAreaSize){//en los menus dynam
				int sob=anchura/kpp; //anchura de un boton dinamico
				dynamic.index=x/sob;
				dynamicSelected=true;
				//simulamos un evento de enter
				keyENTER(0);
			}else{ //en el teclado
				//filBot y colBot son el numero de botones que hay
				int yRel=y-keyboardY;
				int wob=anchura/colBot;
				int hob=keyboardAreaSize/filBot;
				kbSelectedCol=x/wob;
				kbSelectedRow=yRel/hob;
				//Ahora hay que determinar si se ha presionado left o right shift
				int bxRel=x-kbSelectedCol*wob;
				int byRel=yRel-kbSelectedRow*hob;
				int si=0;
				if(byRel<normal.getFontHeight()){//Shift
					if(bxRel<wob/2){//left
						si=1;
					}else{ //Right
						si=2;
					}
				}
				int tn=pantalla.kbSelectedCol+3*pantalla.kbSelectedRow; 
				TeclaVirtual tv = keyb.get(tn).get(si);
				si = 0;
				press(tv);
				pantalla.stackNeedRefresh=true;
				pantalla.keyboardNeedRefresh=true;
			}
		}
		//Hebra del canvas.
		public void run() {
			while(keepgoing){
				try {
					screenThread();
				} catch (RuntimeException e) {
					resetCalculator();
					System.gc();
					e.printStackTrace();
					output(e.getMessage());
					try {
						Thread.sleep(1000);
					} catch (InterruptedException e1) {
					}
				}
			}
		}
		/**
		 * colocar aqui todo aquello que ayude a resetear
		 *
		 */
		private void resetCalculator(){
			editText="";
			editTextInsertPos=0;
		}
		
		private void screenThread() {
			long lastBlinked = System.currentTimeMillis();
			long lastBusy = System.currentTimeMillis();
			long lastClockTick=System.currentTimeMillis();
			int  ticks=0;
			while (keepgoing) {
				long now = System.currentTimeMillis();
				// Control del tiempo de presionado de tecla
				if (keyAction!=-999 && beginKeyPress>0 && now - beginKeyPress > 300 ) {
					longPress = true;
					//Repeticiones
					beginKeyPress = System.currentTimeMillis();
					enqueueKeyAction(keyAction, false,charAction);
				}
				// Parpadeo del cursor
				if ( (isEditing() || forceEditInteractive )&& now - lastBlinked > 500) { 
					lastBlinked = now;
					blinkCursorStatus = !blinkCursorStatus;
					stackNeedRefresh = true;
					repaint();
				}
				if(busyCount>0 && now - lastBusy>200){
					if(++busyPos>=busyLen)
						busyPos=0;
					lastBusy=now;
					notifyNeedRefresh=true;
					repaint();
				}
				if(now-lastClockTick >990 ){
					lastClockTick=now;
					notifyNeedRefresh=true;
					repaint();
				}
				try {
					Thread.sleep(100);
					if(++ticks>3){
						ticks=0;
						if(imageCache.isScroll()){
							stackNeedRefresh=true;
							repaint();
						}
					}
				} catch (InterruptedException ie) {
					keepgoing=false;
				}
				if(alertDisp && (now-alertBeginNow>3000)){
					alertDisp=false;
					alertBeginNow=-1;
					stackNeedRefresh=true;
					alertIsShowing=false;
					repaint();
				}
			}
		}
		
	}
	/**
	 * Encola los eventos que llegan desde el UI o se generan desde el usuario
	 */
	Vector<QueuedAction> queue=new Vector<QueuedAction>();

	public void enqueueKeyAction(int key, boolean userEvent, Character c){
		queue.addElement(new QueuedAction(key,userEvent, c));
	}
	//Hebra del MIDLET. se encarga de procesar los eventos de usuario por fuera
	//de la hebra del canvas.
	public void run() {
		while(processingEvents){
			try {
				calcThread();
			} catch (RuntimeException e) {
				e.printStackTrace();
				output(e.getMessage());
				try {
					Thread.sleep(2000);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		}
	}

	private void calcThread() {
		while(processingEvents){
			while(queue.size()>0){//Hay teclas que procesar
				QueuedAction next=(QueuedAction)queue.elementAt(0);
				queue.removeElementAt(0);
				key(next);
			}
			pantalla.repaint();
			try {
				Thread.sleep(20);
			} catch (InterruptedException e) {
			}
		}
	}

	public void key(QueuedAction action) {
		int keyAction=action.getAction();
		boolean isUser=action.isUser();
		Character charAction=action.getChar();
		alertDisp=false;
		if(isUser){
			switch(keyAction){
			case ICalc.KEYBACK: 		keyBACK(); break;
			case ICalc.KEYDEL: 			keyDEL(); break;
			case ICalc.KEYCURUP: 		moveCursorUp(); break;
			case ICalc.KEYCURDOWN: 		moveCursorDown(); break;
			case ICalc.KEYCURLEFT: 		moveCursorLeft(); break;
			case ICalc.KEYCURRIGHT: 	moveCursorRight(); break;
			}
		}else if (charAction!=null){
			if(charAction=='\n'){
				try {
					new ENTER().pressed();
				}catch(F8Exception e){
					
				}
				pantalla.stackNeedRefresh=true;
			}else
				emit(""+action.getChar(),0);
		}else{
			switch (keyAction) {
			case KeyEvent.VK_UP:	  	joystickUP();			break;
			case KeyEvent.VK_DOWN:	  	joystickDOWN();			break;
			case KeyEvent.VK_LEFT:	  	joystickLEFT();			break;
			case KeyEvent.VK_RIGHT:	  	joystickRIGHT();		break;
			case -6:			      	keyLEFTSHIFT();			break;
			case -7:			      	keyRIGHTSHIFT();		break;
			case KeyEvent.VK_BACK_SPACE:keyBACK();				break;
			default:
				emit(String.valueOf((char)keyAction), 0);
			}
		}
	}

	public void joystickUP() {
		if(scrMode==SCRMODE_INTERACTIVE){
			if(interStackEditing+1<Core.size()){
				interStackEditing++;
				pantalla.stackNeedRefresh=true;
			}
		}else if(dynamicSelected && isLongPress()){
			//Intentamos colocar el menu padre
			if(dynamic.getPadre()!=null){
				setDynMenu(dynamic.getPadre());
				dynamicSelected = true;
				pantalla.dynamicNeedRefresh=true;
			}
		}else if(dynamicSelected && Core.size()>0){
			dynamicSelected=false;
			scrMode=SCRMODE_INTERACTIVE;
			interStackEditing=0;
			pantalla.dynamicNeedRefresh=true;
			pantalla.stackNeedRefresh=true;
		}else{
			if(isLongPress()){
				dynamicSelected = true;
			}else if((--pantalla.kbSelectedRow)<0){
				pantalla.kbSelectedRow=0;
				dynamicSelected = true;
			}
			pantalla.dynamicNeedRefresh=true;
			pantalla.keyboardNeedRefresh=true;
		}
	}

	public void joystickDOWN() {
		if(scrMode==SCRMODE_INTERACTIVE){
			if(interStackEditing>0){
				interStackEditing--;
				pantalla.stackNeedRefresh=true;
			}else{
				scrMode=SCRMODE_STACK;
				dynamicSelected=true;
				pantalla.stackNeedRefresh=true;
				pantalla.dynamicNeedRefresh=true;
			}
		}else{
			if(dynamicSelected){
				dynamicSelected = false;
				pantalla.dynamicNeedRefresh=true;
				pantalla.keyboardNeedRefresh=true;
			}else{
				if((++pantalla.kbSelectedRow)>3){
					pantalla.kbSelectedRow=0;
				}
				pantalla.keyboardNeedRefresh=true;
			}
		}
	}

	
	public final int maxKeybPages=4;
	public void joystickLEFT() {
		if (!dynamicSelected) {
			if(isLongPress()) {
				if (--keybPage == -1)
					keybPage = maxKeybPages-1;
			}else if((--pantalla.kbSelectedCol)<0){
				pantalla.kbSelectedCol=2;
			}
			pantalla.keyboardNeedRefresh=true;
		} else {
			if(isLongPress())
				preDynPage();
			else
				preDynNdx();
			pantalla.dynamicNeedRefresh=true;
		}

	}

	public void joystickRIGHT() {
		if (!dynamicSelected) {
			if(isLongPress()){
				if (++keybPage == maxKeybPages)
					keybPage = 0;
			}else if(++pantalla.kbSelectedCol>2){
				pantalla.kbSelectedCol=0;
			}
			pantalla.keyboardNeedRefresh=true;
		} else {
			if(isLongPress())
				nxtDynPage();
			else
				nxtDynNdx();
			pantalla.dynamicNeedRefresh=true;
		}
	}

	public void keyLEFTSHIFT() {
		pantalla.notifyNeedRefresh=true;
		pantalla.keyboardNeedRefresh=true;
		if (editText.length() > 0) {
			//Si hay texto en el editor, compilarlo
			flush();
			pantalla.stackNeedRefresh=true;
		}
		keyENTER(1);
	}

	public void keyRIGHTSHIFT() {
		pantalla.notifyNeedRefresh=true;
		pantalla.keyboardNeedRefresh=true;
		if (editText.length() > 0) {
			//Si hay texto en el editor, compilarlo
			flush();
			pantalla.stackNeedRefresh=true;
		}
		keyENTER(2);
	}
	
	public void keyBACK() {
		if (editTextInsertPos == 0)
			return;
		if (isLongPress()) {
			editText = editText.substring(editTextInsertPos);
			editTextInsertPos = 0;
		} else {
			editText = editText.substring(0, editTextInsertPos - 1)
					+ editText.substring(editTextInsertPos);
			editTextInsertPos--;
		}
		pantalla.stackNeedRefresh=true;
	}
	
	private final String nubmbersOfTec="123456789.0 ";
	public void keyNUMBER(int n) {
		emit(nubmbersOfTec.substring(n,n+1), 0);
	}

	public void keyDEL() {
		if (editTextInsertPos == editText.length())
			return;
		if (isLongPress()) {
			editText = editText.substring(0, editTextInsertPos);
		} else {
			editText = editText.substring(0, editTextInsertPos)
					+ editText.substring(editTextInsertPos + 1);
		}
		pantalla.stackNeedRefresh=true;
	}

	public void moveCursorDown() {
		if (scrMode == SCRMODE_STACK) {
			pantalla.editCursorDown();
		} else if (scrMode == SCRMODE_PLOT) {
			movePlotCursor(plot.cursor.nextDown());
			if(plot.cursor.v[1].toInt()>=pantalla.stackAreaSize)
				plot.cursor.v[1]=new Ereal(pantalla.stackAreaSize-1);
		}
		pantalla.stackNeedRefresh=true;
	}

	public void moveCursorLeft() {
		if (scrMode == SCRMODE_STACK) {
			pantalla.editCursorLeft();
		} else if (scrMode == SCRMODE_PLOT) {
			movePlotCursor(plot.cursor.nextLeft());
			if(plot.cursor.v[0].toInt()<0)
				plot.cursor.v[0]=Ereal.cero;
		}
		pantalla.stackNeedRefresh=true;
	}

	public void moveCursorRight() {
		if (scrMode == SCRMODE_STACK) {
			pantalla.editCursorRight();
		} else if (scrMode == SCRMODE_PLOT) {
			movePlotCursor(plot.cursor.nextRight());
			if(plot.cursor.v[0].toInt()>=pantalla.anchura)
				plot.cursor.v[0]=new Ereal(pantalla.anchura-1);
		}
		pantalla.stackNeedRefresh=true;
	}

	public void moveCursorUp() {
		if (scrMode == SCRMODE_STACK) {
			pantalla.editCursorUp();
		} else if (scrMode == SCRMODE_PLOT) {
			movePlotCursor(plot.cursor.nextUp());
			if(plot.cursor.v[1].toInt()<0)
				plot.cursor.v[1]=Ereal.cero;
		}
		pantalla.stackNeedRefresh=true;
	}
	
	public boolean isBusy(){
		return busyCount>0;
	}

	@Override
	public void update(Graphics g) {
		//No se borra
	}

	@Override
	public void paint(Graphics g) {
		//Lo que se pinta es Screen
	}

	public static void main(String args[]) {
		Calc calc= new Calc();
		Dimension minimumSize=new Dimension(320,480);
		calc.setMinimumSize(minimumSize);
		calc.setResizable(false);
		calc.pack();
		calc.setVisible(true);
		calc.initFonts();
	}

}
