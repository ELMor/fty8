package f8.platform.glyph;

import java.awt.Graphics;
/**
 * Glyph vac�o pero que ocupa espacio
 * @author elinares
 *
 */
public class GlyphEmpty extends Glyph {
	int width,height,base;
	public GlyphEmpty(Glyph p, int w, int h, int b){
		super(p);
		width=w;
		height=h;
		base=b;
	}
	public GlyphEmpty(Glyph p){
		this(p,0,0,0);
	}
	public void dibuja(Graphics g) {
	}
	public int getBase() {
		return base;
	}
	public int getHeight() {
		return height;
	}
	public int getWidth() {
		return width;
	}
}

