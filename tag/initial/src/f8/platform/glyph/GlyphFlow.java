package f8.platform.glyph;

import java.awt.Graphics;
import java.util.Vector;
/**
 * Base para la serie de glyphs verticales y horizontales
 * @author elinares
 *
 */
public abstract class GlyphFlow extends Glyph {
	Vector<Glyph> elems=new Vector<Glyph>();
		
	public void addHijo(Glyph hijo) {
		hijo.padre=this;
		elems.addElement(hijo);
	}
	
	public Glyph getHijo(int i){
		if(0<=i && i<elems.size())
			return (Glyph)elems.elementAt(i);
		return null;
	}

	public GlyphFlow(Glyph pad){
		super(pad);
	}

	public void dibuja(Graphics g) {
		for(int i=0;i<elems.size();i++){
			((Glyph)elems.elementAt(i)).dibuja(g);
		}
	}
	
}
