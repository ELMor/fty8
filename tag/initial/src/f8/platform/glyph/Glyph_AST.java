package f8.platform.glyph;

import antlr.collections.AST;
import f8.commands.math.mkl;
import f8.kernel.yacc.ObjectParserTokenTypes;
import f8.kernel.yacc.OpPrior;
import f8.objects.types.InfixExp;

public class Glyph_AST extends GlyphHFlow implements ObjectParserTokenTypes {
	public boolean isTwo = false;

	public Glyph_AST(Glyph cpad, AST nodo, int sz) {
		super(cpad);
		switch (nodo.getType()) {
		case MATRIZ:
			addHijo(new Glyph_Matriz(cpad, nodo, sz));
			break;
		case VECTOR:
			addHijo(new Glyph_Vector(cpad, nodo, sz));
			break;
		case DERIV: {
			AST fun = nodo.getFirstChild();
			AST var = fun.getNextSibling();
			addHijo(new Glyph_Deriv(this, fun, var, sz));
			break;
		}
		case INTEG: {
			AST ini = nodo.getFirstChild();
			AST end = ini.getNextSibling();
			AST fun = end.getNextSibling();
			AST var = fun.getNextSibling();
			addHijo(new Glyph_Integ(this, fun, var, ini, end, sz));
			break;
		}
		case SIGMA: {
			AST var = nodo.getFirstChild();
			AST ini = var.getNextSibling();
			AST end = ini.getNextSibling();
			AST fun = end.getNextSibling();
			addHijo(new Glyph_Sigma(this, fun, var, ini, end, sz));
			break;
		}
		case LITERAL:
			nodo = nodo.getFirstChild();
		case ID:
			addHijo(new Glyph_VarFun(this,nodo,sz));
			break;
		case NEG:
		case NUMBER:
			try {
				if (Double.parseDouble(nodo.getText()) == 2.0)
					isTwo = true;
			} catch (Exception e) {

			}
		case COMPLEX:
		case STRING: {
			addHijo(new Glyph_String(this, InfixExp.forma(nodo), sz));
			break;
		}
		case UNIT: {
			AST uVal = nodo.getFirstChild();
			AST uMag = uVal.getNextSibling();
			GlyphHFlow unit=new GlyphHFlow(this);
			unit.addHijo(new Glyph_AST(this, uVal,sz));
			unit.addHijo(new Glyph_Parentesis(this,new Glyph_AST(this, uMag,sz-4),1));
			addHijo(unit);
			break;
		}
		case INV: {
			addHijo(new Glyph_Div(this, mkl.uno, nodo.getFirstChild(), sz));
			break;
		}
		case DIV: {
			AST num = nodo.getFirstChild();
			AST den = num.getNextSibling();
			addHijo(new Glyph_Div(this, num, den, sz));
			break;
		}
		case CARET: {
			AST aBas = nodo.getFirstChild();
			AST aExp = aBas.getNextSibling();
			int pp = OpPrior.val[nodo.getType()];
			int op = OpPrior.val[aBas.getType()];
			if(aExp.getType()==ObjectParserTokenTypes.DIV){
				addHijo(new Glyph_Root(this, aBas, aExp, sz, pp > op));
			}else{
				addHijo(new Glyph_Elev(this, aBas, aExp, sz, pp > op));
			}
			break;
		}
		default: {
			int pp = OpPrior.val[nodo.getType()];
			for (AST child = nodo.getFirstChild(); child != null; child = child
					.getNextSibling()) {
				boolean isTheFirst = true;
				if (child != nodo.getFirstChild()) {
					if (nodo.getType() == MULT) {
						addHijo(new Glyph_Mult(null));
					} else {
						addHijo(new Glyph_String(null, nodo.getText(), sz));
					}
					isTheFirst = false;
				}
				int cp = OpPrior.val[child.getType()];
				if (pp>cp||(isTheFirst&&pp==cp&&(cp==64||cp==32))) {
					addHijo(new Glyph_Parentesis(this, child, sz));
				} else {
					addHijo(new Glyph_AST(this, child, sz));
				}
			}
		}
		}
	}

}
