package f8.platform.glyph;

import antlr.collections.AST;

public class Glyph_Deriv extends GlyphHFlow{

	public Glyph_Deriv(Glyph cpad, AST f, AST v, int sz){
		super(cpad);
		Glyph d=new Glyph_String(this,"\u0088",sz);
		Glyph dx=new Glyph_String(this,"\u0088"+v.getText(),sz);
		Glyph fu=new Glyph_AST(this,f,sz);
		
		GlyphVFlow parcial=new GlyphVFlow(this);
		parcial.addHijo(d);
		parcial.addHijo(new Glyph_Barra(this,dx.getWidth(),3,1));
		parcial.addHijo(dx);
		
		addHijo(parcial);
		addHijo(fu);
	}
}
