package f8.platform.glyph;

import java.awt.Graphics;

import antlr.collections.AST;

public class Glyph_Elev extends Glyph {
	Glyph gBase,gExpo;
	boolean isFunctionPower=false;
	public Glyph_Elev(Glyph cpad, AST b, AST e, int sz, boolean parbase){
		super(cpad);
		if(parbase){
			gBase=new Glyph_Parentesis(this,b,sz);
		}else{
			gBase=new Glyph_AST(this,b,sz);
		}
		gExpo=new Glyph_AST(this,e,sz-4);
		if(gBase instanceof Glyph_AST){
			Glyph_AST ga=(Glyph_AST)gBase;
			if(ga.getHijo(0) instanceof Glyph_VarFun)
				isFunctionPower=true;
		}
	}

	public int getHeight() {
		return gBase.getHeight()+gExpo.getBase();
	}

	public int getWidth() {
		return gBase.getWidth()+gExpo.getWidth();
	}

	public int getBase() {
		return gBase.getBase()+gExpo.getBase();
	}

	public void dibuja(Graphics g) {
		if(!isFunctionPower){
			gBase.setX(0);
			gBase.setY(gExpo.getBase());
			gExpo.setX(gBase.getWidth());
			gExpo.setY(0);
			gBase.dibuja(g);
			gExpo.dibuja(g);
		}else{
			((Glyph_VarFun)(((Glyph_AST)gBase).getHijo(0))).elevadoA=gExpo;
			gBase.dibuja(g);
		}
	}
}
