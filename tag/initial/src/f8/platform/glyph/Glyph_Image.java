package f8.platform.glyph;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;

public class Glyph_Image extends GlyphEmpty {
	String name;
	  public Glyph_Image(Glyph pad, String n, int w, int h, int b) {
		super(pad,w,h,b);
		this.name="/"+n+".png";
	}

	public void dibuja(Graphics g) {
			Image img=Toolkit.getDefaultToolkit().createImage(name);
			BufferedImage tmp=new BufferedImage(img.getWidth(null), img.getHeight(null), BufferedImage.TYPE_INT_RGB);
			tmp.getGraphics().drawImage(img, 0, 0, null);
			BufferedImage rs=resizeImage(tmp);
			g.drawImage(rs, realX(), realY(), new Color(0),null);
	}

	private BufferedImage resizeImage(BufferedImage src) {
	      int srcWidth = src.getWidth();
	      int srcHeight = src.getHeight();
	      BufferedImage tmp = new BufferedImage(width, srcHeight,BufferedImage.TYPE_INT_RGB);
	      Graphics g = tmp.getGraphics();
	      g.setColor(background);
	      g.fillRect(0, 0, width, srcHeight);
	      int ratio = (srcWidth << 16) / width;
	      int pos = ratio/2;
	      //Horizontal Resize        
	      for (int x = 0; x < width; x++) {
	          g.setClip(x, 0, 1, srcHeight);
	          g.drawImage(src, x - (pos >> 16), 0, null);
	          pos += ratio;
	      }
	      BufferedImage resizedImage = new BufferedImage(width, height,BufferedImage.TYPE_INT_RGB);
	      g = resizedImage.getGraphics();
	      g.setColor(background);
	      g.fillRect(0, 0, width, height);
	      ratio = (srcHeight << 16) / height;
	      pos = ratio/2;        
	      //Vertical resize
	      for (int y = 0; y < height; y++) {
	          g.setClip(0, y, width, 1);
	          g.drawImage(tmp, 0, y - (pos >> 16),null);
	          pos += ratio;
	      }
	      return resizedImage;
	  }    

}
