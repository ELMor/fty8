package f8.platform.glyph;

import java.awt.Graphics;

import antlr.collections.AST;

/**
 * Este Glyph dibuja paréntesis que encierran al Glyph que se le pasa como constructor.
 * @author elinares
 *
 */
public class Glyph_Parentesis extends Glyph {
	public static final int sq=4;
	int tipo=0;
	Glyph in;
	
	public Glyph_Parentesis(Glyph cpad, AST ex,int sz){
		this(cpad,new Glyph_AST(null,ex,sz));
	}
	
	public Glyph_Parentesis(Glyph cpad, Glyph ge){
		super(cpad);
		if(ge!=null)
			init(ge);
	}

	public Glyph_Parentesis(Glyph cpad, Glyph ge,int t){
		this(cpad,ge);
		tipo=t;
	}

	protected void init(Glyph ge) {
		ge.padre=this;
		in=ge;
	}

	public void par1(Graphics g) {
		g.setColor(borderColor);
		int rx=realX();
		int ry=realY();
		int anch=in.getWidth()-sq;
		int altu=in.getHeight();
		g.drawLine(rx+sq+anch, ry, rx+sq+anch+sq, ry+sq);
		g.drawLine(rx+sq+anch, ry, rx+sq+anch+sq-1, ry+sq);
		g.drawLine(rx+sq+anch+sq, ry+sq, rx+sq+anch+sq, ry+sq+altu);
		g.drawLine(rx+sq+anch+sq-1, ry+sq, rx+sq+anch+sq-1, ry+sq+altu);
		g.drawLine(rx+sq+anch+sq, ry+sq+altu, rx+sq+anch, ry+sq+altu+sq);
		g.drawLine(rx+sq+anch+sq-1, ry+sq+altu, rx+sq+anch, ry+sq+altu+sq);
		g.drawLine(rx+sq, ry, rx, ry+sq);
		g.drawLine(rx+sq, ry, rx+1, ry+sq);
		g.drawLine(rx, ry+sq, rx, ry+sq+altu );
		g.drawLine(rx+1, ry+sq, rx+1, ry+sq+altu );
		g.drawLine(rx, ry+sq+altu, rx+sq, ry+sq+altu+sq);
		g.drawLine(rx+1, ry+sq+altu, rx+sq, ry+sq+altu+sq);
	}
	
	public void par2(Graphics g) {
		g.setColor(borderColor);
		int rx=realX();
		int ry=realY();
		int anch=in.getWidth()-sq;
		int altu=in.getHeight();
		g.drawLine(rx+sq+anch, ry+sq, rx+sq+anch+sq, ry+sq);
		g.drawLine(rx+sq+anch, ry+sq, rx+sq+anch+sq-1, ry+sq);
		g.drawLine(rx+sq+anch+sq, ry+sq, rx+sq+anch+sq, ry+sq+altu);
		g.drawLine(rx+sq+anch+sq-1, ry+sq, rx+sq+anch+sq-1, ry+sq+altu);
		g.drawLine(rx+sq+anch+sq, ry+sq+altu, rx+sq+anch, ry+sq+altu);
		g.drawLine(rx+sq+anch+sq-1, ry+sq+altu, rx+sq+anch, ry+sq+altu);
		g.drawLine(rx+sq, ry+sq, rx, ry+sq);
		g.drawLine(rx+sq, ry+sq, rx+1, ry+sq);
		g.drawLine(rx, ry+sq, rx, ry+sq+altu );
		g.drawLine(rx+1, ry+sq, rx+1, ry+sq+altu );
		g.drawLine(rx, ry+sq+altu, rx+sq, ry+sq+altu);
		g.drawLine(rx+1, ry+sq+altu, rx+sq, ry+sq+altu);
	}
	
	public void dibuja(Graphics g) {
		if(tipo==0)
			par1(g);
		else
			par2(g);
		in.setX(sq);
		in.setY(sq);
		in.dibuja(g);
	}

	public int getBase() {
		if(tipo==0)
			return sq+in.getBase();
		return sq/2+in.getBase();
	}

	public int getHeight() {
		if(tipo==0)
			return in.getHeight()+2*sq;
		return in.getHeight()+sq+1;
	}

	public int getWidth() {
		return in.getWidth()+2*sq;
	}
}
