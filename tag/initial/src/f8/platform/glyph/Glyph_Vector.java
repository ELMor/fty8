package f8.platform.glyph;

import antlr.collections.AST;
/**
 * Puente entre un vector AST y un vector Glyps
 * @author elinares
 *
 */
public class Glyph_Vector extends Glyph_Parentesis{
	public Glyph_Vector(Glyph cpad, AST def, int sz){
		super(cpad,null);
		GlyphVFlow stack=new GlyphVFlow(this);
		for(AST c=def.getFirstChild();c!=null;c=c.getNextSibling())
			stack.addHijo(new Glyph_AST(this,c,sz));
		init(stack);
	}
}
