package f8.platform.io;

import java.io.DataInputStream;
import java.io.IOException;

public class DIS extends DataInputStream {
	private static byte buffer[]=new byte[256];
	
	public DIS(DataInputStream in) {
		super(in);
	}
	public String readStringSafe() throws IOException {
		short len=readShort();
		if(len==-1)
			return null;
		if(len==0)
			return "";
		int pos=0,readed=0;
		String ret="";
		while(pos<len){
			readed=read(buffer,0, (len-pos)>buffer.length?buffer.length:(len-pos));
			ret+=new String(buffer,0,readed,"ISO-8859-1");
			pos+=readed;
		}
		return ret;
	}
}
